﻿using Caliburn.Micro;
using LengthConverter.Models;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Windows.UI.Xaml.Controls;

namespace LengthConverter.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;

        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.navigationService = navigationService;

            DecimalPoint = new decimal(1.1).ToString(CultureInfo.CurrentCulture)[1].ToString();

            Units = new ObservableCollection<LengthUnit>();

            var res = new Windows.ApplicationModel.Resources.ResourceLoader();

            #region Standard units creation
            Units.Add(new LengthUnit(res.GetString("Nanometers"), 1.0e-9));
            Units.Add(new LengthUnit(res.GetString("Microns"), 1.0e-6));
            Units.Add(new LengthUnit(res.GetString("Centimeters"), 1.0e-2));
            Units.Add(new LengthUnit(res.GetString("Decimeters"), 1.0e-1));
            Units.Add(new LengthUnit(res.GetString("Meters"), 1.0));
            Units.Add(new LengthUnit(res.GetString("Kilometers"), 1.0e+3));

            Units.Add(new LengthUnit(res.GetString("Miles"), 1609.344));
            Units.Add(new LengthUnit(res.GetString("Inches"), 39.3700787));
            Units.Add(new LengthUnit(res.GetString("Feet"), 3.2808399));
            Units.Add(new LengthUnit(res.GetString("Yards"), 1.0936133));
            #endregion

            ConvertTo = ConvertFrom = Units[0];

            ConvertFromValue = Converted = "0";

        }

        #region UI functions

        public void KbPressed(String value)
        {
            if (ConvertFromValue != "0")
                ConvertFromValue += value;
            else
                ConvertFromValue = value;
        }

        public void Clear()
        {
            ConvertFromValue = "0";
        }

        public void Backspace()
        {
            if (ConvertFromValue == "0")
                return;
            else if (ConvertFromValue.Length == 1)
            {
                ConvertFromValue = "0";
                return;
            }

            ConvertFromValue = ConvertFromValue.Substring(0, ConvertFromValue.Length - 1);
        }

        public void Dot()
        {
            if (!ConvertFromValue.Contains(DecimalPoint))
                ConvertFromValue += DecimalPoint;
        }

        #endregion

        #region Private functions - Conversions
        private void Convert()
        {
            if (String.IsNullOrEmpty(Converted) || ConvertTo == null)
                return;
            Converted = (double.Parse(ConvertFromValue, CultureInfo.CurrentCulture) * ConvertFrom.InMeters / ConvertTo.InMeters).ToString();
        }

        #endregion

        #region Properties

        private LengthUnit _ConvertFrom;
        public LengthUnit ConvertFrom
        {
            get { return _ConvertFrom; }
            set
            {
                _ConvertFrom = value;
                NotifyOfPropertyChange(() => ConvertFrom);
                Convert();
            }
        }

        private LengthUnit _ConvertTo;
        public LengthUnit ConvertTo
        {
            get { return _ConvertTo; }
            set
            {
                _ConvertTo = value;
                NotifyOfPropertyChange(() => ConvertTo);
                Convert();
            }
        }

        private String _ConvertFromValue;
        public String ConvertFromValue
        {
            get { return _ConvertFromValue; }
            set
            {
                _ConvertFromValue = value;
                NotifyOfPropertyChange(() => ConvertFromValue);
                Convert();
            }
        }

        private String _Converted;
        public String Converted
        {
            get { return _Converted; }
            set
            {
                _Converted = value;
                NotifyOfPropertyChange(() => Converted);
            }
        }

        private String _DecimalPoint;
        public String DecimalPoint
        {
            get { return _DecimalPoint; }
            set
            {
                _DecimalPoint = value;
                NotifyOfPropertyChange(() => DecimalPoint);
            }
        }


        public ObservableCollection<LengthUnit> Units { get; private set; }
#endregion
        
    }
}
