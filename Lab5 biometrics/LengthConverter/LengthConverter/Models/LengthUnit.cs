﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LengthConverter.Models
{
    public class LengthUnit : PropertyChangedBase
    {
        public LengthUnit(String Name, double ConvertFromMeter)
        {
            this.Name = Name;
            this.InMeters = ConvertFromMeter;
        }

        private String _Name;
        public String Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private double _InMeters;
        public double InMeters
        {
            get { return _InMeters; }
            set
            {
                _InMeters = value;
                NotifyOfPropertyChange(() => InMeters);
            }
        }
        
        

    }
}
