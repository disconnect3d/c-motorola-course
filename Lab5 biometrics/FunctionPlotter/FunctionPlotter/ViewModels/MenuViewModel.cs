﻿using Caliburn.Micro;
using System;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace FunctionPlotter.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;

        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.navigationService = navigationService;
            Plot = new PointCollection();
            PointsX = new PointCollection();
            PointsY = new PointCollection();
            ScaleXIncrement = ScaleYIncrement = 0.1;
            ScaleX = ScaleY = 1.0;
            AxisXIncrement = 0.05;
            MoveIncrement = 5;

            AxisMinX = AxisMaxX = AxisMinY = AxisMaxY = String.Empty;
        }


        #region Properties

        #region Canvas points properties
        private PointCollection _Plot;
        public PointCollection Plot
        {
            get { return _Plot; }
            set
            {
                _Plot = value;
                NotifyOfPropertyChange(() => Plot);
            }
        }

        private PointCollection _PointsX;
        public PointCollection PointsX
        {
            get { return _PointsX; }
            set
            {
                _PointsX = value;
                NotifyOfPropertyChange(() => PointsX);
            }
        }

        private PointCollection _PointsY;
        public PointCollection PointsY
        {
            get { return _PointsY; }
            set
            {
                _PointsY = value;
                NotifyOfPropertyChange(() => PointsY);
            }
        }
        #endregion

        private String _Expr;
        /// <summary>
        /// Expression String that is evaluated after user presses Eval button
        /// </summary>
        public String Expr
        {
            get { return _Expr; }
            set
            {
                _Expr = value;
                NotifyOfPropertyChange(() => Expr);
            }
        }

        #region Axis properties
        private double _AxisXIncrement;
        public double AxisXIncrement
        {
            get { return _AxisXIncrement; }
            set
            {
                _AxisXIncrement = value;
                NotifyOfPropertyChange(() => AxisXIncrement);
            }
        }

        private String _AxisMaxX;
        public String AxisMaxX
        {
            get { return _AxisMaxX; }
            set
            {
                _AxisMaxX = value;
                NotifyOfPropertyChange(() => AxisMaxX);
            }
        }

        private String _AxisMinX;
        public String AxisMinX
        {
            get { return _AxisMinX; }
            set
            {
                _AxisMinX = value;
                NotifyOfPropertyChange(() => AxisMinX);
            }
        }

        private String _AxisMaxY;
        public String AxisMaxY
        {
            get { return _AxisMaxY; }
            set
            {
                _AxisMaxY = value;
                NotifyOfPropertyChange(() => AxisMaxY);
            }
        }


        private String _AxisMinY;
        public String AxisMinY
        {
            get { return _AxisMinY; }
            set
            {
                _AxisMinY = value;
                NotifyOfPropertyChange(() => AxisMinY);
            }
        }

        private int _MoveIncrement;
        /// <summary>
        /// Determines how many x-es the plot will move when user presses MoveRight/MoveLeft buttons
        /// </summary>
        public int MoveIncrement
        {
            get { return _MoveIncrement; }
            set
            {
                _MoveIncrement = value;
                NotifyOfPropertyChange(() => MoveIncrement);
            }
        }

        #endregion

        #region Scales
        private double _ScaleXIncrement;
        public double ScaleXIncrement
        {
            get { return _ScaleXIncrement; }
            set
            {
                _ScaleXIncrement = value;
                NotifyOfPropertyChange(() => ScaleXIncrement);
            }
        }

        private double _ScaleYIncrement;
        public double ScaleYIncrement
        {
            get { return _ScaleYIncrement; }
            set
            {
                _ScaleYIncrement = value;
                NotifyOfPropertyChange(() => ScaleYIncrement);
            }
        }
        #endregion

        #endregion

        #region Variables
        private double StartX = 0;
        private double StartY = 0;

        private double ScaleX;
        private double ScaleY;

        private double CanvasHeight;
        private double CanvasWidth;

        private Maths.Formula Formula;
        #endregion

        #region Constants
        private const double MinScaleX = 0.05;
        private const double MaxScaleX = 50.0;
                    
        private const double MinScaleY = 0.05;
        private const double MaxScaleY = 50.0;

        public double MinScaleIncrement { get { return 0.01; } }
        public double MaxScaleIncrement { get { return 2.0; } }
        public double UIScaleIncrement { get { return 0.05; } }

        public double MinMoveIncrement { get { return 10; } }
        public double MaxMoveIncrement { get { return 5000; } }
        public double UIMoveIncrement { get { return 50; } }
        #endregion

        #region UI funcs
        /// <summary>
        /// Filters TextBox.Text to prevent user from passing unwanted characters into the expression
        /// </summary>
        /// <param name="tb">TextBox control</param>
        public void TextFilter(TextBox tb)
        {
            String result = String.Empty;
            foreach (var match in Regex.Matches(tb.Text, @"[+*\-/x0-9]"))
                result += match.ToString();

            tb.Text = result;
            tb.Select(tb.Text.Length, 0);
        }

        /// <summary>
        /// Action fired on Canvas' SizeChanged event
        /// </summary>
        /// <param name="Args">EventArgs needed to get new size</param>
        public void GetCanvasSize(Windows.UI.Xaml.SizeChangedEventArgs Args)
        {
            CanvasWidth = Args.NewSize.Width;
            CanvasHeight = Args.NewSize.Height;

            if (Formula != null)
                PlotFunction();
            PlotAxes();
        }

        /// <summary>
        /// Evaluates formula and replots the function
        /// </summary>
        public void Eval()
        {
            if (String.IsNullOrEmpty(Expr))
                return;
            try
            {
                Formula = Maths.MathParse.Parse(Expr);
            }
            catch (Exception)
            {
                var res = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();
                new Windows.UI.Popups.MessageDialog(res.GetString("ParseException"), res.GetString("Title.Text"));
                return;
            }

            PlotFunction();
        }

        #region Zooms
        /// <summary>
        /// Zooms in the plot by changing ScaleX and ScaleY values (and replotting everything)
        /// </summary>
        public void ZoomIn()
        {
            if (ScaleX + ScaleXIncrement >= MaxScaleX || ScaleY + ScaleYIncrement >= MaxScaleY)
                return;

            ScaleX += ScaleXIncrement;
            ScaleY += ScaleYIncrement;

            UpdateAxisInfo();
            TryPlotFunction();
        }

        /// <summary>
        /// Zooms out the plot by changing ScaleX and ScaleY values (and replotting everything)
        /// </summary>
        public void ZoomOut()
        {
            if (ScaleX - ScaleXIncrement <= MinScaleX || ScaleY - ScaleYIncrement <= MinScaleY)
                return;

            ScaleX -= ScaleXIncrement;
            ScaleY -= ScaleYIncrement;

            UpdateAxisInfo();
            TryPlotFunction();
        }
        #endregion

        #region Move functions
        /// <summary>
        /// Moves plot left by changing StartX position and replotting axes & function
        /// </summary>
        public void MoveLeft()
        {
            StartX += MoveIncrement;

            PlotAxes();
            TryPlotFunction();
        }

        /// <summary>
        /// Moves plot right by changing StartX position and replotting axes & function
        /// </summary>
        public void MoveRight()
        {
            StartX -= MoveIncrement;

            PlotAxes();
            TryPlotFunction();
        }

        /// <summary>
        /// Moves plot up by changing StartY position and replotting axes & function
        /// </summary>
        public void MoveUp()
        {
            StartY += MoveIncrement;

            PlotAxes();
            TryPlotFunction();
        }

        /// <summary>
        /// Moves plot down by changing StartY position and replotting axes & function
        /// </summary>
        public void MoveDown()
        {
            StartY -= MoveIncrement;

            PlotAxes();
            TryPlotFunction();
        }
        #endregion

        public void SettingsUpdate()
        {
            PlotAxes();
            TryPlotFunction();
        }

        #endregion

        #region Implementation code
        /// <summary>
        /// Plots function to the canvas
        /// </summary>
        private void PlotFunction()
        {
            double halfCanvasH = CanvasHeight / 2.0;
            double halfCanvasW = CanvasWidth / 2.0;

            Plot.Clear();

            double maxX = CanvasWidth / ScaleX;

            for (double x = 0; x < maxX; x += AxisXIncrement)
            {
                double usedX = x * ScaleX;
                double y = Formula.Evaluate(usedX - halfCanvasW - StartX);
                double usedY = StartY + halfCanvasH - y * ScaleY;

                if (usedY >= CanvasHeight || usedY < 0)
                    continue;

                Plot.Add(new Point(usedX, usedY));
            }
        }

        /// <summary>
        /// If it's possible plots function, otherwise not
        /// </summary>
        private void TryPlotFunction()
        {
            if (Formula != null)
                PlotFunction();
        }


        /// <summary>
        /// Plots axes to the canvas
        /// </summary>
        private void PlotAxes()
        {
            PointsX.Clear();
            PointsY.Clear();

            //const int Offset = 30;
            const int BorderOffset = 10;
            const int ArrowLength = 30;
            const int ArrowSize = 10;
            int ArrowEndX = (int)CanvasWidth - BorderOffset;
            int ArrowEndY = (int)CanvasHeight - BorderOffset;

            #region Creating X axis arrow
            double y = CanvasHeight / 2 + StartY;          //old value: (int)CanvasHeight - Offset;

            // Checking if we didn't go out of plot...
            if (y >= CanvasHeight - ArrowSize)
                y = CanvasWidth;
            else if (y <= 0)
                y = 0;

            // Adding arrow points
            PointsX.Add(new Point(0, y));
            PointsX.Add(new Point(ArrowEndX, y));
            PointsX.Add(new Point(ArrowEndX - ArrowLength, y - ArrowSize));
            PointsX.Add(new Point(ArrowEndX, y));
            PointsX.Add(new Point(ArrowEndX - ArrowLength, y + ArrowSize));
            PointsX.Add(new Point(ArrowEndX, y));
            #endregion

            #region Creating Y axis arrow
            double x = StartX + (int)CanvasWidth / 2;           //old value: Offset;

            // Checking if we didn't go out of plot...
            if (x >= CanvasWidth - ArrowSize)
                x = CanvasWidth;
            else if (x <= 0)
                x = 0;

            // Adding arrow points
            PointsY.Add(new Point(x, y));
            PointsY.Add(new Point(x, ArrowEndY));
            PointsY.Add(new Point(x, BorderOffset));

            PointsY.Add(new Point(x - ArrowSize, BorderOffset + ArrowLength));
            PointsY.Add(new Point(x, BorderOffset));
            PointsY.Add(new Point(x + ArrowSize, BorderOffset + ArrowLength));
            PointsY.Add(new Point(x, BorderOffset));
            #endregion

            UpdateAxisInfo();
        }

        /// <summary>
        /// Updates max and min axis values
        /// </summary>
        private void UpdateAxisInfo()
        {
            double relevantX = CanvasWidth / ScaleX;
            AxisMaxX = Math.Round(relevantX + StartX, 1).ToString();
            AxisMinX = "-" + Math.Round(Math.Abs(relevantX - StartX), 1).ToString();

            double relevantY = CanvasHeight / ScaleY;
            AxisMaxY = Math.Round(relevantY + StartY, 1).ToString();
            AxisMinY = "-" + Math.Round(Math.Abs(relevantY - StartY), 1).ToString();
        }
        #endregion
    }
}
