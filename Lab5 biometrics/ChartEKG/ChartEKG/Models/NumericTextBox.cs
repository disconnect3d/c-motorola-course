﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Xaml.Controls;

namespace ChartEKG.Models
{
    public class NumericTextBox : TextBox
    {
        readonly uint[] KeyArray = {
                                       (uint)VirtualKey.Number0, (uint)VirtualKey.Number1, (uint)VirtualKey.Number2, (uint)VirtualKey.Number3,
                                       (uint)VirtualKey.Number4, (uint)VirtualKey.Number5, (uint)VirtualKey.Number6, (uint)VirtualKey.Number7,
                                       (uint)VirtualKey.Number8, (uint)VirtualKey.Number9,
                                       (uint)VirtualKey.NumberPad0, (uint)VirtualKey.NumberPad1, (uint)VirtualKey.NumberPad2, (uint)VirtualKey.NumberPad3,
                                       (uint)VirtualKey.NumberPad4, (uint)VirtualKey.NumberPad5, (uint)VirtualKey.NumberPad6, (uint)VirtualKey.NumberPad7,
                                       (uint)VirtualKey.NumberPad8, (uint)VirtualKey.NumberPad9,
                                       (uint)VirtualKey.Back, (uint)VirtualKey.Delete, (uint)VirtualKey.NumberKeyLock,
                                       (uint)VirtualKey.Left, (uint)VirtualKey.Right, (uint)VirtualKey.Up, (uint)VirtualKey.Down
                                   };
        protected override void OnKeyDown(Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (KeyArray.Contains((uint)e.Key))
                base.OnKeyDown(e);
            else
                e.Handled = true;
        }
    }
}
