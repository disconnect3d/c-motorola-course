﻿using Caliburn.Micro;
using ChartEKG.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Controls;
using Windows.System.Threading;
using Windows.ApplicationModel.Resources;

namespace ChartEKG.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this._navigationService = navigationService;
            StartStopIcon = new SymbolIcon();
            StartStopIcon.Symbol = Symbol.Play;
            Dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;
            Resources = new Windows.ApplicationModel.Resources.ResourceLoader();
            DataLoaded = false;
            Interval = 1;
            Speed = 10;
        }
        public async void LoadText()
        {
            #region Picking file
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.List;
            FOP.FileTypeFilter.Add(".txt");
            FOP.FileTypeFilter.Add(".ascii");
            FOP.FileTypeFilter.Add(".dat");
            FOP.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            StorageFile file = await FOP.PickSingleFileAsync();

            if (file == null)
                return;

            String txt;
            try
            {
                txt = await FileIO.ReadTextAsync(file);
            }
            catch (Exception e)
            {
                new Windows.UI.Popups.MessageDialog(Resources.GetString("ReadFail_Msg") + e.Message, Resources.GetString("Error")).ShowAsync();
                return;
            }
            #endregion

            int numbers = txt.Count(i => i == ' ');
            Data = new long[numbers];

            int count = 0;
            long maxPossibleValue = CanvasHeight / 2;
            MaxValue = 0;

            // parsing file data
            foreach (var strNumber in txt.Split(' '))
            {
                if (strNumber != String.Empty)
                {
                    int parsed;
                    if (!int.TryParse(strNumber, out parsed))
                    {
                        await new Windows.UI.Popups.MessageDialog(Resources.GetString("ConversionFail_Msg"), Resources.GetString("ConversionFail_Title")).ShowAsync();
                        return;
                    }
                    Data[count++] = parsed;
                    int absParsed = Math.Abs(parsed);
                    if (MaxValue < absParsed)
                        MaxValue = absParsed;
                }
            }


            // copying data to rawdata (may be helpful when user rotates screen)
            RawData = new long[Data.Length];
            for (int i = 0; i < Data.Length; ++i)
                RawData[i] = Data[i];


            // scaling values
            SetScaledValuesFromRawData();


            // plotting
            ReplotStandardLine();
            StartDataIndex = 0;
            Replot(StartDataIndex);


            // showing upper and lower bounds
            ShowMaxMinValues();

            DataLoaded = true;
        }

        public void GoLeft()
        {
            StartDataIndex -= Speed;
            if (StartDataIndex <= 0)
                StartDataIndex = 0;
            Replot(StartDataIndex);
        }

        public void GoRight()
        {
            StartDataIndex += Speed;
            if (StartDataIndex > Data.Length - Speed)
                StartDataIndex = Data.Length - Speed - 1;
            Replot(StartDataIndex);
        }

        public async void StartStop()
        {
            StartStopIcon.Symbol = PlotStarted ? Symbol.Play : Symbol.Pause;
            PlotStarted = !PlotStarted;

            if (PlotStarted)
            {
                while (PlotStarted)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => { GoRight(); });
                    await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(100));
                }
            }
        }

        public void GetCanvasSize(Windows.UI.Xaml.SizeChangedEventArgs Args)
        {
            CanvasWidth = (long)Args.NewSize.Width;
            CanvasHeight = (long)Args.NewSize.Height;
            if (DataLoaded)
            {
                SetScaledValuesFromRawData();
                Replot(StartDataIndex);
                ReplotStandardLine();
            }
        }

        public void ResetPlot()
        {
            Replot(StartDataIndex = 0);
        }

        private void ShowMaxMinValues()
        {
            MaxValueStr = MaxValue.ToString();
            MinValueStr = (-MaxValue).ToString();
        }
        private void SetScaledValuesFromRawData()
        {
            long maxPossibleValue = CanvasHeight / 2;
            for (int i = 0; i < Data.Length; ++i)
            {
                long currValue = (long)(maxPossibleValue * RawData[i] / MaxValue);
                Data[i] = maxPossibleValue - currValue;
            }
        }
        private void Replot(int dataIndex)
        {
            PlotPoints.Clear();
            for (PlotIndex = 0; PlotIndex < CanvasWidth + Interval - 1 && dataIndex + PlotIndex < Data.Length; PlotIndex += Interval, dataIndex += 1)
                PlotPoints.Add(new Point(PlotIndex, Data[dataIndex]));
        }

        private void ReplotStandardLine()
        {
            ZeroPointLine.Clear();
            ZeroPointLine.Add(new Point(0, CanvasHeight / 2));
            ZeroPointLine.Add(new Point(CanvasWidth, CanvasHeight / 2));
        }


        private Windows.UI.Core.CoreDispatcher Dispatcher;
        private int StartDataIndex;
        private int PlotIndex;
        private long CanvasWidth;
        private long CanvasHeight;
        private long[] Data;
        private long[] RawData;
        private ResourceLoader Resources;

        private int _Interval;
        public int Interval
        {
            get { return _Interval; }
            set
            {
                _Interval = value;
                NotifyOfPropertyChange(() => Interval);
                if (DataLoaded)
                    Replot(StartDataIndex);
            }
        }
        

        private bool PlotStarted = false;

        private SymbolIcon _StartStopIcon;
        public SymbolIcon StartStopIcon
        {
            get { return _StartStopIcon; }
            set
            {
                _StartStopIcon = value;
                NotifyOfPropertyChange(() => StartStopIcon);
            }
        }



        private PointCollection _PlotPoints = new PointCollection();
        public PointCollection PlotPoints
        {
            get { return _PlotPoints; }
            set
            {
                _PlotPoints = value;
                //NotifyOfPropertyChange(() => PlotPoints);
            }
        }

        private PointCollection _ZeroPointLine = new PointCollection();
        public PointCollection ZeroPointLine
        {
            get { return _ZeroPointLine; }
            set
            {
                _ZeroPointLine = value;
                NotifyOfPropertyChange(() => ZeroPointLine);
            }
        }

        private bool _DataLoaded;
        public bool DataLoaded
        {
            get { return _DataLoaded; }
            set
            {
                _DataLoaded = value;
                NotifyOfPropertyChange(() => DataLoaded);
            }
        }

        private long _MaxValue;
        public long MaxValue
        {
            get { return _MaxValue; }
            set
            {
                _MaxValue = value;
                NotifyOfPropertyChange(() => MaxValue);
            }
        }

        private String _MaxValueStr;
        public String MaxValueStr
        {
            get { return _MaxValueStr; }
            set
            {
                _MaxValueStr = value;
                NotifyOfPropertyChange(() => MaxValueStr);
            }
        }

        private String _MinValueStr;
        public String MinValueStr
        {
            get { return _MinValueStr; }
            set
            {
                _MinValueStr = value;
                NotifyOfPropertyChange(() => MinValueStr);
            }
        }

        private int _Speed;
        public int Speed
        {
            get { return _Speed; }
            set
            {
                _Speed = value;
                NotifyOfPropertyChange(() => Speed);
            }
        }
        
    }
}
