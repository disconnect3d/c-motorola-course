﻿using Navigation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Dcz_1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public partial class MainPage : Page, INotifyPropertyChanged
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Required;
            CB.Items.Add("Brak");
            CB.Items.Add("Negatyw");
            CB.Items.Add("Odcień szarości"); // (R+G+B)/3
            CB.Items.Add("Odcień szarości - dla ludzkiego oka");
            CB.Items.Add("Sepia");           // R=R+2*W, G=G+W, B=B, W należ do [20;40]  gdy R lub G > 255 => 255
            CB.Items.Add("Normalizacja");    // normalizacja // P = 255 * (P-min)/ (max-min) , min i max to wartości maksymalne każdego kanału
            CB.Items.Add("Krzyż Robertsa");  // Krzyż Robertsa - poruszamy się na otoczeniu pixela (x,y), (x+1,y), (x,y+1), (x+1,y+1) ; zakładamy na pixel maskę:
            // | 1  0 | | 0 1 |
            // | 0 -1 | |-1 0 |
            CB.Items.Add("Filtr Sobela");
            CB.SelectedIndex = 0;
            DataContext = this;
            SepiaParamVisibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DataContext = this;
        }

        private async void Btn_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.Thumbnail;
            FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            FOP.FileTypeFilter.Add(".bmp");
            FOP.FileTypeFilter.Add(".jpg");
            FOP.FileTypeFilter.Add(".png");
            FOP.FileTypeFilter.Add(".jpeg");

            StorageFile file = await FOP.PickSingleFileAsync();
            if (file != null)
            {
                fileStream = await file.OpenAsync(FileAccessMode.Read);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(fileStream);

                LeftImg.Source = bitmapImage;
                LeftTxt.Text = file.Name;

                switch (file.FileType.ToLower())
                {
                    case ".jpg":
                    case ".jpeg":
                        decoderId = BitmapDecoder.JpegDecoderId;
                        break;
                    case ".bmp":
                        decoderId = BitmapDecoder.BmpDecoderId;
                        break;
                    case ".png":
                        decoderId = BitmapDecoder.PngDecoderId;
                        break;
                    case ".gif":
                        decoderId = BitmapDecoder.GifDecoderId;
                        break;
                }
                decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
                pixelData = await decoder.GetPixelDataAsync(
                    BitmapPixelFormat.Bgra8,    // byte array format: Blue Green Red Alpha [8 bit values: 0-255]
                    BitmapAlphaMode.Straight,   // How to code Alpha channel
                    new BitmapTransform(),
                    ExifOrientationMode.IgnoreExifOrientation,
                    ColorManagementMode.DoNotColorManage);

                originalPixels = pixelData.DetachPixelData();
                sourceModifiedPixels = null;
                RightImg.Source = null;
            }
        }

        private IRandomAccessStream fileStream = null;
        private Guid decoderId;

        #region AlgorithmsImplementation
        private static int CutColorRange(int val)
        {
            return val > 255 ? 255 : val < 0 ? 0 : val;
        }
        private static uint CutColorRange(uint val)
        {
            return val > 255 ? 255 : val < 0 ? 0 : val;
        }


        private byte[] SwitchColor(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            int resultIndex = 0;
            byte dpp = 255;
            for (int y = 0; y < pixelHeight; ++y)
            {
                for (int x = 0; x < pixelWidth; ++x)
                {
                    sourcePixels[resultIndex] = (byte)(dpp - sourcePixels[resultIndex]);
                    sourcePixels[resultIndex + 1] = (byte)(dpp - sourcePixels[resultIndex + 1]);
                    sourcePixels[resultIndex + 2] = (byte)(dpp - sourcePixels[resultIndex + 2]);
                    resultIndex += 4;
                }
            }
            return sourcePixels;
        }

        private byte[] Sepia(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            int resultIndex = 0;
            for (int y = 0; y < pixelHeight; ++y)
            {
                for (int x = 0; x < pixelWidth; ++x)
                {
                    int G = sourcePixels[resultIndex + 1] + SepiaParam;
                    int R = sourcePixels[resultIndex + 2] + 2 * SepiaParam;
                    sourcePixels[resultIndex + 2] = (byte)CutColorRange(R);
                    sourcePixels[resultIndex + 1] = (byte)CutColorRange(G);
                    resultIndex += 4;
                }
            }
            return sourcePixels;
        }

        private byte[] GreyScale(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            int resultIndex = 0;
            for (int y = 0; y < pixelHeight; ++y)
            {
                for (int x = 0; x < pixelWidth; ++x)
                {
                    sourcePixels[resultIndex] = sourcePixels[resultIndex + 1] = sourcePixels[resultIndex + 2]
                        = (byte)((sourcePixels[resultIndex] + sourcePixels[resultIndex + 1] + sourcePixels[resultIndex + 2]) / 3);
                    resultIndex += 4;
                }
            }
            return sourcePixels;
        }

        private byte[] GreyScale4HumansEye(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)  // TODO: Implement this
        {
            int resultIndex = 0;
            for (int y = 0; y < pixelHeight; ++y)
            {
                for (int x = 0; x < pixelWidth; ++x)
                {
                    sourcePixels[resultIndex] = sourcePixels[resultIndex + 1] = sourcePixels[resultIndex + 2]
                        = (byte)(sourcePixels[resultIndex] * 0.11 + sourcePixels[resultIndex + 1] * 0.59 + sourcePixels[resultIndex + 2] * 0.3);
                    resultIndex += 4;
                }
            }
            return sourcePixels;
        }


        private byte[] Normalization(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            int resultIndex = 0;
            byte dpp = 255;
            for (int y = 0; y < pixelHeight; ++y)
            {
                for (int x = 0; x < pixelWidth; ++x)
                {
                    int B = sourcePixels[resultIndex] - NormalizeBMin;
                    B = CutColorRange(B);
                    int G = sourcePixels[resultIndex + 1] - NormalizeGMin;
                    G = CutColorRange(G);
                    int R = sourcePixels[resultIndex + 2] - NormalizeRMin;
                    R = CutColorRange(R);

                    sourcePixels[resultIndex] = (byte)(dpp * B / (NormalizeBMax - NormalizeBMin));
                    sourcePixels[resultIndex + 1] = (byte)(dpp * G / (NormalizeGMax - NormalizeGMin));
                    sourcePixels[resultIndex + 2] = (byte)(dpp * R / (NormalizeRMax - NormalizeRMin));
                    resultIndex += 4;
                }
            }
            return sourcePixels;
        }

        private byte[] RobertsCross(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            const int channels = 4;

            // creating most of the image
            for (uint y = 0; y < pixelHeight; ++y)
            {
                for (uint x = 0; x < pixelWidth; ++x)
                {
                    for (uint i = 0; i < 3; ++i)
                    {
                        // (x, y)
                        uint x0y0 = y * pixelWidth * channels + x * channels + i;

                        // if pixels are not on the right or bottom borders
                        if (y != pixelHeight - 1 && x != pixelWidth - 1)
                        {
                            // (x+1, y+1)
                            uint x1y1 = x0y0 + pixelWidth * channels + channels;
                            // (x+1, y)
                            uint x1y0 = x0y0 + channels;
                            // (x, y+1)
                            uint x0y1 = x0y0 + pixelWidth * channels;

                            // tmp1 = (x, y) - (x+1, y+1)
                            int tmp1 = sourcePixels[x0y0] - sourcePixels[x1y1];
                            // tmp2 = (x+1, y) - (x, y+1)
                            int tmp2 = sourcePixels[x1y0] - sourcePixels[x0y1];

                            // (x,y) = (tmp1) + (tmp2)
                            sourcePixels[x0y0] = (byte)CutColorRange(Math.Abs(tmp1) + Math.Abs(tmp2));
                        }
                        else // set pixel to black color (each channel to 0) when on border
                            sourcePixels[x0y0] = (byte)0;
                    }
                }
            }
            return sourcePixels;
        }

        private byte[] SobelsFilter(byte[] sourcePixels, uint pixelWidth, uint pixelHeight)
        {
            const uint channels = 4;
            byte[] result = new byte[sourcePixels.Length];
            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4

            // creating most of the image
            for (uint y = 1; y < pixelHeight - 1; ++y)
            {
                for (uint x = 1; x < pixelWidth - 1; ++x)
                {
                    for (uint i = 0; i < 3; ++i)
                    {
                        // px = (x, y)
                        uint px = y * pixelWidth * channels + x * channels + i;

                        uint moveBottom = pixelWidth * channels;
                        // p0 = (x-1, y-1)
                        uint p0 = px - channels - moveBottom;
                        // p1 = (x, y-1)
                        uint p1 = px - moveBottom;
                        // p2 = (x+1, y-1)
                        uint p2 = px + channels - moveBottom;
                        // p3 = (x+1, y)
                        uint p3 = px + channels;
                        // p4 = (x+1, y+1)
                        uint p4 = px + channels + moveBottom;
                        // p5 = (x, y+1)
                        uint p5 = px + moveBottom;
                        // p6 = (x-1, y+1)
                        uint p6 = px - channels + moveBottom;
                        // p7 = (x-1, y)
                        uint p7 = px - channels;

                        // tmp1 = (p2 + 2*p3 + p4) - (p0 + 2*p7 + p6)
                        int tmp1 = (
                            (
                                sourcePixels[p2] + (2 * sourcePixels[p3]) + sourcePixels[p4]
                            )
                            -
                            (
                                sourcePixels[p0] + (2 * sourcePixels[p7]) + sourcePixels[p6]
                            ));
                        // tmp2 = (p6 + 2*p5 + p4) - (p0 + 2*p1 + p2)
                        int tmp2 = (
                            (
                                sourcePixels[p6] + (2 * sourcePixels[p5]) + sourcePixels[p4]
                            )
                            -
                            (
                                sourcePixels[p0] + (2 * sourcePixels[p1]) + sourcePixels[p2]
                            ));

                        // (x,y) = sqrt(tmp1^2 + tmp2^2)
                        result[px] = (byte)CutColorRange((uint)(Math.Pow((Math.Pow(tmp1, 2) + Math.Pow(tmp2, 2)), 0.5)));
                    }
                }
            }
            return result;
        }
        #endregion

        private async void ProcessImage(byte[] sourcePixels, bool addEffects = false)
        {
            switch ((String)(CB.SelectedItem))
            {
                #region Negative
                case "Negatyw":
                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = SwitchColor(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                #region Sepia
                case "Sepia":
                    if (int.TryParse(SepiaTxt.Text, out SepiaParam) && SepiaParam >= 20 && SepiaParam <= 40)
                        await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = Sepia(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }));
                    else
                    {
                        Windows.UI.Popups.MessageDialog x = new Windows.UI.Popups.MessageDialog("Zła wartość parametru sepii, użyj wartość z przedziału [20; 40]", "Błąd");
                        await x.ShowAsync();
                        return;
                    }
                    break;
                #endregion

                #region GreyScale
                case "Odcień szarości":
                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = GreyScale(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                #region GreyScale4HumansEye
                case "Odcień szarości - dla ludzkiego oka":
                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = GreyScale4HumansEye(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                #region Normalization
                case "Normalizacja":
                    // Looking for min & max RGB values
                    byte[] Barray = sourcePixels.Where((item, index) => index % 4 == 0).ToArray();
                    NormalizeBMin = Barray.Min();
                    NormalizeBMax = Barray.Max();
                    byte[] Garray = sourcePixels.Where((item, index) => index % 4 == 1).ToArray();
                    NormalizeGMin = Garray.Min();
                    NormalizeGMax = Garray.Max();
                    byte[] Rarray = sourcePixels.Where((item, index) => index % 4 == 2).ToArray();
                    NormalizeRMin = Rarray.Min();
                    NormalizeRMax = Rarray.Max();

                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = Normalization(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                #region RobertsCross
                case "Krzyż Robertsa":

                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = RobertsCross(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                #region SobelFilter
                case "Filtr Sobela":

                    await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            sourcePixels = SobelsFilter(sourcePixels, decoder.PixelWidth, decoder.PixelHeight);
                        }
                    ));
                    break;
                #endregion

                case "Brak":
                    return;
            }

            using (Stream stream = writeableBitmap.PixelBuffer.AsStream())
                await stream.WriteAsync(sourcePixels, 0, sourcePixels.Length);

            RightImg.Source = writeableBitmap;

            if (!addEffects)
                RightTxt.Text = LeftTxt.Text + " - " + (String)(CB.SelectedItem);
            else
                RightTxt.Text += " + " + (String)(CB.SelectedItem);

            for (int i = 0; i < sourcePixels.Length; ++i)
                sourceModifiedPixels[i] = sourcePixels[i];
        }



        private void CB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((String)(((ComboBox)sender).SelectedItem) == "Sepia")
                SepiaParamVisibility = Visibility.Visible;
            else
                SepiaParamVisibility = Visibility.Collapsed;
        }

        private async void LB_Click(object sender, RoutedEventArgs e)
        {
            if (fileStream == null)
            {
                Windows.UI.Popups.MessageDialog x = new Windows.UI.Popups.MessageDialog("Brak otwartego obrazka.", "Błąd");
                await x.ShowAsync();
                return;
            }
            sourceModifiedPixels = new byte[originalPixels.Length];
            writeableBitmap = new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight);

            byte[] copyPixels = new byte[originalPixels.Length];
            for (int i = 0; i < originalPixels.Length; ++i)
                copyPixels[i] = originalPixels[i];
            ProcessImage(copyPixels);
        }

        private async void RB_Click(object sender, RoutedEventArgs e)
        {
            if (fileStream == null || sourceModifiedPixels == null)
            {
                Windows.UI.Popups.MessageDialog x = new Windows.UI.Popups.MessageDialog("Najpierw zmodyfikuj obrazek.", "Błąd");
                await x.ShowAsync();
                return;
            }
            writeableBitmap = new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight);
            ProcessImage(sourceModifiedPixels, true);
        }

        private BitmapDecoder decoder;
        private PixelDataProvider pixelData;
        WriteableBitmap writeableBitmap;
        private byte[] originalPixels;
        private byte[] sourceModifiedPixels;
        private int SepiaParam;
        private int NormalizeRMin, NormalizeRMax;
        private int NormalizeGMin, NormalizeGMax;
        private int NormalizeBMin, NormalizeBMax;

        private Visibility _SepiaParamVisibility;
        public Visibility SepiaParamVisibility
        {
            get { return _SepiaParamVisibility; }
            set
            {
                _SepiaParamVisibility = value;
                OnPropertyChanged("SepiaParamVisibility");

            }
        }
        private void GoToHistogram(byte[] sourcePixels)
        {
            HistogramPageConfiguration pageParameters = new HistogramPageConfiguration();

            pageParameters.BlueChannel = sourcePixels.Where((item, index) => index % 4 == 0).ToArray();
            pageParameters.GreenChannel = sourcePixels.Where((item, index) => index % 4 == 1).ToArray();
            pageParameters.RedChannel = sourcePixels.Where((item, index) => index % 4 == 2).ToArray();

            Frame.Navigate(typeof(HistogramPage), pageParameters);
        }

        private async void HistogramOriginal_Click(object sender, RoutedEventArgs e)
        {
            if (originalPixels == null)
            {
                Windows.UI.Popups.MessageDialog x = new Windows.UI.Popups.MessageDialog("Najpierw załaduj obrazek.", "Błąd");
                await x.ShowAsync();
                return;
            }
            GoToHistogram(originalPixels);
        }

        private async void HistogramModification_Click(object sender, RoutedEventArgs e)
        {
            if (sourceModifiedPixels == null)
            {
                Windows.UI.Popups.MessageDialog x = new Windows.UI.Popups.MessageDialog("Najpierw zmodyfikuj obrazek.", "Błąd");
                await x.ShowAsync();
                return;
            }
            GoToHistogram(sourceModifiedPixels);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

