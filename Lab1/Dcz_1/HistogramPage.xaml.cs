﻿using Navigation;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Dcz_1
{
    public sealed partial class HistogramPage : Page, INotifyPropertyChanged
    {
        public HistogramPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            const String XAxisDescription = "Odcień danego koloru";
            const String YAxisDescription = "Liczba pikseli";
            const int minX = 0;
            const int maxX = 255;
            const int minY = 0;
            const int maxY = 500;
            var col = OxyColors.White;

            this.DataContext = this;

            var cfg = (HistogramPageConfiguration)e.Parameter;

            byte[] GreyByteArr = new byte[cfg.RedChannel.Length];
            for (int i = 0; i < cfg.RedChannel.Length; ++i)
            {
                GreyByteArr[i] = (byte)(((int)cfg.RedChannel[i] + (int)cfg.GreenChannel[i] + (int)cfg.BlueChannel[i]) / 3);
            }
            int[] GreyChannel = CountPixels(GreyByteArr);
            Rescale(GreyChannel, GreyChannel.Max());

            //await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) =>
            //{
            RedChannel = CountPixels(cfg.RedChannel);
            GreenChannel = CountPixels(cfg.GreenChannel);
            BlueChannel = CountPixels(cfg.BlueChannel);
            Rescale(RedChannel, RedChannel.Max());
            Rescale(GreenChannel, GreenChannel.Max());
            Rescale(BlueChannel, BlueChannel.Max());
            //}));

            RedPlotModel = new PlotModel() { Title = "Histogram kanału czerwonego", LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            RedPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, XAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            RedPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, YAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            for (int i = 0; i < 256; ++i)
            {
                var RedPlotDataSeries = new AreaSeries();
                RedPlotDataSeries.Points.Add(new DataPoint(i, RedChannel[i]));
                RedPlotDataSeries.Points.Add(new DataPoint(i + 1, RedChannel[i]));
                RedPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                RedPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                RedPlotDataSeries.Color = OxyColor.FromRgb((byte)i, 0, 0);
                RedPlotDataSeries.Fill = OxyColor.FromRgb((byte)i, 0, 0);
                RedPlotDataSeries.Background = OxyColors.Gold;
                RedPlotModel.Series.Add(RedPlotDataSeries);
            }

            GreenPlotModel = new PlotModel() { Title = "Histogram kanału zielonego", LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            GreenPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, XAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            GreenPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, YAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            for (int i = 0; i < 256; ++i)
            {
                var GreenPlotDataSeries = new AreaSeries();
                GreenPlotDataSeries.Points.Add(new DataPoint(i, GreenChannel[i]));
                GreenPlotDataSeries.Points.Add(new DataPoint(i + 1, GreenChannel[i]));
                GreenPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                GreenPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                GreenPlotDataSeries.Color = OxyColor.FromRgb(0, (byte)i, 0);
                GreenPlotDataSeries.Fill = OxyColor.FromRgb(0, (byte)i, 0);
                GreenPlotDataSeries.Background = OxyColors.Gold;
                GreenPlotModel.Series.Add(GreenPlotDataSeries);
            }

            BluePlotModel = new PlotModel() { Title = "Histogram kanału niebieskiego", LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            BluePlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, XAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            BluePlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, YAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            for (int i = 0; i < 256; ++i)
            {
                var BluePlotDataSeries = new AreaSeries();
                BluePlotDataSeries.Points.Add(new DataPoint(i, BlueChannel[i]));
                BluePlotDataSeries.Points.Add(new DataPoint(i + 1, BlueChannel[i]));
                BluePlotDataSeries.Points2.Add(new DataPoint(i, 0));
                BluePlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                BluePlotDataSeries.Color = OxyColor.FromRgb(0, 0, (byte)i);
                BluePlotDataSeries.Fill = OxyColor.FromRgb(0, 0, (byte)i);
                BluePlotDataSeries.Background = OxyColors.Gold;
                BluePlotModel.Series.Add(BluePlotDataSeries);
            }

            GreyPlotModel = new PlotModel() { Title = "Histogram odcieni szarości", LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            GreyPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, XAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            GreyPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, YAxisDescription) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            for (int i = 0; i < 256; ++i)
            {
                var GreyPlotDataSeries = new AreaSeries();
                GreyPlotDataSeries.Points.Add(new DataPoint(i, GreyChannel[i]));
                GreyPlotDataSeries.Points.Add(new DataPoint(i + 1, GreyChannel[i]));
                GreyPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                GreyPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                GreyPlotDataSeries.Color = OxyColor.FromRgb((byte)i, (byte)i, (byte)i);
                GreyPlotDataSeries.Fill = OxyColor.FromRgb((byte)i, (byte)i, (byte)i);
                GreyPlotDataSeries.Background = OxyColors.Gold;
                GreyPlotModel.Series.Add(GreyPlotDataSeries);
            }

            base.OnNavigatedTo(e);
        }


        private int[] CountPixels(byte[] channelArray)
        {
            int[] result = new int[256];

            for (int i = 0; i <= 255; ++i)
                result[i] = channelArray.Count(channel => channel == i);

            return result;
        }

        void Rescale(int[] arr, int max)
        {
            for (int i = 0; i < arr.Length; ++i)
                arr[i] = 500 * arr[i] / max;
        }

        private int[] RedChannel;
        private int[] GreenChannel;
        private int[] BlueChannel;

        private PlotModel _RedPlotModel;
        public PlotModel RedPlotModel
        {
            get { return _RedPlotModel; }
            set
            {
                _RedPlotModel = value;
                OnPropertyChanged("RedPlotModel");
            }
        }

        private PlotModel _GreenPlotModel;
        public PlotModel GreenPlotModel
        {
            get { return _GreenPlotModel; }
            set
            {
                _GreenPlotModel = value;
                OnPropertyChanged("GreenPlotModel");
            }
        }

        private PlotModel _BluePlotModel;
        public PlotModel BluePlotModel
        {
            get { return _BluePlotModel; }
            set
            {
                _BluePlotModel = value;
                OnPropertyChanged("BluePlotModel");
            }
        }

        private PlotModel _GreyPlotModel;
        public PlotModel GreyPlotModel
        {
            get { return _GreyPlotModel; }
            set
            {
                _GreyPlotModel = value;
                OnPropertyChanged("GreyPlotModel");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}
