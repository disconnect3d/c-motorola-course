﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace ChartEKG.Models
{
    public class EKGData
    {

        /// <summary>
        /// Creates EKGData by parsing dataStr
        /// </summary>
        /// <param name="dataStr">String containing EKG data points, delimited by spaces (' ')</param>
        /// <param name="frequency">EKG data points frequency, in Hertz; used in smoothing</param>
        /// <param name="fillData">Determines if RawData should be copied to Data; used when called by another ctors</param>
        public EKGData(String dataStr, int frequency = 1000, bool fillData = true)
        {
            this.Frequency = frequency;

            int numbers = dataStr.Count(i => i == ' ');
            RawData = new int[numbers];

            int count = 0;
            this.MinRawValue = this.MaxRawValue = 0;

            // Parsing string data
            int ii = 0;
            foreach (var strNumber in dataStr.Split(' '))
            {
                if (strNumber != String.Empty)
                {
                    int parsed;
                    if (!int.TryParse(strNumber, out parsed))
                        throw new ArgumentException("Failed to parse EKG data string");
                    RawData[count++] = parsed;
                    int absParsed = Math.Abs(parsed);
                    if (MaxRawValue < absParsed)
                        MaxRawValue = absParsed;
                    if (MinRawValue > absParsed)
                        MinRawValue = absParsed;
                }
            }

            Data = new int[RawData.Length];
            if (fillData)
            {
                // Copying data to rawdata (may be helpful when we would like to rescale data; it is better to rescale it from raw data)
                Array.Copy(RawData, Data, RawData.Length);
                this.MaxPossibleValue = (int)this.MaxRawValue;
            }
        }

        /// <summary>
        /// Parses data from String and scales it's height to the one specified
        /// </summary>
        /// <param name="dataStr">Data string delimited with " " (spaces)</param>
        /// <param name="newHeight">Height to scale the data to</param>
        /// <param name="frequency">EKG data points frequency, in Hertz; used in smoothing</param>
        public EKGData(String dataStr, int newHeight, int frequency = 1000)
            : this(dataStr, frequency, false)
        {
            ScaleData(newHeight);
        }

        #region Public methods
        /// <summary>
        /// Scales EKG data to specified height
        /// </summary>
        /// <param name="newHeight">Height to scale the data to</param>
        public unsafe void ScaleData(int newHeight)
        {
            MaxPossibleValue = newHeight / 2;
            fixed (int* rawDataPtr = RawData, dataPtr = Data)
            {
                if (CounterLines == null)
                    for (int i = 0; i < Data.Length; ++i)
                    {
                        int currValue = (int)(MaxPossibleValue * rawDataPtr[i] / MaxRawValue);
                        dataPtr[i] = MaxPossibleValue - currValue;
                    }

                else
                {
                    for (int i = 0; i < Data.Length - 1; ++i)
                    {
                        int currValue = (int)(MaxPossibleValue * rawDataPtr[i] / MaxRawValue);
                        CounterLines[i].Y1 = dataPtr[i] = MaxPossibleValue - currValue;

                        currValue = (int)(MaxPossibleValue * rawDataPtr[i + 1] / MaxRawValue);
                        CounterLines[i].Y2 = dataPtr[i + 1] = MaxPossibleValue - currValue;
                    }
                }
            }
        }

        public long ScaleSingleData(long data)
        {
            return (long)(MaxPossibleValue * data / MaxRawValue);
        }

        /// <summary>
        /// Process the next steps to the data
        /// </summary>
        public void Process()
        {
            if (NextStep == Processing.Smoothing)
            {
                for (int i = 0; i < 5; ++i)
                    Smoothing();
                NextStep = Processing.SelectingDirections;
            }
            else if (NextStep == Processing.SelectingDirections)
            {
                TagDirections();
                SetDirectionLines();
                NextStep = Processing.SelectingMergeArea;
            }
            else if (NextStep == Processing.SelectingMergeArea)
            {
                TagMergingArea();
                NextStep = Processing.Merging;
            }
            else if (NextStep == Processing.Merging)
            {
                Merge();
                //SetDirectionLines();
                NextStep = Processing.SelectAreasWSmallDiff;
            }
            else if (NextStep == Processing.SelectAreasWSmallDiff)
            {
                TagAreasWithSmallDifferences();
                NextStep = Processing.Done;
            }
            ScaleData(MaxPossibleValue * 2);
        }
        private enum Processing { Smoothing, SelectingDirections, SelectingMergeArea, Merging, SelectAreasWSmallDiff, SelectingCharacteristicPoints, Done };
        private Processing NextStep = Processing.Smoothing;
        #endregion

        #region Further processing steps
        private enum Area { Increasing, Decreasing, Merge, SmallDiff };
        private Area[] Areas;

        /// <summary>
        /// Smoothes EKG 'times' times by averaging (SmoothParam * Frequency) neighbours from point's each side
        /// </summary>
        private unsafe void Smoothing()
        {
            const double SmoothParam = 0.005;
            int maxNeighbours = (int)(SmoothParam * Frequency);

            int[] newData = new int[RawData.Length];

            // Pins Data and newData pointers, so GC won't move them
            // Operating on pointers instead of arrays is faster, because we won't waste time on ArrayOutOfBounds checks
            fixed (int* dataPtr = RawData, newDataPtr = newData)
            {
                // Counting average of neighbours
                for (int i = 0; i < newData.Length; ++i)
                {
                    int sum = 0;
                    int neighboursCount = 0;
                    for (int j = -maxNeighbours; j < maxNeighbours; ++j)
                    {
                        // If index is out of range continue the loop
                        if (i + j < 0 || i + j > newData.Length)
                            continue;

                        sum += dataPtr[i + j];
                        ++neighboursCount;
                    }

                    newDataPtr[i] = sum / neighboursCount;
                }

                RawData = newData;
            }
        }


        /// <summary>
        /// Specifies directions in 'Meanings' array for further procesing
        /// </summary>
        private unsafe void TagDirections()
        {
            Areas = new Area[Data.Length];
            fixed (Area* areasPtr = Areas)
            fixed (int* rawDataPtr = RawData)
                for (int i = 0; i < Data.Length - 1; ++i)
                    areasPtr[i] = rawDataPtr[i] > rawDataPtr[i + 1] ? Area.Decreasing : Area.Increasing;
        }

        /// <summary>
        /// Uses 'Areas' array to set direction lines in 'CounterLines' array
        /// Red line - increasing, blue line - decreasing
        /// Use it ONLY after TagDirections() was called (since it uses its result).
        /// </summary>
        private unsafe void SetDirectionLines()
        {
            if (Areas == null)
                throw new InvalidOperationException("You have to call TaggingDirections() before calling GetDirectionLines()");

            CounterLines = new Line[Areas.Length - 1];

            fixed (int* rawDataPtr = RawData)
            fixed (Area* areasPtr = Areas)
                for (int i = 0; i < CounterLines.Length; ++i)
                {
                    CounterLines[i] = new Line()
                    {
                        StrokeThickness = 1,
                        //Y1 = rawDataPtr[i],           // Y1 and Y2 is set in ScaleData() method
                        //Y2 = rawDataPtr[i + 1],
                        Stroke = areasPtr[i] == Area.Decreasing ? decreasing : increasing
                    };
                }
        }
        private SolidColorBrush increasing = new SolidColorBrush(Colors.Red);
        private SolidColorBrush decreasing = new SolidColorBrush(Colors.Blue);

        /// <summary>
        /// Specified merging area in 'Areas' array for further processing.
        /// Use it ONLY after TagDirections() was called (since it uses its result).
        /// </summary>
        private unsafe void TagMergingArea()
        {
            SolidColorBrush merging = new SolidColorBrush(Colors.LightPink);

            const double WidthCoefficient = 0.03;
            const double HeightCoefficient = 0.01;

            int maxAreaWidth = (int)(WidthCoefficient * Frequency);
            int maxAreaHeight = (int)(HeightCoefficient * (MaxRawValue - MinRawValue));

            fixed (int* rawDataPtr = RawData)
            fixed (Area* areasPtr = Areas)
            {
                int areaWidth = 1;
                List<int> areaIndexes = new List<int>();
                areaIndexes.Add(0);
                Area area = areasPtr[0];
                long areaMax, areaMin;
                areaMax = areaMin = rawDataPtr[0];

                int i = 1;
                for (; i < RawData.Length; ++i)
                {
                    if (areasPtr[i] == area)
                    {
                        if (rawDataPtr[i] > areaMax)
                            areaMax = rawDataPtr[i];
                        else if (rawDataPtr[i] < areaMin)
                            areaMin = rawDataPtr[i];
                        ++areaWidth;
                        areaIndexes.Add(i);
                    }
                    else
                    {
                        // If this is merge area, tagging it as "Merge"
                        if (areaWidth < maxAreaWidth && (areaMax - areaMin) < maxAreaHeight)
                            foreach (int index in areaIndexes)
                            {
                                areasPtr[index] = Area.Merge;
                                CounterLines[index].Stroke = merging;
                            }

                        // Resetting values
                        area = areasPtr[i];
                        areaMax = areaMin = rawDataPtr[i];
                        areaWidth = 1;
                        areaIndexes.Clear();
                        areaIndexes.Add(i);
                    }
                }

                // Checking last area
                if (areaWidth < maxAreaWidth && (areaMax - areaMin) < maxAreaHeight)
                    foreach (int index in areaIndexes)
                    {
                        areasPtr[index] = Area.Merge;
                        CounterLines[index].Stroke = merging;
                    }
            }
        }

        /// <summary>
        /// Merges the areas
        /// 
        /// </summary>
        private unsafe void Merge()
        {
            Area lastArea = Areas[0];

            List<int> mergeArea = new List<int>();

            fixed (Area* areasPtr = Areas)
            {
                for (int i = 1; i < Areas.Length; ++i)
                {
                    // If its merge area, add to list
                    if (areasPtr[i] == Area.Merge)
                        mergeArea.Add(i);
                    else
                    {
                        // If we are after merge area, check if the left area and right were the same, if so merge it.
                        if (mergeArea.Count != 0 && lastArea == areasPtr[i])
                        {
                            foreach (int index in mergeArea)
                            {
                                areasPtr[index] = lastArea;
                                CounterLines[index].Stroke = lastArea == Area.Increasing ? increasing : decreasing;
                            }

                            mergeArea.Clear();
                        }
                        lastArea = areasPtr[i];
                    }
                }

                fixed (int* rawDataPtr = RawData)
                {
                    Area rightArea = Area.Merge;
                    bool mergeSoon = false;
                    long maxLeft = -999999;
                    long minLeft = 999999;
                    long minRight = minLeft;
                    long maxRight = maxLeft;

                    // Second merging step
                    for (int i = 1; i < Areas.Length; ++i)
                    {
                        // If its merge area, add to list
                        if (areasPtr[i] == Area.Merge)
                            mergeArea.Add(i);
                        else
                        {
                            // If there were no merge points, that means we are on the "left area" from the next merge area
                            if (mergeArea.Count == 0)
                            {
                                if (minLeft > rawDataPtr[i])
                                    minLeft = rawDataPtr[i];
                                else
                                    maxLeft = rawDataPtr[i];
                            }
                            // If there were merge points - we are on the right side
                            else if (!mergeSoon)
                            {
                                rightArea = areasPtr[i];
                                if (minRight > rawDataPtr[i])
                                    minRight = rawDataPtr[i];
                                else
                                    maxRight = rawDataPtr[i];
                            }
                            else // mergeSoon
                            {
                                if (rightArea == areasPtr[i])
                                {
                                    if (minRight > rawDataPtr[i])
                                        minRight = rawDataPtr[i];
                                    else
                                        maxRight = rawDataPtr[i];
                                }
                                else
                                {
                                    if (maxRight - minRight > maxLeft - minLeft)
                                        rightArea = rightArea == Area.Decreasing ? Area.Increasing : Area.Decreasing;

                                    foreach (int index in mergeArea)
                                    {
                                        areasPtr[index] = rightArea;
                                        CounterLines[index].Stroke = rightArea == Area.Increasing ? increasing : decreasing;
                                    }
                                    mergeArea.Clear();
                                }
                            }

                            lastArea = areasPtr[i];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Uses 'Data' and 'Meanings' arrays to tag areas with small differences
        /// The small diff areas are marked with lime color
        /// Use it ONLY after Merge() was called
        /// (since it's using Merge() result)
        /// </summary>
        private unsafe void TagAreasWithSmallDifferences()
        {

            fixed (Area* areasPtr = Areas)
            fixed (int* rawDataPtr = RawData)
            {
                IList<double> avgLengthInAreas = new List<double>();
                #region Calculating average length between points in areas
                // List of average length between points in areas
                double sum = 0;
                long pointNum = 0;
                for (int i = 0; i < Areas.Length - 1; ++i)
                {
                    sum += Math.Abs(rawDataPtr[i] - rawDataPtr[i + 1]);
                    ++pointNum;

                    if (i == Areas.Length - 2 || areasPtr[i] != areasPtr[i + 1])
                    {
                        avgLengthInAreas.Add(sum / pointNum);
                        sum = 0;
                        pointNum = 0;
                    }
                }
                #endregion

                #region Tagging small difference areas
                SolidColorBrush smallDiff = new SolidColorBrush(Colors.Lime);
                const int neighboursCount = 5;
                int areaNum = 0;
                for (int i = 1; i < Areas.Length - 2; ++i)
                {
                    if (areasPtr[i] != areasPtr[i + 1])
                    {
                        CounterLines[i + 1].Stroke = CounterLines[i].Stroke = smallDiff;
                        ++areaNum;
                    }
                    else
                    {
                        long lengthBetweenNeighbours = 0;
                        for (int j = -neighboursCount; j < neighboursCount + 1; ++j)
                            if (i + j > 0 && i + j + 1 < Areas.Length)
                                lengthBetweenNeighbours += Math.Abs(rawDataPtr[i + j] - rawDataPtr[i + j + 1]);
                        if (lengthBetweenNeighbours < 0.3 * avgLengthInAreas[areaNum])
                        {
                            CounterLines[i].Stroke = new SolidColorBrush(Colors.Lime); // TODO CHECK
                            areasPtr[i] = Area.SmallDiff;
                        }
                    }
                }
                #endregion
            }

        }

        /// <summary>
        /// This metod simply doesn't work as expected :)
        /// Marks characteristic points on the plot
        /// </summary>
        //private unsafe void TagCharacteristicPoints()
        //{
        //    CounterLines = null;
        //    CharacteristicPoints = new Ellipse[Data.Length];
        //    var orange = new SolidColorBrush(Colors.Orange);

        //    long maxThreshold = (long)(0.8 * ScaleSingleData(MaxRawValue));
        //    long minThreshold = (long)(0.8 * ScaleSingleData(MinRawValue));

        //    fixed (Area* areasPtr = Areas)
        //    fixed (int* rawDataPtr = RawData)
        //    {
        //        for (int i = 0; i < Data.Length; ++i)
        //        {
        //            if (areasPtr[i] == Area.SmallDiff && (rawDataPtr[i] > maxThreshold || rawDataPtr[i] < minThreshold))
        //                CharacteristicPoints[i] = new Ellipse()
        //                            {
        //                                Width = 2,
        //                                Height = 2,
        //                                Stroke = orange
        //                            };
        //        }
        //    }

        //    //const int areaLookupSize = 5;
        //    //fixed (Area* areasPtr = Areas)
        //    //fixed (long* dataPtr = Data)
        //    //    for (int i = areaLookupSize; i < Areas.Length - areaLookupSize; ++i)
        //    //    {
        //    //        long leftSum = 0;
        //    //        long rightSum = 0;
        //    //        int leftCount = 0;
        //    //        int rightCount = 0;
        //    //        long leftMin = dataPtr[i - areaLookupSize];
        //    //        long leftMax = dataPtr[i - areaLookupSize];
        //    //        long rightMin = dataPtr[i - areaLookupSize];
        //    //        long rightMax = dataPtr[i - areaLookupSize];
        //    //        if (areasPtr[i] == Area.SmallDiff)
        //    //        {
        //    //            for (int j = -areaLookupSize; j < 0; ++j)
        //    //            {
        //    //                if (i + j > 0)
        //    //                {
        //    //                    leftSum += dataPtr[i + j];
        //    //                    ++leftCount;
        //    //                    if (leftMin > dataPtr[i + j])
        //    //                        leftMin = dataPtr[i + j];
        //    //                    else if (leftMax < dataPtr[i + j])
        //    //                        leftMax = dataPtr[i + j];
        //    //                }
        //    //            }
        //    //            for (int j = 1; j < areaLookupSize + 1; ++j)
        //    //            {
        //    //                if (i + j < Areas.Length)
        //    //                {
        //    //                    if (i == 22089)
        //    //                        i = i;
        //    //                    rightSum += dataPtr[i + j];
        //    //                    ++rightCount;
        //    //                    if (rightMin > dataPtr[i + j])
        //    //                        rightMin = dataPtr[i + j];
        //    //                    else if (rightMax < dataPtr[i + j])
        //    //                        rightMax = dataPtr[i + j];
        //    //                }
        //    //            }
        //    //            if ((leftSum / leftCount < dataPtr[i] && rightSum / rightCount < dataPtr[i] && leftMax < dataPtr[i] && rightMax < dataPtr[i])
        //    //            || (leftSum / leftCount > dataPtr[i] && rightSum / rightCount > dataPtr[i] && leftMin > dataPtr[i] && rightMin > dataPtr[i]))
        //    //                CharacteristicPoints[i] = new Ellipse()
        //    //                        {
        //    //                            Width = 2,
        //    //                            Height = 2,
        //    //                            Stroke = new SolidColorBrush(Colors.Orange)
        //    //                        };
        //    //        }
        //    //    }
        //}


        #endregion

        public int this[int index]
        {
            get
            {
                return Data[index];
            }
        }

        #region Properties
        /// <summary>
        /// Returns data count
        /// </summary>
        public int Count
        {
            get
            {
                return Data.Length;
            }
        }

        /// <summary>
        /// EKG data after scaling
        /// </summary>
        public int[] Data { get; private set; }

        /// <summary>
        /// Raw EKG data (before scaling)
        /// </summary>
        public int[] RawData { get; private set; }

        /// <summary>
        /// Array containing data points connected to lines
        /// in colors, which shows area behavior/purpose
        /// Red - increasing, Blue - decreasing, Light pink - merging, Lime - low difference area
        /// </summary>
        public Line[] CounterLines { get; private set; }

        /// <summary>
        /// Array containing characteristic data points as ellipses
        /// </summary>
        public Ellipse[] CharacteristicPoints { get; private set; }

        /// <summary>
        /// Maximum not scaled data value
        /// </summary>
        public long MaxRawValue { get; private set; }

        /// <summary>
        /// Minimum not scaled data value
        /// </summary>
        public long MinRawValue { get; private set; }

        /// <summary>
        /// Maximum scaled data value
        /// </summary>
        public int MaxPossibleValue { get; private set; }

        /// <summary>
        /// Data frequency
        /// </summary>
        public int Frequency { get; private set; }
        #endregion
    }
}
