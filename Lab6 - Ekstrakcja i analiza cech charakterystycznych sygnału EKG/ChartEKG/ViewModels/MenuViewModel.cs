﻿using Caliburn.Micro;
using ChartEKG.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Controls;
using Windows.System.Threading;
using Windows.ApplicationModel.Resources;
using ChartEKG.Models;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using Windows.UI.Xaml;

namespace ChartEKG.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this._navigationService = navigationService;
            StartStopIcon = new SymbolIcon();
            StartStopIcon.Symbol = Symbol.Play;
            Dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;
            Resources = new Windows.ApplicationModel.Resources.ResourceLoader();
            DataLoaded = false;
            Speed = 10;

            //PolyZero.Opacity = 0.5;
            PolyZero.Stroke = new SolidColorBrush(Windows.UI.Colors.White);
            PolyZero.StrokeThickness = 1;

            PolyPlot.Opacity = 0.5;
            PolyPlot.Stroke = new SolidColorBrush(Windows.UI.Colors.Lime);
            PolyPlot.StrokeThickness = 1;

            ProgressRingActive = false;
        }

        protected override void OnViewAttached(object view, object context)
        {
            CanvasC = (Canvas)(((MenuView)view).FindName("Canvas"));
            base.OnViewAttached(view, context);
        }

        #region UI binds
        /// <summary>
        /// Let user load EKG data from text file
        /// This method is bound to load file/data button
        /// </summary>
        public async void LoadText()
        {
            #region Picking file
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.List;
            FOP.FileTypeFilter.Add(".txt");
            FOP.FileTypeFilter.Add(".ascii");
            FOP.FileTypeFilter.Add(".dat");
            FOP.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            StorageFile file = await FOP.PickSingleFileAsync();

            if (file == null)
                return;

            ProgressRingActive = true;
            String txt;
            try
            {
                txt = await FileIO.ReadTextAsync(file);
                Data = new EKGData(txt, (int)CanvasHeight, 1000);
            }
            catch (Exception e)
            {
                new Windows.UI.Popups.MessageDialog(Resources.GetString("ReadFail_Msg") + e.Message, Resources.GetString("Error")).ShowAsync();
                return;
            }
            #endregion


            ProgressRingActive = false;
            // plotting
            ReplotStandardLine();
            StartDataIndex = 0;
            Replot(StartDataIndex);

            // showing upper and lower bounds
            ShowMinMaxValues();

            DataLoaded = true;
        }

        /// <summary>
        /// Moves plot to the back. The move length is determined by 'Speed' property.
        /// </summary>
        public void GoLeft()
        {
            StartDataIndex -= Speed;
            if (StartDataIndex <= 0)
                StartDataIndex = 0;
            Replot(StartDataIndex);
        }

        /// <summary>
        /// Moves plot forward. The move length is determined by 'Speed' property.
        /// </summary>
        public void GoRight()
        {
            StartDataIndex += Speed;
            if (StartDataIndex > Data.Count - Speed)
                StartDataIndex = Data.Count - Speed - 1;
            Replot(StartDataIndex);
        }

        /// <summary>
        /// Starts/stops automatic plot moving.
        /// </summary>
        public async void StartStop()
        {
            StartStopIcon.Symbol = PlotStarted ? Symbol.Play : Symbol.Pause;
            PlotStarted = !PlotStarted;

            if (PlotStarted)
            {
                while (PlotStarted)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => { GoRight(); });
                    await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(75));
                }
            }
        }

        /// <summary>
        /// Event fired when canvas size was changed, to rescale and redraw the plot
        /// </summary>
        /// <param name="Args">Argument containing new size</param>
        public void GetCanvasSize(Windows.UI.Xaml.SizeChangedEventArgs Args)
        {
            CanvasWidth = (long)Args.NewSize.Width;
            CanvasHeight = (long)Args.NewSize.Height;
            if (DataLoaded)
            {
                Data.ScaleData((int)CanvasHeight);
                Replot(StartDataIndex);
                ReplotStandardLine();
            }
        }

        /// <summary>
        /// Resets the plot (rewinds it to the starting state)
        /// </summary>
        public void ResetPlot()
        {
            Replot(StartDataIndex = 0);
        }

        /// <summary>
        /// Process the next steps to the plot
        /// </summary>
        public async void Process()
        {
            ProgressRingActive = true;
            await Task.Delay(25);
            Data.Process();
            await Task.Delay(25);
            ProgressRingActive = false;
            Replot(StartDataIndex);

            if (Data.CharacteristicPoints != null)
                DataLoaded = false;
        }
        #endregion

        #region Implementation details
        private void ShowMinMaxValues()
        {
            MaxValueStr = Data.MaxRawValue.ToString();
            MinValueStr = (-Data.MaxRawValue).ToString();
        }

        /// <summary>
        /// Redraws Data to the plot starting from dataIndex
        /// </summary>
        /// <param name="dataIndex">Data index to start plot from</param>
        private unsafe void Replot(int dataIndex)
        {
            CanvasC.Children.Clear();
            PolyPlot.Points.Clear();
            CanvasC.Children.Add(PolyZero);

            fixed (int* dataPtr = Data.Data)
            {
                if (Data.CounterLines == null && Data.CharacteristicPoints == null)
                {
                    for (int tmpIndex = dataIndex, PlotIndex = 0; PlotIndex < CanvasWidth + Interval - 1 && tmpIndex + PlotIndex < Data.Count; PlotIndex += Interval, tmpIndex += 1)
                        PolyPlot.Points.Add(new Point(PlotIndex, dataPtr[tmpIndex]));

                    CanvasC.Children.Add(PolyPlot);
                }
                else if (Data.CharacteristicPoints != null)
                {
                    for (int tmpIndex = dataIndex, PlotIndex = 0; PlotIndex < CanvasWidth + Interval - 1 && tmpIndex + PlotIndex < Data.Count; PlotIndex += Interval, tmpIndex += 1)
                    {
                        if (Data.CharacteristicPoints[tmpIndex] != null)
                        {
                            Canvas.SetLeft(Data.CharacteristicPoints[tmpIndex], PlotIndex);
                            Canvas.SetTop(Data.CharacteristicPoints[tmpIndex], dataPtr[tmpIndex]);
                            CanvasC.Children.Add(Data.CharacteristicPoints[tmpIndex]);
                        }
                        PolyPlot.Points.Add(new Point(PlotIndex, dataPtr[tmpIndex]));
                    }
                    CanvasC.Children.Add(PolyPlot);
                }
                else
                    for (PlotIndex = 0; PlotIndex < CanvasWidth + Interval - 1 && dataIndex + PlotIndex < Data.Count; PlotIndex += Interval, dataIndex += 1)
                    {
                        Line addLine = Data.CounterLines[dataIndex];
                        addLine.X1 = PlotIndex;
                        addLine.X2 = PlotIndex + Interval;
                        CanvasC.Children.Add(addLine);
                    }
            }
        }

        private void ReplotStandardLine()
        {
            PolyZero.Points.Clear();
            PolyZero.Points.Add(new Point(0, CanvasHeight / 2));
            PolyZero.Points.Add(new Point(CanvasWidth, CanvasHeight / 2));

            if (CanvasC.Children.ElementAt(0) != PolyZero)    // if it isnt in canvas
                CanvasC.Children.Add(PolyZero);
        }
        #endregion

        #region Fields
        Polyline PolyZero = new Polyline();
        Polyline PolyPlot = new Polyline();

        private const int Interval = 2;
        private Windows.UI.Core.CoreDispatcher Dispatcher;
        private int StartDataIndex;
        private int PlotIndex;
        private long CanvasWidth;
        private long CanvasHeight;
        private EKGData Data;
        private ResourceLoader Resources;
        private bool PlotStarted = false;

        private SolidColorBrush Increasing = new SolidColorBrush(Colors.Red);
        private SolidColorBrush Decreasing = new SolidColorBrush(Colors.Blue);

        /// <summary>
        /// Canvas control, it's taken from view and used in plotting
        /// </summary>
        Canvas CanvasC;
        #endregion

        #region Properties
        private bool _ProgressRingActive;
        public bool ProgressRingActive
        {
            get { return _ProgressRingActive; }
            set
            {
                _ProgressRingActive = value;
                NotifyOfPropertyChange(() => ProgressRingActive);
            }
        }

        private SymbolIcon _StartStopIcon;
        public SymbolIcon StartStopIcon
        {
            get { return _StartStopIcon; }
            set
            {
                _StartStopIcon = value;
                NotifyOfPropertyChange(() => StartStopIcon);
            }
        }

        private bool _DataLoaded;
        public bool DataLoaded
        {
            get { return _DataLoaded; }
            set
            {
                _DataLoaded = value;
                NotifyOfPropertyChange(() => DataLoaded);
            }
        }

        private String _MaxValueStr;
        public String MaxValueStr
        {
            get { return _MaxValueStr; }
            set
            {
                _MaxValueStr = value;
                NotifyOfPropertyChange(() => MaxValueStr);
            }
        }

        private String _MinValueStr;
        public String MinValueStr
        {
            get { return _MinValueStr; }
            set
            {
                _MinValueStr = value;
                NotifyOfPropertyChange(() => MinValueStr);
            }
        }

        private int _Speed;
        public int Speed
        {
            get { return _Speed; }
            set
            {
                _Speed = value;
                NotifyOfPropertyChange(() => Speed);
            }
        }
        #endregion
    }
}
