﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eye.Models
{
    public struct CoordPoint
    {
        public CoordPoint(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public int X;
        public int Y;
    }

}
