﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace Eye.Models.ImageEffects
{
    /// <summary>
    /// Applies specified points to the image with specified color
    /// </summary>
    public class ApplyPoints : IImageEffect
    {
        public ApplyPoints(IList<CoordPoint> CP, Color C)
        {
            this.CP = CP;
            this.C = C;
        }

        private IList<CoordPoint> CP;
        private Color C;

        public string Name { get { return "Color points"; } }

        public string Description { get { return "Colors specified points with specified color"; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            foreach(CoordPoint cp in CP)
                image.SetRGB(cp.X, cp.Y, C.R, C.G, C.B);
            return image;
        }
    }
}
