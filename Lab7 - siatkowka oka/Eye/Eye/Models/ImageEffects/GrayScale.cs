﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eye.Models.ImageEffects
{
    public class GrayScale : IImageEffect
    {
        public string Name { get { return "Gray scale"; } }

        public string Description { get { return "Converts image into black & white"; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            for (int index = 0; index < image.ByteArray.Length; index += 4)
                image.ByteArray[index] = image.ByteArray[index + 1] = image.ByteArray[index + 2]
                    = (byte)((image.ByteArray[index] + image.ByteArray[index + 1] + image.ByteArray[index + 2]) / 3);

            return image;
        }
    }
}
