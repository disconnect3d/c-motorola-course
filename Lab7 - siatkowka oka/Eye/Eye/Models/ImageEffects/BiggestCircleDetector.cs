﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace Eye.Models.ImageEffects
{
    public class BiggestCircleDetector : IImageEffect
    {
        public BiggestCircleDetector()
        {
            this.C = Colors.White;
            Circle = new List<CoordPoint>();
        }

        public BiggestCircleDetector(Color C)
        {
            this.C = C;
            Circle = new List<CoordPoint>();
        }

        private Color C;
        public string Name { get { return "Detect biggest circle"; } }

        public string Description { get { return "Detects the biggest circle on the image and tags it with specified color (default is green)"; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            ImageWrapper copy = new ImageWrapper(image);
            List<List<CoordPoint>> imageObjects = new List<List<CoordPoint>>();

            // Searching objects on the image
            for (int y = 0; y < image.Height; ++y)
                for (int x = 0; x < image.Width; ++x)
                    if (copy[x, y, 0] == ImageWrapper.BLACK)
                        imageObjects.Add(GetImageObject(x, y, copy));

            if (imageObjects.Count == 0)
                return image;

            List<CoordPoint> biggestObj = imageObjects.OrderBy(x => x.Count).LastOrDefault();
            imageObjects.Clear();

            int minX = biggestObj[0].X;
            int maxX = biggestObj[0].X;
            int minY = biggestObj[0].Y;
            int maxY = biggestObj[0].Y;
            foreach(CoordPoint cp in biggestObj)
            {
                if (minX > cp.X)
                    minX = cp.X;
                else if (maxX < cp.X)
                    maxX = cp.X;

                if (minY > cp.Y)
                    minY = cp.Y;
                else if (maxY < cp.Y)
                    maxY = cp.Y;
            }

            int circleRadius = (maxY - minY) > (maxX - minX) ? (maxY - minY)/2 : (maxX - minX)/2;
            int middleX = (maxX + minX) / 2;
            int middleY = (maxY + minY) / 2;

            Circle.Clear();
            for (int y = 0; y < image.Height; ++y)
                for (int x = 0; x < image.Width; ++x)
                    if ((int)Math.Sqrt(Math.Pow(x - middleX, 2) + Math.Pow(y - middleY, 2)) == circleRadius)
                    {
                        image.SetRGB(x, y, C.R, C.G, C.B);
                        Circle.Add(new CoordPoint(x, y));
                    }

            return image;
        }

        public List<CoordPoint> Circle;

        private List<CoordPoint> GetImageObject(int x, int y, ImageWrapper image)
        {
            List<CoordPoint> result = new List<CoordPoint>();

            List<CoordPoint> helper = new List<CoordPoint>();
            image[x, y, 0] = 1;

            CoordPoint firstPoint = new CoordPoint(x,y);
            helper.Add(firstPoint);
            result.Add(firstPoint);

            while (helper.Count != 0)
            {
                CoordPoint popped = helper.ElementAt(0);
                helper.RemoveAt(0);

                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if ((i == 0 && j == 0) || popped.X + j < 0 || popped.X + j == image.Width || popped.Y + i < 0 || popped.Y + i == image.Height)
                            continue;
                        if (image[popped.X + j, popped.Y + i, 0] == ImageWrapper.BLACK)
                        {
                            image[popped.X + j, popped.Y + i, 0] = 1;
                            CoordPoint add = new CoordPoint(popped.X + j, popped.Y + i); 
                            helper.Add(add);
                            result.Add(add);
                        }
                    }
            }

            return result;
        }
    }
}
