﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eye.Models.ImageEffects
{
    public class Threshold : IImageEffect
    {
        public Threshold(DoItFor Option)
        {
            this.Option = Option;
        }


        public enum DoItFor { Pupil, Iris };

        private DoItFor Option;
        private static readonly double PupilCoefficient = 4.5; // źrenica
        private static readonly double IrisCoefficient = 1.5; // tęczówka

        public string Name { get { return "Threshold"; } }

        public string Description { get { return String.Empty; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            double sum = 0;
            for (int h = 0; h < image.Height; ++h)
                for (int w = 0; w < image.Width; ++w)
                    sum += image[w, h, 0];

            byte thresholdValue = ImageWrapper.CutColorRange((long)(
                sum / 
                    ((double)image.Height *
                    (double)image.Width * 
                    (Option == DoItFor.Pupil? PupilCoefficient : IrisCoefficient)
                    )
                ));

            for (int h = 0; h < image.Height; ++h)
                for (int w = 0; w < image.Width; ++w)
                    image.SetRGB(w,h, image[w, h, 0] < thresholdValue ? ImageWrapper.BLACK : ImageWrapper.WHITE);

            return image;
        }
    }
}
