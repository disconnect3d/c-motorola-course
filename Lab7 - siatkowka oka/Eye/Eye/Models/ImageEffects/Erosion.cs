﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eye.Models.ImageEffects
{
    public class Erosion : IImageEffect
    {
        public string Name { get { return "Erosion"; } }

        public string Description { get { return "Makes erosion to the image"; } }

        private static readonly int[,] mask = new int[3, 3]
        {
            {1, 1, 1},
            {1, 0, 1},
            {1, 1, 1},
        };

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            // Just in case someone ever wants to change the mask somehow
            //if (mask.Length != mask[0].Length)
            //    throw new ArgumentException("Mask dimensions must be equal!");

            int maskLength = 1;

            ImageWrapper result = new ImageWrapper(image);
            for (int h = maskLength; h < image.Height - maskLength; ++h)
                for (int w = maskLength; w < image.Width - maskLength; ++w)
                {
                    for (int x = -maskLength; x <= maskLength; ++x)
                        for (int y = -maskLength; y <= maskLength; ++y)
                        {
                            if (y == 0 && x == 0)
                                continue;
                            if (mask[x+1, y+1] == 1 && image[w+x, h+y, 0] == ImageWrapper.WHITE)
                            {
                                result.SetRGB(w, h, ImageWrapper.WHITE);
                                goto EndWorkOnThisPixel;
                            }
                        }
                EndWorkOnThisPixel:
                    continue;
                }
            return result;
        }
    }
}
