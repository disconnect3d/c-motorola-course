﻿using Caliburn.Micro;
using Eye.Models;
using System;
using System.IO;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.UI;
using Eye.Models.ImageEffects;
using System.Collections.Generic;
using Windows.UI.Popups;

namespace Eye.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;

        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.navigationService = navigationService;

        }

        /// <summary>
        /// Lets user load image from a file by showing a popup "choose a file"
        /// </summary>
        public async void LoadImage()
        {
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.Thumbnail;
            FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            FOP.FileTypeFilter.Add(".bmp");
            FOP.FileTypeFilter.Add(".jpg");
            FOP.FileTypeFilter.Add(".png");
            FOP.FileTypeFilter.Add(".jpeg");

            StorageFile file = await FOP.PickSingleFileAsync();
            if (file != null)
                LoadFile(file);
        }

        /// <summary>
        /// Loads image from a StorageFile
        /// This function doesn't check if file == null
        /// </summary>
        /// <param name="file">StorageFile to load image from</param>
        private async void LoadFile(StorageFile file)
        {
            fileStream = await file.OpenAsync(FileAccessMode.Read);

            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    decoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".bmp":
                    decoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".png":
                    decoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".gif":
                    decoderId = BitmapDecoder.GifDecoderId;
                    break;
            }
            decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
            pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,    // byte array format: Blue Green Red Alpha [8 bit values: 0-255]
                BitmapAlphaMode.Straight,   // How to code Alpha channel
                new BitmapTransform(),
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage);

            OriginalImage = new ImageWrapper(pixelData.DetachPixelData(), decoder.PixelWidth, decoder.PixelHeight);

            ImageWriteableBitmap = new WriteableBitmap((int)OriginalImage.Width, (int)OriginalImage.Height);
            await ImageWriteableBitmap.SetSourceAsync(fileStream);

            CurrentImage = new ImageWrapper(OriginalImage);
        }

        /// <summary>
        /// Captures image from a camera.
        /// This function WAS NOT TESTED, so MAY BE BUGGY!
        /// </summary>
        async private void CaptureImage()
        {
            CameraCaptureUI cameraDlg = null;
            bool result = true;
            String err = String.Empty;
            try
            {
                cameraDlg = new CameraCaptureUI();
                cameraDlg.PhotoSettings.AllowCropping = true;
                cameraDlg.PhotoSettings.MaxResolution = CameraCaptureUIMaxPhotoResolution.MediumXga;

            }
            catch (Exception e)
            {
                result = false;
                err = e.Message;
            }

            if (!result)
            {
                await (new MessageDialog("Error initializing camera: " + err).ShowAsync());
                return;
            }

            Windows.Storage.StorageFile capturedMediaFile = await cameraDlg.CaptureFileAsync(CameraCaptureUIMode.Photo);

            LoadFile(capturedMediaFile);
        }

        /// <summary>
        /// Processes the next detection step onto the loaded image
        /// </summary>
        public async void Process()
        {
            ProcessEnabled = false;
            const int LowTime = 50;
            const int HighTime = 400;
            if (Now == Steps.ThresholdIris)
            {
                if (!CurrentImage.IsInGrayScale())
                {
                    ApplyEffect(new GrayScale());
                    await Task.Delay(LowTime);
                }
                ApplyEffect(new Threshold(Threshold.DoItFor.Iris));
            }
            else if (Now == Steps.ErosionIris)
            {
                for (int i = 0; i < 4; ++i)
                {
                    ApplyEffect(new Erosion());
                    await Task.Delay(LowTime);
                }
            }
            else if (Now == Steps.DetectIris)
            {
                BiggestCircleDetector bcd = new BiggestCircleDetector(Colors.Lime);
                ApplyEffect(bcd);
                BigCirclePoints = bcd.Circle;
            }
            else if (Now == Steps.ThresholdPupil)
            {
                ResetToOriginal();
                await Task.Delay(HighTime);

                if (!CurrentImage.IsInGrayScale())
                {
                    ApplyEffect(new GrayScale());
                    await Task.Delay(LowTime);
                }
                ApplyEffect(new Threshold(Threshold.DoItFor.Pupil));
            }
            else if (Now == Steps.ErosionPupil)
            {
                for (int i = 0; i < 1; ++i)
                {
                    ApplyEffect(new Erosion());
                    await Task.Delay(LowTime);
                }
            }
            else if (Now == Steps.DetectPupil)
            {
                BiggestCircleDetector bcd = new BiggestCircleDetector(Colors.Red);
                ApplyEffect(bcd);
                SmallCirclePoints = bcd.Circle;
            }
            else if (Now == Steps.ApplyToImage)
            {
                ResetToOriginal();
                await Task.Delay(HighTime);
                ApplyEffect(new ApplyPoints(BigCirclePoints, Colors.Lime));
                await Task.Delay(HighTime);
                ApplyEffect(new ApplyPoints(SmallCirclePoints, Colors.Red));
            }

            if (Now != Steps.Done)
            {
                ++Now;
                ProcessEnabled = true;
            }
            else
                ProcessEnabled = false;
        }
        List<CoordPoint> SmallCirclePoints;
        List<CoordPoint> BigCirclePoints;

        private Steps Now;
        private enum Steps { ThresholdIris, ErosionIris, DetectIris, ThresholdPupil, ErosionPupil, DetectPupil, ApplyToImage, Done }

        /// <summary>
        /// Resets the showing image to the original loaded one
        /// </summary>
        public void ResetToOriginal()
        {
            if (OriginalImage == null)
                return;

            ImageWriteableBitmap = new WriteableBitmap((int)OriginalImage.Width, (int)OriginalImage.Height);

            using (Stream stream = ImageWriteableBitmap.PixelBuffer.AsStream())
                stream.Write(OriginalImage.ByteArray, 0, OriginalImage.ByteArray.Length);

            CurrentImage = new ImageWrapper(OriginalImage);
        }

        /// <summary>
        /// Applies effect to the image
        /// </summary>
        /// <param name="effect">Effect to be applied</param>
        private void ApplyEffect(IImageEffect effect)
        {
            if (fileStream == null)
                return;

            ImageWrapper result = effect.ApplyEffect(CurrentImage);

            if (result == null)
                return;
            else
                CurrentImage = result;

            ImageWriteableBitmap = new WriteableBitmap((int)CurrentImage.Width, (int)CurrentImage.Height);

            using (Stream stream = ImageWriteableBitmap.PixelBuffer.AsStream())
                stream.Write(CurrentImage.ByteArray, 0, CurrentImage.ByteArray.Length);


        }

        #region Fields
        private IRandomAccessStream fileStream = null;
        private Guid decoderId;
        private BitmapDecoder decoder;
        private PixelDataProvider pixelData;
        // private ImageWrapper OriginalImage; --- is actually a property
        private ImageWrapper CurrentImage;
        #endregion

        #region Properties
        private ImageWrapper _OriginalImage;
        private ImageWrapper OriginalImage
        {
            get
            {
                return _OriginalImage;
            }
            set
            {
                _OriginalImage = value;
                Now = Steps.ThresholdIris;  // resetting the processing steps
                ProcessEnabled = true;
            }
        }

        private WriteableBitmap _ImageWriteableBitmap;
        public WriteableBitmap ImageWriteableBitmap
        {
            get { return _ImageWriteableBitmap; }
            set
            {
                _ImageWriteableBitmap = value;
                NotifyOfPropertyChange(() => ImageWriteableBitmap);
            }
        }

        private bool _ProcessEnabled;
        public bool ProcessEnabled
        {
            get { return _ProcessEnabled; }
            set
            {
                _ProcessEnabled = value;
                NotifyOfPropertyChange(() => ProcessEnabled);
            }
        }

        #endregion
    }
}
