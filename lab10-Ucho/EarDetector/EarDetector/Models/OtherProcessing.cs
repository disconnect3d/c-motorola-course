﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using ImagingLib.Effects.Others;
using Windows.UI.Xaml.Media.Imaging;

namespace EarDetector.Models
{
    public static class OtherProcessing
    {
        public static WriteableBitmap Process(WriteableBitmap source, int ObjectColor, int LongLineColor, int PerpendicularLineColor, int CharacteristicPointsColor, int LastColor)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                #region Creating "longest" line - connecting top and bottom points
                CoordPoint top = new CoordPoint(), bot = new CoordPoint();
                int foundCount = 0;
                for (int y = 0; y < context.Height; ++y)
                    for (int x = context.Width - 1; x >= 0; --x)
                        if (context.Pixels[context.IntArrIndex(x, y)] == ObjectColor)
                        {
                            ++foundCount;
                            top.X = x;
                            top.Y = y;
                            goto FOUND_TOP;
                        }
            FOUND_TOP:
                for (int y = context.Height - 1; y >= 0; --y)
                    for (int x = 0; x < context.Width; ++x)
                        if (context.Pixels[context.IntArrIndex(x, y)] == ObjectColor)
                        {
                            ++foundCount;
                            bot.X = x;
                            bot.Y = y;
                            goto FOUND_BOT;
                        }
            FOUND_BOT:
                if (foundCount < 2)
                    throw new ArgumentException("Couldnt find top and bottom points in specified ObjectColor");

                DrawingUtils.LinearFunctionParams funcParams = DrawingUtils.CalcLinearFuncParams(top, bot);
                Func<int, int> foo = new Func<int, int>((x) => (int)(funcParams.a * x + funcParams.b));

                DrawingUtils.DrawLine(context, funcParams, top, bot, LongLineColor);
                #endregion

                #region Marking characteristic points on main line
                bool topIsLeft = top.X < bot.X;
                const int N = 6;

                double diffX = Math.Abs(top.X - bot.X);

                // "small x difference"
                double dx = diffX / N;

                // "small y difference"
                double dy = topIsLeft ? bot.Y - top.Y : top.Y - bot.Y;
                dy /= N;

                // Iterating over "MAIN" line and getting the perpendicular line cross points
                List<CoordPoint> pointsOnLine = new List<CoordPoint>();
                for (double x = topIsLeft ? top.X : bot.X; x <= (topIsLeft ? bot.X : top.X); x += dx)
                    pointsOnLine.Add(new CoordPoint((int)x, foo((int)x)));

                foreach (CoordPoint cp in pointsOnLine)
                    DrawingUtils.DrawSquare(context, cp, 1, CharacteristicPointsColor);
                #endregion

                #region Creating perpendicular lines to the main line crossing on marked points & adding characteristic points to the list
                List<CoordPoint> characteristicPoints = new List<CoordPoint>();

                funcParams.a = DrawingUtils.GetPerpendicularParamA(funcParams.a);

                foreach (CoordPoint cp in pointsOnLine)
                {
                    funcParams.b = DrawingUtils.GetParamB(cp.Y, funcParams.a, cp.X);

                    foo = DrawingUtils.GetLinearDrawingFunc(funcParams);

                    for (int x = 0; x < context.Width; ++x)
                    {
                        int y1 = foo(x);
                        int y2 = foo(x + 1);

                        if (!context.IsInImageBounds(x, y1))
                            continue;

                        int index = context.IntArrIndex(x, y1);
                        CoordPoint p = new CoordPoint(x, y1);
                        if (context.Pixels[index] == ObjectColor || DrawingUtils.ThereIsANeighbour(context, p, 1, ObjectColor))
                            characteristicPoints.Add(p);

                        context.Pixels[index] = PerpendicularLineColor;

                        int yStart = y2 > y1 ? y1 : y2;
                        int yEnd = y2 > y1 ? y2 : y1;

                        for (int yi = yStart + 1; yi < yEnd; ++yi)
                        {
                            if (!context.IsInImageBounds(x, yi))
                                continue;

                            index = context.IntArrIndex(x, yi);
                            p.Y = yi;
                            if (context.Pixels[index] == ObjectColor || 
                                DrawingUtils.ThereIsANeighbour(context, p, 1, ObjectColor) ||
                                DrawingUtils.ThereIsANeighbour(context, p, 1, CharacteristicPointsColor) )
                                characteristicPoints.Add(p);

                            context.Pixels[context.IntArrIndex(x, yi)] = PerpendicularLineColor;
                        }

                    }
                }
                #endregion

                // Removing too close points
                const int reg = 5;
                for (int i = 1; i < characteristicPoints.Count; ++i)
                    if (Math.Abs(characteristicPoints[i].X - characteristicPoints[i - 1].X) <= reg ||
                        Math.Abs(characteristicPoints[i].Y - characteristicPoints[i - 1].Y) <= reg)
                        characteristicPoints.RemoveAt(i--);

                #region Marking characteristic points
                foreach (CoordPoint cp in characteristicPoints)
                {
                    DrawingUtils.DrawLine(context, pointsOnLine[N / 2 + 1], cp, LastColor);
                    DrawingUtils.DrawSquare(context, cp, 2, CharacteristicPointsColor);
                }
                DrawingUtils.DrawLine(context, pointsOnLine[0], pointsOnLine[N/2+1], LastColor);
                DrawingUtils.DrawSquare(context, pointsOnLine[0], 2, CharacteristicPointsColor);
                #endregion
            }
            return source;
        }
    }
}
