﻿using Caliburn.Micro;
using ImagingLib;
using ImagingLib.Effects.EdgeDetection;
using ImagingLib.Effects.Others;
using ImagingLib.Effects.Thresholds;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using ImagingLib.Effects.Thinning;
using ImagingLib.Effects;
using ImagingLib.Effects.Morphology;
using EarDetector.Models;
using Windows.UI.Popups;

namespace EarDetector.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;

        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.navigationService = navigationService;
            ProcessEnabled = false;
        }

        public async void LoadImage()
        {
            Bitmap = await WriteableBitmapFactory.CreateWithFOP();
            OriginalImage = Bitmap.DeepCopy();
            Now = Steps.ChangeColor;
        }

        private enum Steps { ChangeColor, Threshold, Erosion, Dilation, CutFilter, Canny, CutFilter2, LastProcessing };
        private Steps Now;

        private const int EROSIONS = 10;
        private const int DILATIONS = 6;
        private const int CUTFILTER_EROSIONS = 3;


        public async void Process()
        {
            Process(true);
        }


        private async void Process(bool fromGuI)
        {
            //if (fromGuI && !ProcessEnabled)
            //    return;

            ProcessEnabled = false;

            const int LowTime = 5;
            const int CannyTime = 5;
            const int MidTime = 5;

            String errMessage = String.Empty;
            try
            {
                switch (Now)
                {
                    case Steps.ChangeColor:
                        {
                            Bitmap = await new ColorFilter(ColorMask.SkinDetectionGrayscale).ApplyEffect(Bitmap);
                            break;
                        }
                    case Steps.Threshold:
                        {
                            Bitmap = await new Otsu().ApplyEffect(Bitmap);
                            break;
                        }
                    case Steps.Erosion:
                        {
                            for (int i = 0; i < EROSIONS; ++i)
                            {
                                Bitmap = await Erosion.ApplyEffect(Bitmap);
                                await Task.Delay(LowTime);
                            }
                            break;
                        }
                    case Steps.Dilation:
                        {
                            for (int i = 0; i < DILATIONS; ++i)
                            {
                                Bitmap = await Dilation.ApplyEffect(Bitmap);
                                await Task.Delay(LowTime);
                            }
                            break;
                        }
                    case Steps.CutFilter:
                        {
                            TmpImage = Bitmap.DeepCopy();
                            Bitmap = await new ColorFilter(ColorMask.Negative).ApplyEffect(Bitmap);
                            await Task.Delay(MidTime);
                            
                            Bitmap = await new MaskFilter(Bitmap, Def.BLACK).ApplyEffect(OriginalImage.DeepCopy());
                            break;
                        }
                    case Steps.Canny:
                        {
                            var cannySteps = new Canny(5, 30).GetSteps(Bitmap);
                            foreach (var bitmap in cannySteps)
                            {
                                Bitmap = bitmap;
                                await Task.Delay(CannyTime);
                            }
                            break;
                        }
                    case Steps.CutFilter2:
                        {
                            var copyBitmap = Bitmap.DeepCopy();
                            Bitmap = TmpImage;
                            await Task.Delay(LowTime);

                            for (int i = 0; i < CUTFILTER_EROSIONS; ++i)
                            {
                                Bitmap = await Dilation.ApplyEffect(Bitmap);
                                await Task.Delay(LowTime);
                            }
                            await Task.Delay(MidTime);
                            Bitmap = await new MaskFilter(Bitmap).ApplyEffect(copyBitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new DeleteObjects(DeleteObjects.Delete.LessThanPixels, 35, Def.WHITE, Def.BLACK).ApplyEffect(Bitmap);

                            for (int i = 0; i < 2; ++i)
                            {
                                await Task.Delay(MidTime);
                                Bitmap = await Erosion.ApplyEffect(Bitmap);
                            }
                            break;
                        }
                    case Steps.LastProcessing:
                        {
                            Bitmap = await new DeleteObjects(DeleteObjects.Delete.BiggestObject, 0, Def.WHITE, Def.BLACK).ApplyEffect(Bitmap);
                            await Task.Delay(MidTime);

                            Bitmap = await new DeleteObjects(DeleteObjects.Delete.LessThanPixels, 150, Def.WHITE, Def.BLACK).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new ColorFilter(ColorMask.Negative).ApplyEffect(Bitmap);

                            for (int i = 0; i < 3; ++i)
                            {
                                await Task.Delay(MidTime);
                                Bitmap = await Dilation.ApplyEffect(Bitmap);
                            }

                            await Task.Delay(MidTime);
                            Bitmap = await new K3M(false).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new CleanIllegalPixels(Def.BLACK_N_WHITE, Def.WHITE).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new AfterK3MThinning(Def.BLACK, Def.WHITE).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new CloseSkeletonizedObject(Connect.FurthestPoints, Def.BLACK, Def.WHITE).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = OtherProcessing.Process(
                                Bitmap,
                                Def.BLACK,
                                Def.RED,
                                Def.BLUE,
                                unchecked((int)0xFFDEAD00),
                                unchecked((int)0xFF00BABE)
                            );


                            await Task.Delay(MidTime);
                            Bitmap = await new ColorFilter(ColorMask.Negative).ApplyEffect(Bitmap);

                            await Task.Delay(MidTime);
                            Bitmap = await new MaskFilter(Bitmap, Def.BLACK).ApplyEffect(OriginalImage);
                            break;
                        }

                }

                ++Now;
                if ((int)Now < (int)Steps.CutFilter)
                    Process();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
            }

            if (!String.IsNullOrEmpty(errMessage))
                await new MessageDialog("Error message: " + errMessage, "Error ocurred").ShowAsync();

            ProcessEnabled = true;
        }

        Dilation Dilation = new Dilation();
        Erosion Erosion = new Erosion();

        #region Properties & Fields
        private WriteableBitmap OriginalImage;
        private WriteableBitmap TmpImage;

        // Fields connected with properties
        private WriteableBitmap _Bitmap;
        private bool _ProcessEnabled;


        /// <summary>
        /// Determines which image is shown in the UI
        /// (it is bound to Image's Source property)
        /// </summary>
        public WriteableBitmap Bitmap
        {
            get { return _Bitmap; }
            set
            {
                _Bitmap = value;

                if (value != null)
                    ProcessEnabled = true;
                else
                    ProcessEnabled = false;

                NotifyOfPropertyChange(() => Bitmap);
            }
        }

        /// <summary>
        ///  Determines whether 'Process' AppBarButton's IsEnabled
        /// </summary>
        public bool ProcessEnabled
        {
            get { return _ProcessEnabled; }
            set
            {
                _ProcessEnabled = value;
                NotifyOfPropertyChange(() => ProcessEnabled);
            }
        }

        #endregion
    }
}
