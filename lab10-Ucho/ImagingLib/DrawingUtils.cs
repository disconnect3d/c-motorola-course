﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.Effects.Others;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib
{
    public static class DrawingUtils
    {
        #region Linear function
        /// <summary>
        /// Represents `a` and `b` params from y=a*x+b linear function formula
        /// </summary>
        public struct LinearFunctionParams
        {
            public double a;
            public double b;
        }


        public static LinearFunctionParams CalcLinearFuncParams(CoordPoint p1, CoordPoint p2)
        {
            double a = (double)(p1.Y - p2.Y) / (p1.X - p2.X);

            return new LinearFunctionParams()
            {
                a = a,
                b = p1.Y - (a * p1.X)
            };
        }

        public static Func<int, int> GetLinearDrawingFunc(LinearFunctionParams funcParams)
        {
            return new Func<int, int>((x) => (int)(funcParams.a * x + funcParams.b));
        }
        public static double GetPerpendicularParamA(double a)
        {
            return -1.0 / a;
        }

        public static double GetParamB(double y, double a, double x)
        {
            return y - a * x;
        }
        #endregion


        #region DrawLine
        /// <summary>
        /// Draw line between 2 points in specified color
        /// </summary>
        /// <param name="context">BitmapContext which must have Write Access</param>
        /// <param name="p1">First point</param>
        /// <param name="p2">Second point</param>
        /// <param name="Color">Color of drawn line</param>
        public static void DrawLine(BitmapContext context, CoordPoint p1, CoordPoint p2, int Color)
        {
            DrawLine(
                context,
                CalcLinearFuncParams(p1, p2),
                p1,
                p2,
                Color
            );
        }

        public static void DrawLine(BitmapContext context, LinearFunctionParams funcParams, CoordPoint p1, CoordPoint p2, int Color)
        {
            if (!context.IsInImageBounds(p1.X, p1.Y))
                throw new ArgumentException("CoordPoint p1 is out of image bounds");
            else if (!context.IsInImageBounds(p2.X, p2.Y))
                throw new ArgumentException("CoordPoint p2 is out of image bounds");

            Func<int, int> foo = GetLinearDrawingFunc(funcParams);

            for (int x = Math.Min(p1.X, p2.X); x < Math.Max(p1.X, p2.X); ++x)
            {
                int y1 = foo(x);
                int y2 = foo(x + 1);

                context.Pixels[context.IntArrIndex(x, y1)] = Color;

                int yStart = y2 > y1 ? y1 : y2;
                int yEnd = y2 > y1 ? y2 : y1;

                for (int yi = yStart + 1; yi < yEnd; ++yi)
                    context.Pixels[context.IntArrIndex(x, yi)] = Color;
            }
        }

        /// <summary>
        /// Draw line using all possible x values
        /// </summary>
        /// <param name="context">BitmapContext which must have Write Access</param>
        /// <param name="funcParams">Linear function parameters</param>
        /// <param name="Color">Color of drawn line</param>
        public static void DrawLine(BitmapContext context, LinearFunctionParams funcParams, int Color)
        {
            Func<int, int> foo = GetLinearDrawingFunc(funcParams);

            for (int x = 0; x < context.Width; ++x)
            {
                int y1 = foo(x);
                int y2 = foo(x + 1);

                if (!context.IsInImageBounds(x, y1))
                    continue;

                context.Pixels[context.IntArrIndex(x, y1)] = Color;

                int yStart = y2 > y1 ? y1 : y2;
                int yEnd = y2 > y1 ? y2 : y1;

                for (int yi = yStart + 1; yi < yEnd; ++yi)
                {
                    if (!context.IsInImageBounds(x, yi))
                        continue;
                    context.Pixels[context.IntArrIndex(x, yi)] = Color;
                }
            }
        }
        #endregion

        /// <summary>
        /// Draw square around specified pixel. This function calls Debug.Assert if trying to draw out of image bounds.
        /// </summary>
        /// <param name="context">BitmapContext which must have Write Access</param>
        /// <param name="cp">Point around which we will draw the square</param>
        /// <param name="region">Half of the side size (if you want square to be of side size=3, pass 1 here)</param>
        /// <param name="Color">Square color</param>
        public static void DrawSquare(BitmapContext context, CoordPoint cp, int region, int Color)
        {
            for (int i = -region; i <= region; ++i)
                for (int j = -region; j <= region; ++j)
                {
                    int x = cp.X + j;
                    int y = cp.Y + i;
                    if (!context.IsInImageBounds(x, y))
                    {
                        Debug.Assert(false, "Trying to draw square out of image bounds");
                        continue;
                    }
                    context.Pixels[context.IntArrIndex(x, y)] = Color;
                }
        }

        /// <summary>
        /// Checking if there is a neighbour in specified color. This function calls Debug.Assert if trying to check out of image bounds.
        /// </summary>
        /// <param name="context">BitmapContext which must have Read Access</param>
        /// <param name="cp">Point around which we will draw the square</param>
        /// <param name="region">Half of the side size (if you want checking square to be of side size=3, pass 1 here)</param>
        /// <param name="Color">Searching neighbour color</param>
        /// <returns>true if there is a neighbour, otherwise false</returns>
        public static bool ThereIsANeighbour(BitmapContext context, CoordPoint cp, int region, int CheckColor)
        {
            for (int i = -region; i <= region; ++i)
                for (int j = -region; j <= region; ++j)
                {
                    int x = cp.X + j;
                    int y = cp.Y + i;
                    if (!context.IsInImageBounds(x, y))
                    {
                        Debug.Assert(false, "Trying to check pixels out of image bounds");
                        continue;
                    }
                    if (context.Pixels[context.IntArrIndex(x, y)] == CheckColor)
                        return true;
                }

            return false;
        }
    }
}
