﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib
{
    internal static class InternalExtensions
    {
        internal static byte CutToByte(this int val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        internal static byte CutToByte(this float val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        /// <summary>
        /// Returns 32-bit integer representing ARGB color (each channel has 1B = 8b), glued up using pixel's alpha channel and specified RGB int color
        /// </summary>
        /// <param name="pixel">Alpha channel is taken from this pixel</param>
        /// <param name="rgb">This should not have first 2B set (alpha channel) because it does sum with pixel's alpha channel</param>
        /// <returns></returns>
        internal static int ColorWAlpha(this int pixel, int rgb)
        {
            return rgb + (pixel & Def.MASK_A);
        }

        internal enum Channel { A = 0, R = 1, G = 2, B = 3 };
        internal static int ByteArrIndex(this WriteableBitmap source, int x, int y, int i)
        {
            return (source.PixelWidth * y + x) * 4 + i;
        }

    }
}
