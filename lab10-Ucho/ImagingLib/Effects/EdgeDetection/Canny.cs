﻿using ImagingLib.Effects.Others;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI;

namespace ImagingLib.Effects.EdgeDetection
{
    public class Canny : IImageEffect
    {
        private const double PI = Math.PI;
        private const int channels = 4;
        private double ThresholdLow;
        private double ThresholdHigh;
        private double[,] grads;

        public string Name
        {
            get
            {
                return "Canny";
            }
        }

        public string Description
        {
            get
            {
                return "Detects edges on the image using Canny filter.";
            }
        }

        public Canny(double ThresholdLow, double ThresholdHigh)
        {
            Debug.Assert(ThresholdLow < ThresholdHigh, "Low threshold value is equals or greater than the high one.");
            this.ThresholdLow = ThresholdLow;
            this.ThresholdHigh = ThresholdHigh;
        }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            WriteableBitmap WB = await new Convolution(Convolution.GaussianBlurMask).ApplyEffect(source);
            WB = ColoredSobel(WB);
            return WB;
        }

        public IEnumerable<WriteableBitmap> GetSteps(WriteableBitmap source)
        {
            // Gaussian blur
            Task<WriteableBitmap> task = new Convolution(Convolution.GaussianBlurMask).ApplyEffect(source);
            task.Wait();
            WriteableBitmap WB = task.Result;
            yield return WB;

            // Sobel filter
            WriteableBitmap copy = ExternalExtensions.DeepCopy(WB);
            task = new SobelsFilter().ApplyEffect(WB);
            WB = task.Result;
            yield return WB;

            // Colored sobel
            WB = ColoredSobel(copy);
            yield return WB;

            // Non-maximum suppression
            WB = NonMaxSuppressStep(WB);
            yield return WB;

            // Double thresholding
            WB = DoubleThresholdStep(WB);
            yield return WB;

            // Histeresis
            WB = HisteresisStep(WB);
            yield return WB;
        }

        private unsafe WriteableBitmap ColoredSobel(WriteableBitmap source)
        {
            byte[] copy = WriteableBitmapExtensions.ToByteArray(source);
            byte[] result = WriteableBitmapExtensions.ToByteArray(source);

            grads = new double[source.PixelHeight, source.PixelWidth];

            // p0, p1, p2, p3, p4, p5, p6, p7, px stands for the indexes of pixel and its neighbors in such a grid:
            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4

            // creating most of the image

            // Pinning the ByteArray memory, so GC won't move it during work
            // Using ptr[] instead of copy[] is faster (same for resultPtr[] instead of result[])
            // because OutOfBoundsArray checks aren't made
            fixed (byte* ptr = copy, resultPtr = result)
            {
                for (int y = 1; y < source.PixelHeight - 1; ++y)
                {
                    for (int x = 1; x < source.PixelWidth - 1; ++x)
                    {
                        // px = (x, y)
                        int px = source.ByteArrIndex(x, y, 0);

                        int moveBottom = source.PixelWidth * channels;
                        // p0 = (x-1, y-1)
                        int p0 = px - channels - moveBottom;
                        // p1 = (x, y-1)
                        int p1 = px - moveBottom;
                        // p2 = (x+1, y-1)
                        int p2 = px + channels - moveBottom;
                        // p3 = (x+1, y)
                        int p3 = px + channels;
                        // p4 = (x+1, y+1)
                        int p4 = px + channels + moveBottom;
                        // p5 = (x, y+1)
                        int p5 = px + moveBottom;
                        // p6 = (x-1, y+1)
                        int p6 = px - channels + moveBottom;
                        // p7 = (x-1, y)
                        int p7 = px - channels;

                        // tmp1 = (p2 + 2*p3 + p4) - (p0 + 2*p7 + p6)
                        double tmp1 = (ptr[p2] + (2.0 * ptr[p3]) + ptr[p4]) - (ptr[p0] + (2.0 * ptr[p7]) + ptr[p6]);
                        // tmp2 =  (p0 + 2*p1 + p2) - (p6 + 2*p5 + p4)
                        double tmp2 = (ptr[p0] + (2.0 * ptr[p1]) + ptr[p2]) - (ptr[p6] + (2.0 * ptr[p5]) + ptr[p4]);

                        // Setting gradient here for later processing
                        grads[y, x] = Math.Sqrt(Math.Pow(tmp1, 2.0) + Math.Pow(tmp2, 2.0));

                        // But the result has to be in bytes :)
                        resultPtr[px] = ((int)grads[y, x]).CutToByte();


                        double theta = tmp1 == 0.0 ? Math.Atan(tmp2 / 1.0) : Math.Atan(tmp2 / tmp1);

                        if (resultPtr[px] < 15)
                            SetBlack(&resultPtr[px]);
                        else if (theta >= -3.0 * Math.PI / 8.0 && theta < -1.0 * Math.PI / 8.0)
                        {   // RED
                            resultPtr[px] = 0;
                            resultPtr[px + 1] = 0;
                            resultPtr[px + 2] = 255;
                        }
                        else if (theta >= -1.0 * Math.PI / 8.0 && theta < Math.PI / 8.0)
                        {   // YELLOW
                            resultPtr[px] = 0;
                            resultPtr[px + 1] = 255;
                            resultPtr[px + 2] = 255;
                        }
                        else if (theta >= Math.PI / 8.0 && theta < 3.0 * Math.PI / 8.0)
                        {   // GREEN
                            resultPtr[px] = 0;
                            resultPtr[px + 1] = 255;
                            resultPtr[px + 2] = 0;
                        }
                        else
                        {   // BLUE
                            resultPtr[px] = 255;
                            resultPtr[px + 1] = 0;
                            resultPtr[px + 2] = 0;
                        }
                    }
                }
            }
            return source.FromByteArray(result);
        }

        private unsafe WriteableBitmap NonMaxSuppressAndDoubleThreshold(WriteableBitmap source)
        {
            byte[] result = source.ToByteArray();
            fixed (byte* resultPtr = result)
            {
                int index = 4 * source.PixelWidth;
                for (int y = 1; y < source.PixelHeight - 1; ++y)
                {
                    index += 4;
                    for (int x = 1; x < source.PixelWidth - 1; ++x)
                    {
                        switch (GetColor(&resultPtr[index]))
                        {
                            case SobelColor.R:
                                if (!IsCurrentGradientMax(grads[y, x], grads[y + 1, x + 1], grads[y - 1, x - 1]))
                                    DoubleThreshold(&resultPtr[index], grads[y, x]);
                                break;

                            case SobelColor.G:
                                if (!IsCurrentGradientMax(grads[y, x], grads[y - 1, x + 1], grads[y + 1, x - 1]))
                                    DoubleThreshold(&resultPtr[index], grads[y, x]);
                                break;

                            case SobelColor.B:
                                if (!IsCurrentGradientMax(grads[y, x], grads[y - 1, x], grads[y + 1, x]))
                                    DoubleThreshold(&resultPtr[index], grads[y, x]);
                                break;

                            case SobelColor.Y:
                                if (!IsCurrentGradientMax(grads[y, x], grads[y, x + 1], grads[y, x - 1]))
                                    DoubleThreshold(&resultPtr[index], grads[y, x]);
                                break;
                        }
                        index += 4;
                    }
                    index += 4;
                }
            }
            return source.FromByteArray(result);
        }

        #region GetSteps implementation details
        private unsafe void DoubleThreshold(byte* ptr, double gradient)
        {
            if (gradient < ThresholdLow)
                SetBlack(ptr);
            else if (gradient < ThresholdHigh)
                SetGray(ptr);
            else
                SetWhite(ptr);
        }

        private unsafe WriteableBitmap NonMaxSuppressStep(WriteableBitmap source)
        {
            byte[] result = source.ToByteArray();
            fixed (byte* resultPtr = result)
            {
                int index = 4 * source.PixelWidth;
                for (int y = 1; y < source.PixelHeight - 1; ++y)
                {
                    index += 4;
                    for (int x = 1; x < source.PixelWidth - 1; ++x)
                    {
                        switch (GetColor(&resultPtr[index]))
                        {
                            case SobelColor.R:
                                {
                                    if (!IsCurrentGradientMax(grads[y, x], grads[y + 1, x + 1], grads[y - 1, x - 1]))
                                        SetBlack(&resultPtr[index]);
                                    break;
                                }
                            case SobelColor.G:
                                {
                                    if (!IsCurrentGradientMax(grads[y, x], grads[y - 1, x + 1], grads[y + 1, x - 1]))
                                        SetBlack(&resultPtr[index]);
                                    break;
                                }
                            case SobelColor.B:
                                {
                                    if (!IsCurrentGradientMax(grads[y, x], grads[y - 1, x], grads[y + 1, x]))
                                        SetBlack(&resultPtr[index]);
                                    break;
                                }
                            case SobelColor.Y:
                                {
                                    if (!IsCurrentGradientMax(grads[y, x], grads[y, x + 1], grads[y, x - 1]))
                                        SetBlack(&resultPtr[index]);
                                    break;
                                }
                        }
                        index += 4;
                    }
                    index += 4;
                }
            }
            return WriteableBitmapExtensions.FromByteArray(source, result);
        }

        private unsafe WriteableBitmap DoubleThresholdStep(WriteableBitmap source)
        {
            byte[] result = source.ToByteArray();

            fixed (byte* resultPtr = result)
            {
                int index = 4 * source.PixelWidth;
                for (int y = 1; y < source.PixelHeight - 1; ++y)
                {
                    index += 4;
                    for (int x = 1; x < source.PixelWidth - 1; ++x)
                    {
                        if (GetColor(&resultPtr[index]) != SobelColor.None)
                            DoubleThreshold(&resultPtr[index], grads[y, x]);
                        index += 4;
                    }
                    index += 4;
                }
            }
            return source.FromByteArray(result);
        }

        private unsafe WriteableBitmap HisteresisStep(WriteableBitmap source)
        {
            const int WHITE = unchecked((int)0xFFFFFFFF);
            const int GRAY = unchecked((int)0xFF808080);
            
            const int xStart = 5;
            const int yStart = 5;

            byte[] result = source.ToByteArray();

            using (var context = source.GetBitmapContext(ReadWriteMode.ReadOnly))
                fixed (byte* resultPtr = result)
                {
                    fixed (int* sourcePtr = context.Pixels)
                    {
                        int index = 4 * source.PixelWidth * yStart;
                        for (int y = yStart; y < source.PixelHeight - yStart; ++y)
                        {
                            index += 4 * xStart;
                            for (int x = xStart; x < source.PixelWidth - xStart; ++x)
                            {
                                if (sourcePtr[source.IntArrIndex(x, y)] != unchecked((int)0xFF000000) && sourcePtr[source.IntArrIndex(x,y)] != WHITE)
                                    Debug.Assert(true);
                                if (sourcePtr[source.IntArrIndex(x, y)] == GRAY)
                                {
                                    bool found_gray = false;
                                    for (int i = -1; i <= 1; ++i)
                                    {
                                        for (int j = -1; j <= 1; ++j)
                                        {
                                            int iArrIndex = source.IntArrIndex(x + j, y + i);
                                            if (sourcePtr[iArrIndex] == WHITE)
                                            {
                                                SetWhite(&resultPtr[index]);
                                                goto CONTINUE_LOOP;
                                            }
                                            else if (sourcePtr[iArrIndex] == GRAY && !(i == 0 && j == 0))
                                            {
                                                found_gray = true;
                                                goto FOUND_GRAY;
                                            }
                                        }
                                    }
                                FOUND_GRAY:
                                    if (found_gray)
                                    {
                                        for (int i = -2; i <= 2; ++i)
                                        {
                                            for (int j = -2; j <= 2; ++j)
                                            {
                                                int iArrIndex = source.IntArrIndex(x + j, y + i);
                                                if (sourcePtr[iArrIndex] == WHITE)
                                                {
                                                    SetWhite(&resultPtr[index]);
                                                    goto CONTINUE_LOOP;
                                                }
                                            }
                                        }
                                    }
                                SetBlack(&resultPtr[index]);
                                }
                            CONTINUE_LOOP:
                                index += 4;
                            }
                            index += 4 * xStart;
                        }
                    }
                }
            return source.FromByteArray(result);
        }
        #endregion

        #region Helper methods
        private unsafe SobelColor GetColor(byte* ptr)
        {
            //index:  B:0| G:1| R:2
            //-----------------------
            //Red       0|   0| 255
            //Green     0| 255|   0
            //Blue    255|   0|   0
            //Yellow    0| 255| 255

            if (ptr[0] == 0)
            {
                if (ptr[1] == 0 && ptr[2] == 255)
                    return SobelColor.R;
                if (ptr[1] == 255)
                {
                    if (ptr[2] == 0)
                        return SobelColor.G;
                    if (ptr[2] == 255)
                        return SobelColor.Y;
                }
            }
            else if (ptr[0] == 255 && ptr[1] == 0 && ptr[2] == 0)
                return SobelColor.B;

            return SobelColor.None;
        }

        private unsafe void SetBlack(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = (sbyte)0);
            ptr[0] = ptr[1] = ptr[2] = 0;
        }

        private unsafe void SetWhite(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = (sbyte)-1);
            ptr[0] = ptr[1] = ptr[2] = 255;
        }

        private unsafe void SetGray(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = sbyte.MinValue);
            ptr[0] = ptr[1] = ptr[2] = 128;
        }

        private bool IsCurrentGradientMax(double current, double other1, double other2)
        {
            return current > other1 && current > other2;
        }
        #endregion

        private enum SobelColor
        {
            None,
            R,
            G,
            B,
            Y,
        }
    }
}
