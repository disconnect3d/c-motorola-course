﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.Effects.Others;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Thinning
{
    /// <summary>
    /// Thinning algorithm to delete 2 pixels neighbours when there should be only one pixel.
    /// Written by Dominik Czarnota
    /// </summary>
    public class AfterK3MThinning : FindObjectsBase, IImageEffect
    {
        public AfterK3MThinning(int objectColor, int bgColor)
            : base(objectColor, bgColor)
        {
        }

        public string Name { get { return "AfterK3MThinning"; } }

        public string Description { get { return "Additional thinning used after K3M algorithm"; } }
        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            using (var context = source.GetBitmapContext())
            {
                var imageObject = GetImageObjects(context).First();

                foreach (CoordPoint cp in imageObject)
                {
                    int x = cp.X;
                    int y = cp.Y;

                    int neighbourCount = 0;
                    context.Pixels[context.IntArrIndex(x, y)] = ObjectColor;

                    /// looking at neighbours
                    for (int i = -1; i <= 1; ++i)
                        for (int j = -1; j <= 1; ++j)
                        {
                            if (i == 0 && j == 0)
                                continue;

                            int index = context.IntArrIndex(x + j, y + i);
                            if (context.Pixels[index] == ObjectColor)
                            {
                                ++neighbourCount;
                            }
                        }
                    if (neighbourCount >= 2)
                    {
                        // TOP and (TOP-LEFT or TOP-RIGHT)
                        if (context.Pixels[context.IntArrIndex(x, y - 1)] == ObjectColor &&
                            (context.Pixels[context.IntArrIndex(x - 1, y - 1)] == ObjectColor || context.Pixels[context.IntArrIndex(x + 1, y - 1)] == ObjectColor))
                            context.Pixels[context.IntArrIndex(x, y - 1)] = BackgroundColor;

                        // LEFT and (LEFT-TOP or LEFT-BOT)
                        else if (context.Pixels[context.IntArrIndex(x - 1, y)] == ObjectColor
                            && (context.Pixels[context.IntArrIndex(x - 1, y - 1)] == ObjectColor || context.Pixels[context.IntArrIndex(x - 1, y + 1)] == ObjectColor))
                            context.Pixels[context.IntArrIndex(x - 1, y)] = BackgroundColor;

                        // BOT and (BOT-LEFT or BOT-RIGHT)
                        else if (context.Pixels[context.IntArrIndex(x, y + 1)] == ObjectColor
                            && (context.Pixels[context.IntArrIndex(x - 1, y + 1)] == ObjectColor || context.Pixels[context.IntArrIndex(x + 1, y + 1)] == ObjectColor))
                            context.Pixels[context.IntArrIndex(x, y + 1)] = BackgroundColor;

                        // RIGHT and (RIGHT-BOT or RIGHT-TOP)
                        else if (context.Pixels[context.IntArrIndex(x + 1, y)] == ObjectColor
                            && (context.Pixels[context.IntArrIndex(x + 1, y + 1)] == ObjectColor || context.Pixels[context.IntArrIndex(x + 1, y - 1)] == ObjectColor))
                            context.Pixels[context.IntArrIndex(x + 1, y)] = BackgroundColor;
                    }
                }

            }

            return source;
        }

        public bool DoGrayscale { get; set; }
    }
}
