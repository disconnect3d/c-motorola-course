﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public class CleanIllegalPixels : IImageEffect
    {
        public CleanIllegalPixels(int[] legalColors, int changeIllegalColorsIntoColor)
        {
            this.LegalColors = legalColors;
            this.FixingColor = changeIllegalColorsIntoColor;
        }

        public string Name
        {
            get { return "Clean Pixels"; }
        }

        public string Description
        {
            get { return "Removes pixels in colors other than the filter pixels"; }
        }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            using (var context = source.GetBitmapContext())
            {
                unsafe
                {
                    fixed (int* Pixels = context.Pixels)
                    {
                        int* lastPixel = Pixels + context.Length;
                        int* currentPixel = Pixels;

                        do
                            if (!LegalColors.Contains(*currentPixel))
                                *currentPixel = FixingColor;
                        while (currentPixel++ != lastPixel);
                    }
                }
            }

            return source;
        }

        public int FixingColor { get; set; }

        public int[] LegalColors { get; set; }
    }
}
