﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public class MaskFilter : IImageEffect
    {
        /// <summary>
        /// Filter used to change parts of image into maskingColor. 
        /// All pixels that are white on maskImage will be changed to maskingColor in the given to ApplyEffect image.
        /// </summary>
        /// <param name="maskImage">Image that is a mask</param>
        /// <param name="nonFilteringColor">maskImage background color (all pixels which will be in this color in maskImage won't be applied to the output image)</param>
        public MaskFilter(WriteableBitmap maskImage, int nonFilteringColor = Def.WHITE)
        {
            this.FilterMask = maskImage;
            this.NotMaskingColor = nonFilteringColor;
        }

        public string Name
        {
            get { return "Image filter"; }
        }

        public string Description
        {
            get { return "Filters image by other binary image"; }
        }

        /// <summary>
        /// Applies ChangeColors effect into specified image
        /// </summary>
        /// <param name="image">Input image</param>
        /// <param name="varArgs">Pass another WriteableBitmap image here.</param>
        /// <returns>WriteableBitmap containing image with applied effect</returns>
        public unsafe async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            using (BitmapContext sourceContext = source.GetBitmapContext(), filterContext = FilterMask.GetBitmapContext(ReadWriteMode.ReadOnly))
                fixed(int* sourcePixels = sourceContext.Pixels, filterPixels = filterContext.Pixels)
                    for(int i=0; i<source.PixelHeight*source.PixelWidth; ++i)
                        if (filterPixels[i] != NotMaskingColor)
                            sourcePixels[i] = filterPixels[i];
            return source;
        }

        public WriteableBitmap FilterMask { get; set; }

        public int NotMaskingColor { get; set; }
    }
}
