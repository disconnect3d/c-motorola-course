﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public class Convolution : IImageEffect
    {
        public Convolution(int[,] Mask)
        {
            this.Kernel = Mask;
        }

        /// <summary>
        /// Mask filter
        /// </summary>
        readonly int[,] Kernel;

        /// <summary>
        /// Gaussian blur mask
        /// Passed as Convultion's ctor argument
        /// </summary>
        public readonly static int[,] GaussianBlurMask =
        { 
            {2,  4,  5,  4, 2},
            {4,  9, 12,  9, 4},
            {5, 12, 15, 12, 5},
            {4,  9, 12,  9, 4},
            {2,  4,  5,  4, 2}
        };

        public string Name { get { return "Convultion"; } }

        public string Description { get { return "Applies convultion to the image"; } }
        
        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            return source.Convolute(Kernel);
        }
    }
}
