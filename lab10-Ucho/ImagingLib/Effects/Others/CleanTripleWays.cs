﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public class CleanTripleWays : FindObjectsBase, IImageEffect
    {
        public enum Mode { ShortestWay, LongestWay }

        /// <summary>
        /// Constructs CleanTripleWays effect
        /// </summary>
        /// <param name="cleanMode">Determines what to clean when triple way is found</param>
        /// <param name="objectColor">Object/line color</param>
        /// <param name="bgColor">Background color</param>
        public CleanTripleWays(Mode cleanMode, int objectColor, int bgColor)
            : base(objectColor, bgColor)
        {
            this.CleanMode = cleanMode;
            throw new NotImplementedException("This effect has to be implemented");
        }

        public string Name
        {
            get { return "Clean Triple Ways"; }
        }

        public string Description
        {
            get
            {
                return "Iterates over objects assuming they are lines. If there is a line having 3 neighbours instead of 1 or 2, deletes one of the ways determined by the CleanMode property.";
            }
        }

        public Mode CleanMode { get; set; }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            throw new NotImplementedException("This effect has to be implemented");
            //List<List<CoordPoint>> objects = new List<List<CoordPoint>>();
            //using (var context = source.GetBitmapContext())
            //{
            //    int[] Pixels = context.Pixels;

            //    for (int y = 0; y < context.Height; ++y)
            //        for (int x = 0; x < context.Width; ++x)
            //             objects.Add(GetImageObject(x, y, context));
                
            //    List<CoordPoint> neighbours = new List<CoordPoint>();
            //    // iterating over objects and cleaning triple ways
            //    foreach(var obj in objects)
            //    {
            //        // looking for triple neighbours spots
            //        foreach(CoordPoint px in obj)
            //        {
            //            neighbours.Clear();

            //            for(int i=-1; i<=1; ++i)
            //                for(int j=-1; j<=1; ++j)
            //                {
            //                    int x = px.X + j;
            //                    int y = px.Y + i;
            //                    if (!source.IsInImageBounds(x, y) || (i == 0 && j == 0))
            //                        continue;
                                
            //                    int index = source.IntArrIndex(x, y);
            //                    if (Pixels[index] == ObjectColor)
            //                        neighbours.Add(new CoordPoint(x, y));
            //                }

            //            if (neighbours.Count == 3)
            //            {
            //                // TODO ...
            //            }
            //        }
            //    }
            //}

            //return source;
        }
    }
}
