﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib
{
    public static class WriteableBitmapFactory
    {
        /// <summary>
        /// Creates WriteableBitmap asynchronously using FileOpenPicker
        /// </summary>
        /// <param name="FOP">Default FOP will be created if this stays null</param>
        /// <returns>Created WriteableBitmap or null</returns>
        public static async Task<WriteableBitmap> CreateWithFOP(FileOpenPicker FOP = null)
        {
            if (FOP == null)
            {
                FOP = new FileOpenPicker();
                FOP.ViewMode = PickerViewMode.Thumbnail;
                FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                FOP.FileTypeFilter.Add(".bmp");
                FOP.FileTypeFilter.Add(".jpg");
                FOP.FileTypeFilter.Add(".png");
                FOP.FileTypeFilter.Add(".jpeg");
                FOP.FileTypeFilter.Add(".gif");
            }
            return await Create(await FOP.PickSingleFileAsync());
        }

        /// <summary>
        /// Creates WriteableBitmap asynchronously using StorageFile
        /// </summary>
        /// <param name="file">Input StorageFile; can't be null</param>
        /// <returns>Created WriteableBitmap</returns>
        public static async Task<WriteableBitmap> Create(StorageFile file)
        {
            if (file == null)
                return null;

            IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read);
            Guid decoderId;

            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    decoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".bmp":
                    decoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".png":
                    decoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".gif":
                    decoderId = BitmapDecoder.GifDecoderId;
                    break;
                default:
                    throw new ArgumentException("File provided must be in format jpg/jpeg/bmp/png/gif");
            }


            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
            PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Rgba8,    // Byte Array Format [8 bit values: 0-255]
                BitmapAlphaMode.Straight,   // How to code Alpha channel
                new BitmapTransform(),
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage
            );

            WriteableBitmap wb = new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight);
            await wb.SetSourceAsync(fileStream);

            return wb;
        }
    }
}
