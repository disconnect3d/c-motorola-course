﻿using Anaglyphs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Storage.Streams;
using Windows.Storage.Pickers;
using Caliburn.Micro;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.Media.Capture;
using Windows.UI.Popups;

namespace Anaglyphs
{
    public class MenuViewModel : PropertyChangedBase
    {
        public MenuViewModel()
        {
            LoadRightEnabled = false;
            ProcessEnabled = false;
        }

        /// <summary>
        /// Captures image from a camera.
        /// This function WAS NOT TESTED, so MAY BE BUGGY!
        /// </summary>
        async private Task CaptureImage(Choice ch)
        {
            CameraCaptureUI cameraDlg = null;
            bool result = true;
            String err = String.Empty;
            try
            {
                cameraDlg = new CameraCaptureUI();
                cameraDlg.PhotoSettings.AllowCropping = true;
                cameraDlg.PhotoSettings.MaxResolution = CameraCaptureUIMaxPhotoResolution.MediumXga;

            }
            catch (Exception e)
            {
                result = false;
                err = e.Message;
            }

            if (!result)
            {
                await (new MessageDialog("Error initializing camera: " + err).ShowAsync());
                return;
            }

            Windows.Storage.StorageFile capturedMediaFile = await cameraDlg.CaptureFileAsync(CameraCaptureUIMode.Photo);

            LoadFile(capturedMediaFile, ch);
        }

        public void ResetImages()
        {
            if (LeftOriginalImage != null)
            {
                LeftImageChange = true;
                LeftImage = new ImageWrapper(LeftOriginalImage);
                ProcessEnabled = true;

                LeftBitmap = new WriteableBitmap((int)LeftImage.Width, (int)LeftImage.Height);
                using (Stream stream = LeftBitmap.PixelBuffer.AsStream())
                    stream.Write(LeftImage.ByteArray, 0, LeftImage.ByteArray.Length);

                MergedBitmap = null;
            }
            if (RightOriginalImage != null)
            {
                RightImageChange = true;
                RightImage = new ImageWrapper(RightOriginalImage);
                ProcessEnabled = true;

                RightBitmap = new WriteableBitmap((int)RightImage.Width, (int)RightImage.Height);
                using (Stream stream = RightBitmap.PixelBuffer.AsStream())
                    stream.Write(RightImage.ByteArray, 0, RightImage.ByteArray.Length);
            }
        }

        public async void CaptureLeftImage()
        {
            await CaptureImage(Choice.LeftImage);
        }

        public async void CaptureRightImage()
        {
            await CaptureImage(Choice.RightImage);
        }

        /// <summary>
        /// Bound to the UI button
        /// </summary>
        public void LoadLeftImage()
        {
            LoadImage(Choice.LeftImage);
        }

        /// <summary>
        /// Bound to the UI button
        /// </summary>
        public void LoadRightImage()
        {
            LoadImage(Choice.RightImage);
        }

        /// <summary>
        /// Lets user load image from a file by showing a popup "choose a file"
        /// </summary>
        private async void LoadImage(Choice direction)
        {
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.Thumbnail;
            FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            FOP.FileTypeFilter.Add(".bmp");
            FOP.FileTypeFilter.Add(".jpg");
            FOP.FileTypeFilter.Add(".png");
            FOP.FileTypeFilter.Add(".jpeg");

            StorageFile file = await FOP.PickSingleFileAsync();
            if (file != null)
                LoadFile(file, direction);
        }

        internal enum Choice { LeftImage, RightImage, Merged };

        /// <summary>
        /// Loads image from a StorageFile
        /// This function doesn't check if file == null
        /// </summary>
        /// <param name="file">StorageFile to load image from</param>
        private async void LoadFile(StorageFile file, Choice ch)
        {
            fileStream = await file.OpenAsync(FileAccessMode.Read);

            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    decoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".bmp":
                    decoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".png":
                    decoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".gif":
                    decoderId = BitmapDecoder.GifDecoderId;
                    break;
            }
            decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
            pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,    // byte array format: Blue Green Red Alpha [8 bit values: 0-255]
                BitmapAlphaMode.Straight,   // How to code Alpha channel
                new BitmapTransform(),
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage
            );

            if (ch == Choice.LeftImage)
            {
                LeftImage = new ImageWrapper(pixelData.DetachPixelData(), decoder.PixelWidth, decoder.PixelHeight);
                LeftBitmap = new WriteableBitmap((int)LeftImage.Width, (int)LeftImage.Height);
                
                await LeftBitmap.SetSourceAsync(fileStream);

                LeftOriginalImage = new ImageWrapper(LeftImage);

                LeftImageChange = true;
                LoadRightEnabled = true;
            }
            else if (ch == Choice.RightImage)
            {
                RightImage = new ImageWrapper(pixelData.DetachPixelData(), decoder.PixelWidth, decoder.PixelHeight);
                RightBitmap = new WriteableBitmap((int)RightImage.Width, (int)RightImage.Height);

                await RightBitmap.SetSourceAsync(fileStream);

                RightOriginalImage = new ImageWrapper(RightImage);

                RightImageChange = true;
                ProcessEnabled = true;
            }

        }

        /// <summary>
        /// Applies effect to the image
        /// </summary>
        /// <param name="effect">Effect to be applied</param>
        private void ApplyEffect(IImageEffect effect, Choice ch, params object[] varArgs)
        {
            if (fileStream == null)
                return;

            ImageWrapper workingImage = ch == Choice.LeftImage ? LeftImage : RightImage;

            if (ch == Choice.LeftImage)
            {
                LeftImage = effect.ApplyEffect(workingImage, varArgs);
                LeftBitmap = new WriteableBitmap((int)LeftImage.Width, (int)LeftImage.Height);
                using (Stream stream = LeftBitmap.PixelBuffer.AsStream())
                    stream.Write(LeftImage.ByteArray, 0, LeftImage.ByteArray.Length);

                MergedBitmap = null;
            }
            else if (ch == Choice.RightImage)
            {
                RightImage = effect.ApplyEffect(workingImage, varArgs);
                RightBitmap = new WriteableBitmap((int)RightImage.Width, (int)RightImage.Height);
                using (Stream stream = RightBitmap.PixelBuffer.AsStream())
                    stream.Write(RightImage.ByteArray, 0, RightImage.ByteArray.Length);
            }
            else if (ch == Choice.Merged)
            {
                LeftImage = effect.ApplyEffect(LeftImage, RightImage);
                LeftBitmap = null;
                RightBitmap = null;

                MergedBitmap = new WriteableBitmap((int)LeftImage.Width, (int)LeftImage.Height);
                using (Stream stream = MergedBitmap.PixelBuffer.AsStream())
                    stream.Write(LeftImage.ByteArray, 0, LeftImage.ByteArray.Length);
            }
        }

        /// <summary>
        /// Processes next steps
        /// </summary>
        public async void Process()
        {
            LoadRightEnabled = false;
            if (LeftImage == null || RightImage == null)
            {
                var dlg = new Windows.UI.Popups.MessageDialog("Load left & right images first", "Error");
                await dlg.ShowAsync();
                return;
            }
            else if (LeftImage.Width != RightImage.Width || LeftImage.Height != RightImage.Height)
            {
                var dlg = new Windows.UI.Popups.MessageDialog("Left & right images must be in the same size", "Error");
                await dlg.ShowAsync();
                return;
            }
            else if (SelectedMask == null)
            {
                var dlg = new Windows.UI.Popups.MessageDialog("Choose anaglyph version (mask)", "Error");
                await dlg.ShowAsync();
                return;
            }

            if (LeftImageChange)
            {
                LeftCopy = new ImageWrapper(LeftImage);
                ApplyEffect(new ChangeColors(SelectedMask.Left), Choice.LeftImage);
                LeftImageChange = false;
            }
            else if (RightImageChange)
            {
                RightCopy = new ImageWrapper(RightImage);
                ApplyEffect(new ChangeColors(SelectedMask.Right), Choice.RightImage);
                RightImageChange = false;
            }
            else
            {
                LeftImage = LeftCopy;
                RightImage = RightCopy;
                ApplyEffect(new Anaglyph(SelectedMask), Choice.Merged);
                
                ProcessEnabled = false;
            }

        }
        private bool LeftImageChange;
        private bool RightImageChange;



        private Guid decoderId;
        private BitmapDecoder decoder;
        private PixelDataProvider pixelData;
        private IRandomAccessStream fileStream = null;

        private ImageWrapper LeftOriginalImage;
        private ImageWrapper RightOriginalImage;

        private ImageWrapper LeftImage;
        private ImageWrapper RightImage;
        private ImageWrapper LeftCopy;
        private ImageWrapper RightCopy;

        private WriteableBitmap _LeftBitmap;
        public WriteableBitmap LeftBitmap
        {
            get { return _LeftBitmap; }
            set
            {
                _LeftBitmap = value;
                NotifyOfPropertyChange(() => LeftBitmap);
            }
        }

        private WriteableBitmap _RightBitmap;
        public WriteableBitmap RightBitmap
        {
            get { return _RightBitmap; }
            set
            {
                _RightBitmap = value;
                NotifyOfPropertyChange(() => RightBitmap);
            }
        }

        private WriteableBitmap _MergedBitmap;
        public WriteableBitmap MergedBitmap
        {
            get { return _MergedBitmap; }
            set
            {
                _MergedBitmap = value;
                NotifyOfPropertyChange(() => MergedBitmap);
            }
        }
        

        private bool _LoadRightEnabled;
        public bool LoadRightEnabled
        {
            get { return _LoadRightEnabled; }
            set
            {
                _LoadRightEnabled = value;
                NotifyOfPropertyChange(() => LoadRightEnabled);
            }
        }

        private bool _ProcessEnabled;
        public bool ProcessEnabled
        {
            get { return _ProcessEnabled; }
            set
            {
                _ProcessEnabled = value;
                NotifyOfPropertyChange(() => ProcessEnabled);
            }
        }
        

        #region Anaglyph modes, when adding new mode add field setter and property notifier to SelectedMask setter ! ! !

        private AnaglyphMask _SelectedMask;
        public AnaglyphMask SelectedMask
        {
            get { return _SelectedMask; }
            set
            {
                _SelectedMask = value;
                NotifyOfPropertyChange(() => SelectedMask);

                _MonochromaticAnaglyph = false;
                NotifyOfPropertyChange(() => MonochromaticAnaglyph);

                _TrueAnaglyph = false;
                NotifyOfPropertyChange(() => TrueAnaglyph);

                _ColorAnaglyph = false;
                NotifyOfPropertyChange(() => ColorAnaglyph);

                _HalfColorAnaglyph = false;
                NotifyOfPropertyChange(() => HalfColorAnaglyph);

                _OptimizedAnaglyph = false;
                NotifyOfPropertyChange(() => OptimizedAnaglyph);
            }
        }


        
        private bool _MonochromaticAnaglyph;
        public bool MonochromaticAnaglyph
        {
            get { return _MonochromaticAnaglyph; }
            set
            {
                SelectedMask = value ? Predefined.Monochromatic : null;

                _MonochromaticAnaglyph = value;
                NotifyOfPropertyChange(() => MonochromaticAnaglyph);
            }
        }

        private bool _TrueAnaglyph;
        public bool TrueAnaglyph
        {
            get { return _TrueAnaglyph; }
            set
            {
                SelectedMask = value? Predefined.True : null;

                _TrueAnaglyph = value;
                NotifyOfPropertyChange(() => TrueAnaglyph);
            }
        }

        private bool _ColorAnaglyph;
        public bool ColorAnaglyph
        {
            get { return _ColorAnaglyph; }
            set
            {
                SelectedMask = value ? Predefined.Color : null;

                _ColorAnaglyph = value;
                NotifyOfPropertyChange(() => ColorAnaglyph);
            }
        }

        private bool _HalfColorAnaglyph;
        public bool HalfColorAnaglyph
        {
            get { return _HalfColorAnaglyph; }
            set
            {
                SelectedMask = value ? Predefined.HalfColor : null;

                _HalfColorAnaglyph = value;
                NotifyOfPropertyChange(() => HalfColorAnaglyph);
            }
        }

        private bool _OptimizedAnaglyph;
        public bool OptimizedAnaglyph
        {
            get { return _OptimizedAnaglyph; }
            set
            {
                SelectedMask = value ? Predefined.Optimized : null;

                _OptimizedAnaglyph = value;
                NotifyOfPropertyChange(() => OptimizedAnaglyph);
            }
        }

        #endregion




    }
}
