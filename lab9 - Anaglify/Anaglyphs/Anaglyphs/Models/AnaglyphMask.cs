﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anaglyphs.Models
{
    public class ColorMask
    {
        /// <summary>
        /// Size must be 3
        /// </summary>
        public float[] Red;
        /// <summary>
        /// Size must be 3
        /// </summary>
        public float[] Green;
        /// <summary>
        /// Size must be 3
        /// </summary>
        public float[] Blue;
    }

    public class AnaglyphMask
    {
        public AnaglyphMask(String Name, ColorMask Left, ColorMask Right)
        {
            this.Name = Name;
            this.Left = Left;
            this.Right = Right;
        }

        public readonly String Name;

        public readonly ColorMask Left;

        public readonly ColorMask Right;
    }

    public static class Predefined
    {
        public static AnaglyphMask Monochromatic = new AnaglyphMask(
            "Monochromatic (gray)",
            new ColorMask()
            {
                Red = new float[3] { 0.299F, 0.587F, 0.114F},
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0F, 0F, 0F }
            },
            new ColorMask()
            {
                Red = new float[3] { 0F, 0F, 0F },
                Green = new float[3] { 0.299F, 0.587F, 0.114F },
                Blue = new float[3] { 0.299F, 0.587F, 0.114F }
            }
        );

        public static AnaglyphMask True = new AnaglyphMask(
            "True",
            new ColorMask()
            {
                Red = new float[3] { 0.299F, 0.587F, 0.114F },
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0F, 0F, 0F }
            },
            new ColorMask()
            {
                Red = new float[3] { 0F, 0F, 0F },
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0.299F, 0.587F, 0.114F }
            }
        );

        public static AnaglyphMask Color = new AnaglyphMask(
            "Color",
            new ColorMask()
            {
                Red = new float[3] { 1F, 0F, 0F },
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0F, 0F, 0F }
            },
            new ColorMask()
            {
                Red = new float[3] { 0F, 0F, 0F },
                Green = new float[3] { 0F, 1F, 0F },
                Blue = new float[3] { 0F, 0F, 1F }
            }
        );

        public static AnaglyphMask HalfColor = new AnaglyphMask(
            "Half Color",
            new ColorMask()
            {
                Red = new float[3] { 0.299F, 0.587F, 0.114F },
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0F, 0F, 0F }
            },
            new ColorMask()
            {
                Red = new float[3] { 0F, 0F, 0F },
                Green = new float[3] { 0F, 1F, 0F },
                Blue = new float[3] { 0F, 0F, 1F }
            }
        );

        public static AnaglyphMask Optimized = new AnaglyphMask(
            "Optimized",
            new ColorMask()
            {
                Red = new float[3] { 0F, 0.7F, 0.3F },
                Green = new float[3] { 0F, 0F, 0F },
                Blue = new float[3] { 0F, 0F, 0F }
            },
            new ColorMask()
            {
                Red = new float[3] { 0F, 0F, 0F },
                Green = new float[3] { 0F, 1F, 0F },
                Blue = new float[3] { 0F, 0F, 1F }
            }
        );
    }


}
