﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Anaglyphs.Models
{
    

    public class ChangeColors : IImageEffect
    {
        private readonly ColorMask Mask;
        public ChangeColors(ColorMask mask)
        {
            this.Mask = mask;
        }

        public string Name
        {
            get { return "Change colors"; }
        }

        public string Description
        {
            get { return "Changes colors using specified mask"; }
        }

        /// <summary>
        /// Applies ChangeColors effect into specified image
        /// </summary>
        /// <param name="image">Input image</param>
        /// <param name="varArgs">This is not used in this effect.</param>
        /// <returns>Image with applied effect</returns>
        public unsafe ImageWrapper ApplyEffect(ImageWrapper image, params object[] varArgs)
        {
            fixed (byte* ptr = image.ByteArray)
                for (int index = 0; index < image.ByteArray.Length; index += 4)
                {
                    float B = (float)ptr[index + 0];
                    float G = (float)ptr[index + 1];
                    float R = (float)ptr[index + 2];

                    // BLUE
                    ptr[index + 0] = (byte)(Mask.Blue[0] * R + Mask.Blue[1] * G + Mask.Blue[2] * B);

                    // GREEN
                    ptr[index + 1] = (byte)(Mask.Green[0] * R + Mask.Green[1] * G + Mask.Green[2] * B);

                    // RED
                    ptr[index + 2] = (byte)(Mask.Red[0] * R + Mask.Red[1] * G + Mask.Red[2] * B);
                }

            return image;
        }
    }
}
