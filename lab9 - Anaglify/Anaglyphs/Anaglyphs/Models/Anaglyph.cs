﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anaglyphs.Models
{
    public class Anaglyph : IImageEffect
    {
        private readonly ColorMask LeftMask;
        private readonly ColorMask RightMask;

        public Anaglyph(AnaglyphMask mask)
        {
            this.LeftMask = mask.Left;
            this.RightMask = mask.Right;
        }

        public string Name
        {
            get { return "Anaglyphs"; }
        }

        public string Description
        {
            get { return "Creates 3D anaglyph effect using specified images"; }
        }

        /// <summary>
        /// Applies effect using specified images.
        /// </summary>
        /// <param name="leftImage">First image, the result will be saved here</param>
        /// <param name="varArgs">Pass second image (ImageWrapper) here</param>
        /// <returns>Image with applied effect.</returns>
        public unsafe ImageWrapper ApplyEffect(ImageWrapper leftImage, params object[] varArgs)
        {
            ImageWrapper rightImg = (ImageWrapper)varArgs[0];
            fixed (byte* left = leftImage.ByteArray, right = rightImg.ByteArray)
                for (int index = 0; index < leftImage.ByteArray.Length; index += 4)
                {
                    float leftB = (float)left[index + 0];
                    float leftG = (float)left[index + 1];
                    float leftR = (float)left[index + 2];

                    float rightB = (float)right[index + 0];
                    float rightG = (float)right[index + 1];
                    float rightR = (float)right[index + 2];

                    #region Blue
                    float value = (
                        LeftMask.Blue[0] * leftR + LeftMask.Blue[1] * leftG + LeftMask.Blue[2] * leftB +
                        RightMask.Blue[0] * rightR + RightMask.Blue[1] * rightG + RightMask.Blue[2] * rightB
                    );
                    Debug.Assert(value <= 255 && value >= 0);
                    left[index + 0] = (byte)value;
                    #endregion

                    #region Green
                    value = (
                        LeftMask.Green[0] * leftR + LeftMask.Green[1] * leftG + LeftMask.Green[2] * leftB +
                        RightMask.Green[0] * rightR + RightMask.Green[1] * rightG + RightMask.Green[2] * rightB
                    );
                    Debug.Assert(value <= 255 && value >= 0);
                    left[index + 1] = (byte)value;
                    #endregion

                    #region Red
                    value = (
                        LeftMask.Red[0] * leftR + LeftMask.Red[1] * leftG + LeftMask.Red[2] * leftB +
                        RightMask.Red[0] * rightR + RightMask.Red[1] * rightG + RightMask.Red[2] * rightB
                    );
                    Debug.Assert(value <= 255 && value >= 0);
                    left[index + 2] = (byte)value;
                    #endregion
                }

            return leftImage;
        }
    }
}
