﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anaglyphs.Models
{
    public interface IImageEffect
    {
        String Name
        { get; }
        String Description
        { get; }
        ImageWrapper ApplyEffect(ImageWrapper image, params object[] varArgs);
    }
}
