# README #

This repository contains labs and project made for a university course sponsored by Motorola Solutions Foundation - Biometry and Image and Signal Analysis in Mobile Devices.

**It has been done around 03-06.2014.**

Most of the code has been written in C# for Windows desktops/tablets.

The labs contains implementations of:

* simple image manipulations: sepia, grayscale, scaling
* plotting histogram
* edge recognition algorithms (sobel filter, robert's cross, canny filter)
* image morphology - dilation and erosion
* image skeletonization - K3M thinning
* image binarization (mixed threshold, bernsen and otsu methods)
* noise filtering
* creating anaglyphs
* EKG signal analysis
* making an outline/stroke of an ear image (however it didn't work well for other inputs...)
* finding eye retina
* ...and probably something else I forgot to write about.


The project covered license plate recognition algorithm (although it didn't use any machine learning it worked not so bad; however for a real life project I would have just used [alpr](https://github.com/openalpr/openalpr)).