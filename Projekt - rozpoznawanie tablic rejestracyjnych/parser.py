import re

fileName = "data.txt"
letters = "ABCDEFGHIJKLMNOPRSTUVWXYZ"

with open(fileName) as fp:
    lines = fp.readlines()
    #print(lines)
    i = 0
    for line in lines:
        if "Rows = " in line:
            print('''\t\t\t\tnew Tuple<double[], String>(
                \tnew double[] {''' + line[7:line.rindex(',')] + '''},
                \t"''' + letters[i] + '"),')
            i += 1
