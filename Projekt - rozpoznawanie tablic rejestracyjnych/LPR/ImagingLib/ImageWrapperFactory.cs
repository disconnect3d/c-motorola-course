﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.ImageUtils;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib
{
    public static class ImageWrapperFactory
    {
        /// <summary>
        /// Creates the ImageWrapper with FileOpenPicker.
        /// </summary>
        /// <param name="FOP">Default FOP will be created if this stays null: ViewMode:Thumbnail, StartLoc:PicturesLib, FileTypes: bmp, jpg, jpeg, png, gif</param>
        /// <returns>Created ImageWrapper or null (in case when user cancels operation).</returns>
        public static async Task<ImageWrapper> CreateWithFOP(FileOpenPicker FOP = null)
        {
            if (FOP == null)
            {
                FOP = new FileOpenPicker();
                FOP.ViewMode = PickerViewMode.Thumbnail;
                FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                FOP.FileTypeFilter.Add(".bmp");
                FOP.FileTypeFilter.Add(".jpg");
                FOP.FileTypeFilter.Add(".png");
                FOP.FileTypeFilter.Add(".jpeg");
                FOP.FileTypeFilter.Add(".gif");
            }

            // TODO: Create & use universal file picker since method below works only for Windows 8.x, and not for Windows Phone 8.x ...
            return await Create(await FOP.PickSingleFileAsync());
        }

        /// <summary>
        /// Creates the ImageWrapper asynchronously.
        /// <para> </para>
        /// Extensions supported: jpg, jpeg, bmp, png, gif, ico, tif
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>Created ImageWrapper or null (when file == null)</returns>
        /// <exception cref="System.ArgumentException">File provided must be in format jpg/jpeg/bmp/png/gif</exception>
        public static async Task<ImageWrapper> Create(StorageFile file)
        {
            if (file == null)
                return null;

            IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read);
            Guid decoderId;

            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    decoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".bmp":
                    decoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".png":
                    decoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".gif":
                    decoderId = BitmapDecoder.GifDecoderId;
                    break;
                case ".ico":
                    decoderId = BitmapDecoder.IcoDecoderId;
                    break;
                case ".tif":
                    decoderId = BitmapDecoder.TiffDecoderId;
                    break;
                default:
                    throw new ArgumentException("File provided must be in format jpg/jpeg/bmp/png/gif");
            }

            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
            PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,    // Byte Array Format [8 bit values: 0-255]
                BitmapAlphaMode.Straight,   // How to code Alpha channel
                new BitmapTransform(),
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage
            );

            WriteableBitmap wb = new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight);
            await wb.SetSourceAsync(fileStream);

            return new ImageWrapper(pixelData.DetachPixelData(), (int)decoder.PixelWidth, (int)decoder.PixelHeight);
        }
    }
}
