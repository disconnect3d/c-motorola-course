﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using ImagingLib.ImageUtils;

namespace ImagingLib
{
    public class ImageWrapper
    {
        public Brightness GetBrightness()
        {
            return new Brightness(ByteArray, Width, Height);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageWrapper"/> class.
        /// </summary>
        /// <param name="byteArray">The byte array containing BGRA color values.</param>
        /// <param name="width">The image width.</param>
        /// <param name="height">The image height.</param>
        public ImageWrapper(byte[] byteArray, int width, int height)
        {
            ByteArray = byteArray;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageWrapper"/> class.
        /// </summary>
        /// <param name="other">The ImageWrapper to deep copy from.</param>
        public ImageWrapper(ImageWrapper other)
        {
            ByteArray = new byte[other.ByteArray.Length];

            Buffer.BlockCopy(other.ByteArray, 0, ByteArray, 0, other.ByteArray.Length);
            Width = other.Width;
            Height = other.Height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageWrapper"/> class with black image (all channels are 0).
        /// </summary>
        /// <param name="width">The image width.</param>
        /// <param name="height">The image height.</param>
        public ImageWrapper(int width, int height)
        {
            Width = width;
            Height = height;
            ByteArray = new byte[Height * Width * channels];
        }

        public const int channels = 4;
        public int Height { get; private set; }
        public int Width { get; private set; }
        public byte[] ByteArray { get; private set; }

        /// <summary>
        /// Changes the byte array.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <exception cref="System.ArgumentException">Wrong ByteArray length. Consider using ChangeTo(ImageWrapper) method</exception>
        public void ChangeByteArray(byte[] other)
        {
            if (other.Length != ByteArray.Length)
                throw new ArgumentException("Wrong ByteArray length. Consider using ChangeTo(ImageWrapper) method");
            ByteArray = other;
        }

        /// <summary>
        /// Changes the image.
        /// </summary>
        /// <param name="other">The other.</param>
        public void ChangeImage(ImageWrapper other)
        {
            ByteArray = other.ByteArray;
            Width = other.Width;
            Height = other.Height;
        }

        /// <summary>
        /// Returns byte value of specified color channel.
        /// <para> </para>
        /// Do not use SET in iterative scenarios! (Use GetIndex() and SetRGB())
        /// </summary>
        /// <value>
        /// The <see cref="System.Byte"/>.
        /// </value>
        /// <param name="x">horizontal pixel position</param>
        /// <param name="y">vertical pixel position</param>
        /// <param name="channel">0 - Blue, 1 - Green, 2 - Red, 3 - Alpha</param>
        /// <returns></returns>
        public byte this[int x, int y, int channel]
        {
            get
            {
                return ByteArray[y * Width * channels + x * channels + channel];
            }
            set
            {
                ByteArray[y * Width * channels + x * channels + channel] = value;
            }
        }

        /// <summary>
        /// Gets the index in ByteArray.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="channel">The color channel.</param>
        /// <returns>Index in ByteArray</returns>
        public int GetIndex(int x, int y, int channel = 0)
        {
            return y * Width * channels + x * channels + channel;
        }

        /// <summary>
        /// Sets the RGB.
        /// </summary>
        /// <param name="index">The index in ByteArray.</param>
        /// <param name="R">The red channel value.</param>
        /// <param name="G">The green channel value.</param>
        /// <param name="B">The blue channel value.</param>
        public void SetRGB(int index, byte R, byte G, byte B)
        {
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
        }

        public void SetRGB(int index, byte val)
        {
            ByteArray[index] = ByteArray[index + 1] = ByteArray[index + 2] = val;
        }

        /// <summary>
        /// Sets the R, G, B channels to specified colors.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="color">The color.</param>
        public void SetRGB(int x, int y, byte color)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = ByteArray[index + 1] = ByteArray[index + 2] = color;
        }

        /// <summary>
        /// Sets the RGB.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="R">The red channel value.</param>
        /// <param name="G">The green channel value.</param>
        /// <param name="B">The blue channel value.</param>
        public void SetRGB(int x, int y, byte R, byte G, byte B)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
        }

        /// <summary>
        /// Sets the RGB.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="R">The red channel value. It will be cut to byte (If greater than 255 => 255. If lower than 0 => 0)</param>
        /// <param name="G">The green channel value. It will be cut to byte (If greater than 255 => 255. If lower than 0 => 0)</param>
        /// <param name="B">The blue channel value. It will be cut to byte (If greater than 255 => 255. If lower than 0 => 0)</param>
        public void SetRGB(int x, int y, long R, long G, long B)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = B.CutToByte();
            ByteArray[index + 1] = G.CutToByte();
            ByteArray[index + 2] = R.CutToByte();
        }

        /// <summary>
        /// Sets the RGB.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="R">The red channel value.</param>
        /// <param name="G">The green channel value.</param>
        /// <param name="B">The blue channel value.</param>
        /// <param name="A">The alpha channel value.</param>
        public void SetRGB(int x, int y, byte R, byte G, byte B, byte A)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
            ByteArray[index + 3] = A;
        }


        /// <summary>
        /// Sets the RGB.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="color">The int color representation (ARGB).</param>
        public unsafe void SetRGB(int x, int y, uint color)
        {
            // ARGB
            byte A = (byte)(color >> 24);
            byte R = (byte)((color << 8) >> 24);
            byte G = (byte)((color << 16) >> 24);
            byte B = (byte)((color << 24) >> 24);
            SetRGB(x, y, R, G, B, A);
        }

        /// <summary>
        /// Deeply copies the image by increasing using borders.
        /// </summary>
        /// <param name="region">The number of pixels to increase image's width & height by.</param>
        /// <returns>Created ImageWrapper.</returns>
        public unsafe ImageWrapper CopyImageIncreasingBorders(int region)
        {
            return CopyImageIncreasingBorders(region, region);
        }

        /// <summary>
        /// Deeply copies image increasing it by duplicating its' border pixels.
        /// <para> </para>
        /// Original image stays in the middle of returned image.
        /// </summary>
        /// <param name="regionX">The number of pixels to increase image's width by.</param>
        /// <param name="regionY">The number of pixels to increase image's height by.</param>
        /// <returns>Created ImageWrapper.</returns>
        public unsafe ImageWrapper CopyImageIncreasingBorders(int regionX, int regionY)
        {
            int newHeight = Height + regionY * 2;
            int newWidth = Width + regionX * 2;
            ImageWrapper returnImage = new ImageWrapper(newWidth, newHeight);

            long retIndex, index;
            fixed (byte* returnImg = returnImage.ByteArray)
            {
                fixed (byte* img = this.ByteArray)
                {
                    for (int y = 0; y < returnImage.Height; ++y)
                        for (int x = 0; x < returnImage.Width; ++x)
                        {
                            retIndex = returnImage.GetIndex(x, y, 0);

                            // if coordinates are in the region where the "old" image should be.
                            if (x >= regionX && x < regionX + this.Width && y >= regionY && y < regionY + this.Height)
                            {
                                index = GetIndex(x - regionX, y - regionY, 0);

                                returnImg[retIndex] = img[index];
                                returnImg[retIndex + 1] = img[index + 1];
                                returnImg[retIndex + 2] = img[index + 2];
                                returnImg[retIndex + 3] = img[index + 3];
                                //*((uint*)(returnImg + retIndex)) = *((uint*)(img + index));
                            }
                            else  // else use the %border pixel
                            {
                                int xUse = 0;
                                int yUse = 0;

                                if (x > regionX)
                                    xUse = this.Width - 1;

                                if (y > regionY)
                                    yUse = this.Height - 1;
                                index = GetIndex(xUse, yUse, 0);

                                returnImg[retIndex] = img[index];
                                returnImg[retIndex + 1] = img[index + 1];
                                returnImg[retIndex + 2] = img[index + 2];
                                returnImg[retIndex + 3] = img[index + 3];
                                //*((uint*)(returnImg + retIndex)) = *((uint*)(img + index));
                            }

                        }
                }
            }
            return returnImage;
        }

        /// <summary>
        /// Deeply copies image increasing it by duplicating specified pixel.
        /// <para> </para>
        /// Original image stays in the middle of returned image.
        /// </summary>
        /// <param name="region">The number of pixels to increase image by.</param>
        /// <param name="R">The red channel of the duplicating pixel.</param>
        /// <param name="G">The green channel of the duplicating pixel.</param>
        /// <param name="B">The blue channel of the duplicating pixel.</param>
        /// <param name="A">The alpha channel of the duplicating pixel.</param>
        /// <returns></returns>
        public ImageWrapper CopyImageBySpecifiedPixel(int region, byte R, byte G, byte B, byte A)
        {
            int newHeight = Height + region * 2;
            int newWidth = Width + region * 2;
            ImageWrapper returnImage = new ImageWrapper(newWidth, newHeight);

            for (int y = 0; y < returnImage.Height; ++y)
                for (int x = 0; x < returnImage.Width; ++x)
                {
                    long index = returnImage.GetIndex(x, y, 0);
                    // if coordinates are in the region where the "old" image should be.
                    if (x >= region && x < region + this.Width && y >= region && y < region + this.Height)
                    {
                        returnImage.ByteArray[index] = this[x - region, y - region, 0];
                        returnImage.ByteArray[index + 1] = this[x - region, y - region, 1];
                        returnImage.ByteArray[index + 2] = this[x - region, y - region, 2];
                        returnImage.ByteArray[index + 3] = this[x - region, y - region, 3];
                    }
                    else
                    {
                        returnImage.ByteArray[index] = R;
                        returnImage.ByteArray[index + 1] = G;
                        returnImage.ByteArray[index + 2] = B;
                        returnImage.ByteArray[index + 3] = A; // ALPHA
                    }
                }

            return returnImage;
        }

        public int[] BlueHistogramData()
        {
            int[] result = new int[256];
            byte[] channelArray = ByteArray.Where((item, index) => index % 4 == 0).ToArray();
            //for (int i = 0; i < ByteArray.Length; i += 4)
            //    ++result[ByteArray[i]];
            for (int i = 0; i <= 255; ++i)
                result[i] = channelArray.Count(channel => channel == i);
            return result;
        }

        /// <summary>
        /// Checks if image is in gray scale
        /// </summary>
        /// <returns>
        /// True if image is in gray scale, otherwise false
        /// </returns>
        public unsafe bool IsInGrayScale()
        {
            // even if you decode as BGRA8 you still get it like this: //that's weird, isn't it?
            const uint redMask = 0xFF0000;
            const uint greenMask = 0xFF00;
            const uint blueMask = 0xFF;

            fixed (byte* ptr = ByteArray)
            {
                byte* pixel = ptr;
                byte* maximumPtr = ptr + (Height * Width + 4);
                byte R, G, B;

                while (pixel < maximumPtr)
                {
                    R = (byte)((*((uint*)pixel) & redMask) >> 16);
                    G = (byte)((*((uint*)pixel) & greenMask) >> 8);
                    B = (byte)(*((uint*)pixel) & blueMask);

                    if (R != B || R != G)
                        return false;
                    pixel += 4;
                }
            }
            return true;
        }

        /// <summary>
        /// Converts ImageWrapper to the WriteableBitmap.
        /// </summary>
        /// <returns>Task that converts IW to WB.</returns>
        public async Task<WriteableBitmap> ToWriteableBitmap()
        {
            WriteableBitmap result = new WriteableBitmap(Width, Height);

            using (Stream stream = result.PixelBuffer.AsStream())
                await stream.WriteAsync(ByteArray, 0, ByteArray.Length);

            return result;
        }



        public bool IsInImageBounds(int x, int y)
        {
            return (x >= 0 && y >= 0 && x < Width && y < Height);
        }

        public bool IsInImageBounds(CoordPoint cp)
        {
            return (cp.X >= 0 && cp.Y >= 0 && cp.X < Width && cp.Y < Height);
        }


        /// <summary>
        /// Crops the specified image.
        /// </summary>
        /// <param name="image">The ImageWrapper to crop from.</param>
        /// <param name="TopLeft">The top left pixel.</param>
        /// <param name="BottomRight">The bottom right pixel.</param>
        /// <returns>Cropped image.</returns>
        /// <exception cref="System.ArgumentException">
        /// BottomRight.X is greater than TopLeft.X (should be lower)
        /// or
        /// BottomRight.Y is lower than TopLeft.Y (should be greater)
        /// or
        /// TopLeft point is out of image bounds
        /// or
        /// BottomRight point is out of image bounds
        /// </exception>
        public unsafe ImageWrapper Crop(CoordPoint TopLeft, CoordPoint BottomRight)
        {

            #region Debug exceptions // TODO: Change to return null? and Debug.Assert?
#if DEBUG
            if (TopLeft.X > BottomRight.X)
                throw new ArgumentException("BottomRight.X is lower than TopLeft.X (should be greater)");
            else if (TopLeft.Y > BottomRight.Y)
                throw new ArgumentException("BottomRight.Y is lower than TopLeft.Y (should be greater)");
            else if (!IsInImageBounds(TopLeft))
                throw new ArgumentException("TopLeft point is out of image bounds");
            else if (!IsInImageBounds(BottomRight))
                throw new ArgumentException("BottomRight point is out of image bounds");
#endif
            #endregion

            ImageWrapper result = new ImageWrapper(
                BottomRight.X - TopLeft.X + 1,
                BottomRight.Y - TopLeft.Y + 1
            );

            // copying rectangle
            fixed (byte* sourcePtr = ByteArray, resultHelpPtr = result.ByteArray)
            {
                uint* resultPtr = (uint*)resultHelpPtr;
                for (int y = TopLeft.Y; y <= BottomRight.Y; ++y)
                {
                    int index = GetIndex(TopLeft.X, y);
                    for (int x = TopLeft.X; x <= BottomRight.X; ++x)
                    {
                        *resultPtr++ = *(uint*)(sourcePtr + index);
                        index += 4;
                    }
                }
            }

            return result;
        }

        public unsafe void ClearImage(uint color)
        {
            fixed (byte* fixedPtr = ByteArray)
            {
                uint* ptr = (uint*)fixedPtr;
                for (int index = 0; index < ByteArray.Length / 4; ++index)
                    *ptr++ = color;
            }
        }

        public unsafe uint GetRGB(int x, int y)
        {
            fixed (byte* ptr = ByteArray) // TODO: does pinning and unpinning slows the program?
                return *(uint*)(ptr + GetIndex(x, y));
        }

        ///// <summary>
        ///// <para> Calculates index offset for uint* pixel ptr in ByteArray </para>
        ///// <para> Assumes that ByteArray is fixed! </para>
        ///// </summary>
        ///// <param name="x">The x coordinate.</param>
        ///// <param name="y">The y coordinate.</param>
        ///// <returns>uint ptr to pixel</returns>
        //public unsafe uint* GetPixel(int x, int y)
        //{
        //    fixed (byte* ptr = ByteArray)
        //        return (uint*)(ptr + GetIndex(x, y));
        //}
    }
}