﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib
{
    public class Brightness
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Brightness"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        public Brightness(ImageWrapper source) : this(source.ByteArray, source.Width, source.Height)
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="Brightness"/> class.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Brightness(byte[] byteArray, int width, int height)
        {
            BrightnessByte = new byte[byteArray.Length / 4];

            for (int index = 0; index < byteArray.Length; index += 4)
                BrightnessByte[index/4] = ((byteArray[index] + byteArray[index + 1] + byteArray[index + 2]) / 3).CutToByte();

            Width = width;
            Height = height;
        }

        /// <summary>
        /// Gets the index in BrightnessByte array of specified x, y images coordinates.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>Index in BrightnessByte array.</returns>
        public int GetIndex(int x, int y)
        {
            return y * Width + x;
        }

        /// <summary>
        /// The brightness byte array
        /// </summary>
        public byte[] BrightnessByte;

        /// <summary>
        /// <para> Gets or sets the BrightnessByte array. </para>
        /// <para> This method may be slow in iterative scenario. Consider using [index]. </para>
        /// </summary>
        /// <param name="y">The y coordinate.</param>
        /// <param name="x">The x coordinate.</param>
        public byte this[int y, int x]
        {
            get
            {
                return BrightnessByte[GetIndex(x, y)];
            }
            set
            {
                BrightnessByte[GetIndex(x, y)] = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Byte"/> at the specified index.
        /// </summary>
        /// <value>
        /// The <see cref="System.Byte"/>.
        /// </value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public byte this[int index]
        {
            get
            {
                return BrightnessByte[index];
            }
            set
            {
                BrightnessByte[index] = value;
            }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width { get; private set; }

        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height { get; private set; }
    }
}
