﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib
{
    public static class Extensions
    {

        /// <summary>
        /// Cuts the color range. (If greater than 255 => 255. If lower than 0 => 0. Otherwise the specified value)
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>val > 255 ? 255 : val < 0 ? 0 : val;</returns>
        public static byte CutToByte(this int val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        /// <summary>
        /// Cuts the color range. (If greater than 255 => 255. If lower than 0 => 0. Otherwise the specified value)
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>val > 255 ? 255 : val < 0 ? 0 : val;</returns>
        public static byte CutToByte(this long val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }
    }
}
