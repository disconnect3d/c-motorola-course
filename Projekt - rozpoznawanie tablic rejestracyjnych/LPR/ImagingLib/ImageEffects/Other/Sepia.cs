﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Other
{
    public class Sepia : IImageEffect
    {
        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("Sepia_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("SepiaDescription"); } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            fixed (byte* ptr = image.ByteArray)
                for (int index = 0; index < image.ByteArray.Length; index += 4)
                {
                    int G = ptr[index + 1] + (int)SepiaParameter;
                    int R = ptr[index + 2] + 2 * (int)SepiaParameter;
                    ptr[index + 2] = R.CutToByte();
                    ptr[index + 1] = G.CutToByte();
                }
            return image;
        }
        public double SepiaParameter { get; set; }

    }
}
