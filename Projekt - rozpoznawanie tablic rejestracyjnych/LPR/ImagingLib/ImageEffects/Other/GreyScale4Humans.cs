﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Other
{
    public class GreyScale4Humans : IImageEffect
    {

        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("GreyScale4HumansName"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("GreyScale4HumansDescription"); } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            int index = 0;
            for (int y = 0; y < image.Height; ++y)
            {
                for (int x = 0; x < image.Width; ++x)
                {
                    image.ByteArray[index] = image.ByteArray[index + 1] = image.ByteArray[index + 2]
                        = (byte)(image.ByteArray[index] * 0.11 + image.ByteArray[index + 1] * 0.59 + image.ByteArray[index + 2] * 0.3);
                    index += 4;
                }
            }
            return image;
        }
    }
}
