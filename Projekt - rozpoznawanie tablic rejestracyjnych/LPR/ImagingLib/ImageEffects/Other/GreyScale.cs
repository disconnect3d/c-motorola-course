﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Other
{
    public class GreyScale : IImageEffect
    {
        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("GreyScale_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("GreyScaleDescription"); } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            fixed (byte* ptr = image.ByteArray)
                for (int index = 0; index < image.ByteArray.Length; index += 4)
                    ptr[index] = ptr[index + 1] = ptr[index + 2]
                        = (byte)((ptr[index] + ptr[index + 1] + ptr[index + 2]) / 3);
            return image;
        }
    }
}
