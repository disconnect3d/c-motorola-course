﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Other
{
    public class Negative : IImageEffect
    {
        public String Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("Negative_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("NegativeDescription"); } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image.ChangeByteArray(image.ByteArray.Select((channel, index) => index % 4 == 3 ? channel : (byte)(255 - channel)).ToArray());
            return image;
        }
    }
}
