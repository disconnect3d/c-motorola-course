﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib.ImageEffects.Morphology
{
    public class MorphologyBase
    {
        protected static readonly bool[,] Mask = new bool[3, 3]
        {
            {true,  true,    true},
            {true,  false,   true},
            {true,  true,    true},
        };

        protected const int MASK_LEN = 1;
    }
}
