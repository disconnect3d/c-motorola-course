﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Morphology
{
    public class Erosion : MorphologyBase, IImageEffect
    {

        public string Name
        {
            get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("Erosion_Name"); }

        }

        public string Description
        {
            get { throw new NotImplementedException(); }
        }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            ImageWrapper result = new ImageWrapper(image);

            for (int h = MASK_LEN; h < image.Height - MASK_LEN; ++h)
                for (int w = MASK_LEN; w < image.Width - MASK_LEN; ++w)
                {
                    for (int y = -MASK_LEN; y <= MASK_LEN; ++y)
                        for (int x = -MASK_LEN; x <= MASK_LEN; ++x)
                        {
                            if (Mask[x + 1, y + 1] && image[w + x, h + y, 0] == 255)
                            {
                                result.SetRGB(w, h, Byte.MaxValue);
                                goto EndWorkOnThisPixel;
                            }
                        }
                EndWorkOnThisPixel:
                    continue;
                }

            return result;
        }
    }
}
