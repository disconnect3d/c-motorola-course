﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;

namespace ImagingLib.ImageEffects.Morphology
{
    public class Dilation : MorphologyBase, IImageEffect
    {

        public string Name
        {
            get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("Dilation_Name"); }
        }

        public string Description
        {
            get { throw new NotImplementedException(); }
        }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            ImageWrapper result = new ImageWrapper(image);

            for (int h = MASK_LEN; h < image.Height - MASK_LEN; ++h)
                for (int w = MASK_LEN; w < image.Width - MASK_LEN; ++w)
                {
                    for (int y = -MASK_LEN; y <= MASK_LEN; ++y)
                        for (int x = -MASK_LEN; x <= MASK_LEN; ++x)
                        {
                            if (Mask[x + 1, y + 1] && image[w + x, h + y, 0] == 0)
                            {
                                result.SetRGB(w, h, 0);
                                goto EndWorkOnThisPixel;
                            }
                        }
                EndWorkOnThisPixel:
                    continue;
                }

            return result;
        }
    }
}
