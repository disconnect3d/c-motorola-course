﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using ImagingLib;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects
{
    public class Rotation : IImageEffect
    {
        public Rotation()
        {
            Alpha = 1.0;
        }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            // 1) Take old image - count coordinates of its corners on the new image by using formula:
            // newX = cos(rad) * oldX - sin(rad) * oldY
            // newY = sin(rad) * oldX + cos(rad) * oldY
            // 2) by using minX, maxX, minY, maxY count newWidth, newHeight
            // 3) create new image with black background
            // 4) iterate over the new image, and calculate the originalX, originalY by using formula:
            // originalX = cos(-rad) * (x + xMin) - sin(-rad) * (y + yMin)
            // originalY = sin(-rad) * (x + xMin) + cos(-rad) * (y + yMin)
            // if the calculated coordinates lies in the original image, apply its colour to the result image

            double rad = (Alpha / 180.0) * Math.PI;
            double cos = Math.Cos(Alpha);
            double sin = Math.Sin(Alpha);

            // TOP LEFT CORNER
            int xi = 0;
            int yi = 0;
            int minX = xi, maxX = xi, minY = yi, maxY = yi;

            // TOP RIGHT CORNER
            xi = (int)image.Width - 1;
            // yi = 0
            int countedX = (int)(Math.Cos(rad) * xi - Math.Sin(rad) * yi);
            int countedY = (int)(Math.Sin(rad) * yi + Math.Cos(rad) * yi);

            if (minX > countedX)
                minX = countedX;
            else if (maxX < countedX)
                maxX = countedX;

            if (minY > countedY)
                minY = countedY;
            else if (maxY < countedY)
                maxY = countedY;

            // BOTTOM RIGHT CORNER
            //xi = (int)image.Width - 1;
            yi = (int)image.Height - 1;
            countedX = (int)(Math.Cos(rad) * xi - Math.Sin(rad) * yi);
            countedY = (int)(Math.Sin(rad) * yi + Math.Cos(rad) * yi);

            if (minX > countedX)
                minX = countedX;
            else if (maxX < countedX)
                maxX = countedX;

            if (minY > countedY)
                minY = countedY;
            else if (maxY < countedY)
                maxY = countedY;

            // BOTTOM LEFT CORNER
            xi = 0;
            //yi = (int)image.Height - 1;
            countedX = (int)(Math.Cos(rad) * xi - Math.Sin(rad) * yi);
            countedY = (int)(Math.Sin(rad) * yi + Math.Cos(rad) * yi);

            if (minX > countedX)
                minX = countedX;
            else if (maxX < countedX)
                maxX = countedX;

            if (minY > countedY)
                minY = countedY;
            else if (maxY < countedY)
                maxY = countedY;

            int newWidth = maxX - minX;
            int newHeight = maxY - minY;

            ImageWrapper returnImage = new ImageWrapper(newWidth, newHeight);

            // P(i,j) = P[cos(-r)*i-sin(-r)*j,sin(-r)*i+cos(-r)*j]
            cos = Math.Cos(-Alpha);
            sin = Math.Sin(-Alpha);

            fixed (byte* ptr = image.ByteArray)
            {
                for (int y = 0; y < returnImage.Height; ++y)
                    for (int x = 0; x < returnImage.Width; ++x)
                    {
                        //int x = (int)(Math.Cos(-rad) * (i + przesX) - Math.Sin(-rad) * (j + przesY)); 
                        //int y = (int)(Math.Sin(-rad) * (i + przesX) + Math.Cos(-rad) * (j + przesY));
                        int originalX = (int)(Math.Cos(-rad) * (x + minX) - Math.Sin(-rad) * (y + minY));
                        int originalY = (int)(Math.Sin(-rad) * (x + minX) + Math.Cos(-rad) * (y + minY));
                        if (originalX >= 0 && originalX < image.Width && originalY >= 0 && originalY < image.Height)
                        {
                            int index = (int)image.GetIndex(originalX, originalY, 0);
                            byte B = ptr[index];
                            byte G = ptr[index + 1];
                            byte R = ptr[index + 2];
                            returnImage.SetRGB(x, y, R, G, B);
                        }
                    }
            }
            return returnImage;
        }

        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("Rotation_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("RotationDescription"); } }

        /// <summary>
        /// Rotation angle
        /// </summary>
        public double Alpha { get; set; }



    }
}
