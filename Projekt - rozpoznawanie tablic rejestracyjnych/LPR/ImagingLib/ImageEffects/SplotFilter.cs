﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects
{

    public class SplotFilter : IImageEffect
    {

        public int[,] KernelMask;
        public int KernelMaskW { get; set; }
        public int KernelMaskH { get; set; }

        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("SplotFilter_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("SplotFilterDescription"); } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            ImageWrapper imageHelper = image.CopyImageIncreasingBorders(1);
            System.Diagnostics.Stopwatch xxx = new System.Diagnostics.Stopwatch();
            xxx.Start();

            fixed (byte* ptr = imageHelper.ByteArray)
            {
                for (int y = KernelMaskH / 2; y < imageHelper.Height - KernelMaskH / 2; ++y)
                    for (int x = KernelMaskW / 2; x < imageHelper.Width - KernelMaskW / 2; ++x)
                    {
                        int B = 0;
                        int G = 0;
                        int R = 0;

                        int maskSum = 0;

                        for (int i = -KernelMaskH / 2; i <= KernelMaskH / 2; ++i)
                            for (int j = -KernelMaskW / 2; j <= KernelMaskW / 2; ++j)
                            {
                                int index = imageHelper.GetIndex(x + j, y + i);
                                int mask = KernelMask[i + KernelMaskH / 2, j + KernelMaskW / 2];
                                maskSum += mask;
                                B += ptr[index + 0] * mask;
                                G += ptr[index + 1] * mask;
                                R += ptr[index + 2] * mask;
                            }

                        B /= maskSum;
                        G /= maskSum;
                        R /= maskSum;

                        image.SetRGB(x - 1, y - 1, R.CutToByte(), G.CutToByte(), B.CutToByte());
                    }
            }
            System.Diagnostics.Debug.WriteLine(xxx.ElapsedMilliseconds);
            return image;
        }
    }
}
