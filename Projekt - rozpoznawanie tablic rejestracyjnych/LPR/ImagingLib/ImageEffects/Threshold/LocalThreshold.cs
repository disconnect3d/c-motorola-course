﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using ImagingLib;
using ImagingLib.ImageEffects.Other;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Threshold
{
    public class LocalThreshold : RegionThresholdBase, IImageEffect
    {
        public LocalThreshold()
        {
            Region = 15;
        }

        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("LocalThreshold_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("LocalThresholdDescription"); } }
        //*
        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);
            ImageWrapper resultImage = new ImageWrapper(image);

            for (int y = 0; y < image.Height; ++y)
                for (int x = 0; x < image.Width; ++x)
                {
                    int threshold = 0;
                    for (int xRegion = -Region / 2; xRegion <= Region / 2; ++xRegion)
                    {
                        for (int yRegion = -Region / 2; yRegion <= Region / 2; ++yRegion)
                        {
                            int xCopy = xRegion;
                            int yCopy = yRegion;
                            while (x + xCopy < 0)
                                ++xCopy;
                            while (x + xCopy >= image.Width)
                                --xCopy;

                            while (y + yCopy < 0)
                                ++yCopy;
                            while (y + yCopy >= image.Height)
                                --yCopy;

                            threshold += image[x + xCopy, y + yCopy, 0];
                        }
                    }
                    threshold /= Region * Region;
                    resultImage.SetRGB(x, y, image[x, y, 0] < threshold.CutToByte() ? (byte)0 : (byte)255);
                }

            return resultImage;
        }
    }
}
