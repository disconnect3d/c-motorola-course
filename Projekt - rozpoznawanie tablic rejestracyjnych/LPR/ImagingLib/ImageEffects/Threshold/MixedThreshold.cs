﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using ImagingLib;
using ImagingLib.ImageEffects.Other;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Threshold
{	
    public class MixedThreshold : RegionAndDeviationThresholdBase, IImageEffect
    {
        public MixedThreshold()
        {
            Region = 15;
            Deviation = 25;
        }
        
        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("MixedThreshold_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("MixedThresholdDescription"); } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);
            
            int globalThreshold = GlobalThreshold.GetGlobalThresholdFromGrayScaleImage(image.ByteArray);
            
            ImageWrapper imageHelper = image.CopyImageIncreasingBorders(Region);

            for (int y = Region; y < image.Height + Region - 1; ++y)
                for (int x = Region; x < image.Width + Region - 1; ++x)
                {
                    int threshold = 0;
                    for (int xRegion = -Region / 2; xRegion <= Region / 2; ++xRegion)
                        for (int yRegion = -Region / 2; yRegion <= Region / 2; ++yRegion)
                            threshold += imageHelper[x + xRegion, y + yRegion, 0];

                    threshold = (threshold / (Region * Region)).CutToByte();

                    if (Math.Abs(globalThreshold - threshold) > Deviation)
                    {
                        if (threshold < globalThreshold - Deviation)
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold - Deviation) ? (byte)0 : (byte)255);
                        else
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold + Deviation) ? (byte)0 : (byte)255);
                    }
                    else
                        image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < threshold ? (byte)0 : (byte)255);
                }

            return image;


            /* old implementation
            System.Diagnostics.Stopwatch xyz = new System.Diagnostics.Stopwatch(); xyz.Start();
            image = new GreyScale().ApplyEffect(image);
            int globalThreshold = GlobalThreshold.GetGlobalThresholdFromGrayScaleImage(image.ByteArray);
            ImageWrapper resultImage = new ImageWrapper(image);

            for (long y = 0; y < image.Height; ++y)
                for (long x = 0; x < image.Width; ++x)
                {
                    long threshold = 0;
                    for (long xRegion = -region / 2; xRegion <= region / 2; ++xRegion)
                    { 
                        for (long yRegion = -region / 2; yRegion <= region / 2; ++yRegion)
                        {
                            long xCopy = xRegion;
                            long yCopy = yRegion;
                            while (x + xCopy < 0)
                                ++xCopy;
                            while (x + xCopy > image.MaxX())
                                --xCopy;

                            while (y + yCopy < 0)
                                ++yCopy;
                            while (y + yCopy > image.MaxY())
                                --yCopy;

                            threshold += image[(uint)(x + xCopy), (uint)(y + yCopy), 0];
                        }
                    }
                    threshold = ImageWrapper.CutColorRange(threshold / (region * region));
                    //if local > (global - odchylenie) && local < (global + odchylenie) pixel = global +/- odchylenie else pixel = global ?
                    if (Math.Abs(globalThreshold - threshold) > deviation)
                        resultImage.SetRGB(x, y, image[x, y, 0] < globalThreshold ? (byte)0 : (byte)255);
                    else
                        resultImage.SetRGB(x, y, image[x, y, 0] < threshold ? (byte)0 : (byte)255);
                }

            System.Diagnostics.Debug.WriteLine("Incrase by Copy = " + xyz.ElapsedMilliseconds);
            return resultImage;
              // */
        }
    }
}
