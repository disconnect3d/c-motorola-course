﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib.ImageEffects.Threshold
{
    public abstract class RegionAndDeviationThresholdBase : RegionThresholdBase
    {
        public int Deviation { get; set; }
    }
}
