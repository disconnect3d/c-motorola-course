﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.ImageEffects.Other;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.Threshold
{
    public class BernsenThreshold : RegionAndDeviationThresholdBase, IImageEffect
    {
        public BernsenThreshold()
        {
            Region = 15;
            Deviation = 15;
        }
            
        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("BernsenThreshold_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("BernsenThresholdDescription"); } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);

            int globalThreshold = GlobalThreshold.GetGlobalThresholdFromGrayScaleImage(image.ByteArray);

            ImageWrapper imageHelper = image.CopyImageIncreasingBorders(Region);

            for (int y = Region; y < image.Height + Region - 1; ++y)
                for (int x = Region; x < image.Width + Region - 1; ++x)
                {
                    byte min = imageHelper[x-1, y-1, 0];
                    byte max = min;

                    long threshold = 0;
                    for (int xRegion = -Region / 2; xRegion <= Region / 2; ++xRegion)
                        for (int yRegion = -Region / 2; yRegion <= Region / 2; ++yRegion)
                        {
                            byte taken = imageHelper[x + xRegion, y + yRegion, 0];
                            if (min > taken)
                                min = taken;
                            else if (max < taken)
                                max = taken;
                        }

                    threshold = (min + max) / 2;

                    if (Math.Abs(globalThreshold - threshold) > Deviation)
                    {
                        if (threshold < globalThreshold - Deviation)
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold - Deviation) ? (byte)0 : (byte)255);
                        else
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold + Deviation) ? (byte)0 : (byte)255);
                    }
                    else
                        image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < threshold ? (byte)0 : (byte)255);
                }

            return image;
        }
    }
}
