﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.ImageUtils;

namespace ImagingLib.ImageEffects.Thinning
{
    /// <summary>
    /// Thinning algorithm to delete 2 pixels neighbours when there should be only one pixel.
    /// Written by Dominik Czarnota
    /// </summary>
    public class AfterK3MThinning : IImageEffect
    {
        /// <summary>
        /// <para> It assumes that image is in greyscale! </para>
        /// </summary>
        /// <param name="objColor"></param>
        /// <param name="bgColor"></param>
        public AfterK3MThinning(FindObjects.ObjColor objColor)
        {
            if (objColor == FindObjects.ObjColor.Black)
            {
                this.ObjectColor = Byte.MinValue;
                this.BackgroundColor = Byte.MaxValue;
            }
            else
            {
                this.ObjectColor = Byte.MaxValue;
                this.BackgroundColor = Byte.MinValue;
            }
        }

        public string Name { get { return "AfterK3MThinning"; } }

        public string Description { get { return "Additional thinning used after K3M algorithm"; } }
        
        public ImageWrapper ApplyEffect(ImageWrapper source)
        {
            var searchEngine = new FindObjects(ObjectColor == Byte.MinValue ? FindObjects.ObjColor.Black : FindObjects.ObjColor.White);


            foreach (var imageObject in searchEngine.GetImageObjects(source))
                foreach (CoordPoint cp in imageObject)
                {
                    int x = cp.X;
                    int y = cp.Y;

                    int neighbourCount = 0;
                    source.ByteArray[source.GetIndex(x, y)] = ObjectColor;

                    /// looking at neighbours
                    for (int i = -1; i <= 1; ++i)
                        for (int j = -1; j <= 1; ++j)
                        {
                            if (i == 0 && j == 0)
                                continue;

                            int index = source.GetIndex(x + j, y + i);
                            if (source.ByteArray[index] == ObjectColor)
                                ++neighbourCount;
                        }
                    if (neighbourCount >= 2)
                    {
                        // TOP and (TOP-LEFT or TOP-RIGHT)
                        if (source.ByteArray[source.GetIndex(x, y - 1)] == ObjectColor &&
                            (source.ByteArray[source.GetIndex(x - 1, y - 1)] == ObjectColor || source.ByteArray[source.GetIndex(x + 1, y - 1)] == ObjectColor))
                            source.SetRGB(source.GetIndex(x, y - 1), (byte)BackgroundColor);

                        // LEFT and (LEFT-TOP or LEFT-BOT)
                        else if (source.ByteArray[source.GetIndex(x - 1, y)] == ObjectColor
                            && (source.ByteArray[source.GetIndex(x - 1, y - 1)] == ObjectColor || source.ByteArray[source.GetIndex(x - 1, y + 1)] == ObjectColor))
                            source.SetRGB(source.GetIndex(x, y - 1), (byte)BackgroundColor);

                        // BOT and (BOT-LEFT or BOT-RIGHT)
                        else if (source.ByteArray[source.GetIndex(x, y + 1)] == ObjectColor
                            && (source.ByteArray[source.GetIndex(x - 1, y + 1)] == ObjectColor || source.ByteArray[source.GetIndex(x + 1, y + 1)] == ObjectColor))
                            source.SetRGB(source.GetIndex(x, y - 1), (byte)BackgroundColor);

                        // RIGHT and (RIGHT-BOT or RIGHT-TOP)
                        else if (source.ByteArray[source.GetIndex(x + 1, y)] == ObjectColor
                            && (source.ByteArray[source.GetIndex(x + 1, y + 1)] == ObjectColor || source.ByteArray[source.GetIndex(x + 1, y - 1)] == ObjectColor))
                            source.SetRGB(source.GetIndex(x, y - 1), (byte)BackgroundColor);
                    }
                }


            return source;
        }

        public bool DoGrayscale { get; set; }

        public byte ObjectColor { get; set; }

        public byte BackgroundColor { get; set; }
    }
}
