﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib.ImageUtils;

namespace ImagingLib.ImageEffects.Thinning
{
    /// <summary>
    /// The algorithm assumes that image is binarized (black & white)
    /// </summary>
    public class KMM : IImageEffect
    {
        public KMM(uint objColor, uint bgColor)
        {
            this.ObjectColor = objColor;
            this.BackgroundColor = bgColor;
        }

        private uint ObjectColor;
        private uint BackgroundColor;


        public string Name
        {
            get { return "KMM_Thinning"; }
        }

        public string Description
        {
            get { throw new NotImplementedException(); }
        }


        private static readonly int[] A1 = new int[] { 3, 6, 7, 12, 14, 15, 24, 28, 30, 48, 56, 60, 96, 112, 120, 129, 131, 135, 192, 193, 195, 224, 225, 240 };
        private static readonly int[] A2 = new int[]
        {
            3, 5, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28,
			29, 30, 31, 48, 52, 53, 54, 55, 56, 60, 61, 62, 63, 65, 67, 69, 71,
			77, 79, 80, 81, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 97,
			99, 101, 103, 109, 111, 112, 113, 115, 116, 117, 118, 119, 120, 121,
			123, 124, 125, 126, 127, 131, 133, 135, 141, 143, 149, 151, 157, 159,
			181, 183, 189, 191, 192, 193, 195, 197, 199, 205, 207, 208, 209, 211,
			212, 213, 214, 215, 216, 217, 219, 220, 221, 222, 223, 224, 225, 227,
			229, 231, 237, 239, 240, 241, 243, 244, 245, 246, 247, 248, 249, 251,
			252, 253, 254, 255
        };

        private static readonly int[][] Checker = new int[][]
        {
			new int[] {128, 64, 32},
			new int[] {1, 0, 16},
			new int[] {2, 4, 8}
        };



        public unsafe ImageWrapper ApplyEffect(ImageWrapper source)
        {
            byte[,] tab = new byte[source.Width, source.Height];    // W, H so use it [x, y]

            fixed (byte* helperPtr = source.ByteArray)
            {
                uint* ptr = (uint*)helperPtr;
                for (int x = 0; x < source.Width; ++x)
                    for (int y = 0; y < source.Height; ++y)
                        if (source.GetRGB(x, y) == ObjectColor)
                            tab[x, y] = 1;
            }
            

            int max = 1000;
            bool sthChanged;

            do
            {
                sthChanged = false;
                for (int x = 1; x < source.Width - 1; ++x)
                    for (int y = 1; y < source.Height - 1; ++y)
                        if (tab[x, y] == 1)
                        {
                            if (tab[x + 1, y] == 0 ||
                                tab[x - 1, y] == 0 ||
                                tab[x, y + 1] == 0 ||
                                tab[x, y - 1] == 0)
                                tab[x, y] = 2;

                            else if (tab[x + 1, y + 1] == 0 ||
                                     tab[x - 1, y + 1] == 0 ||
                                     tab[x + 1, y - 1] == 0 ||
                                     tab[x - 1, y - 1] == 0)
                                tab[x, y] = 3;
                        }
                for (int x = 0; x < source.Width; ++x)
                    for (int y = 0; y < source.Height; ++y)
                        if (tab[x, y] == 2)
                        {
                            int sum = 0;
                            for (int i = 0; i < 3; i++)
                                for (int j = 0; j < 3; j++)
                                    sum += (tab[x + i - 1, y + j - 1] > 0 ? 1 : 0) * Checker[i][j];
                            if (Array.BinarySearch(A1, sum) > 0)
                                tab[x, y] = 4;
                        }
                for (int x = 0; x < source.Width; ++x)
                    for (int y = 0; y < source.Height; ++y)
                        if (tab[x, y] == 4)
                        {
                            int sum = 0;
                            for (int i = 0; i < 3; i++)
                                for (int j = 0; j < 3; j++)
                                    sum += (tab[x + i - 1, y + j - 1] > 0 ? 1 : 0) * Checker[i][j];
                            if (Array.BinarySearch(A2, sum) > 0)
                            {
                                tab[x, y] = 0;
                                sthChanged = true;
                            }
                            else
                                tab[x, y] = 1;
                        }
                for (int x = 0; x < source.Width; ++x)
                    for (int y = 0; y < source.Height; ++y)
                        if (tab[x, y] == 2)
                        {
                            int sum = 0;
                            for (int i = 0; i < 3; i++)
                                for (int j = 0; j < 3; j++)
                                    sum += (tab[x + i - 1, y + j - 1] > 0 ? 1 : 0) * Checker[i][j];
                            if (Array.BinarySearch(A2, sum) > 0)
                            {
                                tab[x, y] = 0;
                                sthChanged = true;
                            }
                            else
                                tab[x, y] = 1;
                        }
                for (int x = 0; x < source.Width; ++x)
                    for (int y = 0; y < source.Height; ++y)
                        if (tab[x, y] == 3)
                        {
                            int sum = 0;
                            for (int i = 0; i < 3; i++)
                                for (int j = 0; j < 3; j++)
                                    sum += (tab[x + i - 1, y + j - 1] > 0 ? 1 : 0) * Checker[i][j];
                            if (Array.BinarySearch(A2, sum) > 0)
                            {
                                tab[x, y] = 0;
                                sthChanged = true;
                            }
                            else
                                tab[x, y] = 1;
                        }
                --max;
            } while (sthChanged && max > 0);

            source.ClearImage(BackgroundColor);

            for (int x = 0; x < source.Width; ++x)
                for (int y = 0; y < source.Height; ++y)
                    if (tab[x, y] > 0)
                        source.SetRGB(x, y, ObjectColor);

            return source;
        }
    }
}
