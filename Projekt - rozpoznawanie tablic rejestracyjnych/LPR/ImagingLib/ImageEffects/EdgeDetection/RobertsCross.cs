﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagingLib;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;

namespace ImagingLib.ImageEffects.EdgeDetection
{
    public class RobertsCross : IImageEffect
    {
        public string Name { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("RobertsCross_Name"); } }
        public string Description { get { return ResourceLoader.GetForViewIndependentUse("Effects").GetString("RobertsCrossDescription"); } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            const int channels = 4;

            // creating most of the image
            for (int y = 0; y < image.Height; ++y)
            {
                for (int x = 0; x < image.Width; ++x)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        // (x, y)
                        int x0y0 = y * image.Width * channels + x * channels + i;

                        // if pixels are not on the right or bottom borders
                        if (y != image.Height - 1 && x != image.Width - 1)
                        {
                            // (x+1, y+1)
                            int x1y1 = x0y0 + image.Width * channels + channels;
                            // (x+1, y)
                            int x1y0 = x0y0 + channels;
                            // (x, y+1)
                            int x0y1 = x0y0 + image.Width * channels;

                            // tmp1 = (x, y) - (x+1, y+1)
                            int tmp1 = image.ByteArray[x0y0] - image.ByteArray[x1y1];
                            // tmp2 = (x+1, y) - (x, y+1)
                            int tmp2 = image.ByteArray[x1y0] - image.ByteArray[x0y1];

                            // (x,y) = (tmp1) + (tmp2)
                            image.ByteArray[x0y0] = (Math.Abs(tmp1) + Math.Abs(tmp2)).CutToByte();
                        }
                        else // set pixel to black color (each channel to 0) when on border
                            image.ByteArray[x0y0] = (byte)0;
                    }
                }
            }
            return image;
        }
    }
}
