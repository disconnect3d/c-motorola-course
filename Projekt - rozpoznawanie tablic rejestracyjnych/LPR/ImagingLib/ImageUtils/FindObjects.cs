﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib.ImageUtils
{
    public struct CoordPoint
    {
        public CoordPoint(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public int X;
        public int Y;

        public static bool operator ==(CoordPoint cp1, CoordPoint cp2)
        {
            return cp1.X == cp2.X && cp1.Y == cp2.Y;
        }

        public static bool operator !=(CoordPoint cp1, CoordPoint cp2)
        {
            return cp1.X != cp2.X || cp1.Y != cp2.Y;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class FindObjects
    {
        public enum ObjColor { Black, White }
        public FindObjects(ObjColor objectColor)
        {
            this.ObjectColor = objectColor == ObjColor.White ? byte.MaxValue : byte.MinValue;
            this.BackgroundColor = objectColor == ObjColor.White ? byte.MinValue : byte.MaxValue;
        }

        private byte ObjectColor { get; set; }
        private byte BackgroundColor { get; set; }

        public List<CoordPoint> GetImageObject(int x, int y, ImageWrapper source)
        {
            List<CoordPoint> result = new List<CoordPoint>();

            List<CoordPoint> helper = new List<CoordPoint>();

            const int helpColorValue = 1;
            source.ByteArray[source.GetIndex(x, y)] = helpColorValue;

            CoordPoint firstPoint = new CoordPoint(x, y);
            helper.Add(firstPoint);
            result.Add(firstPoint);

            while (helper.Count != 0)
            {
                CoordPoint popped = helper.ElementAt(0);
                helper.RemoveAt(0);

                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if ((i == 0 && j == 0) || !source.IsInImageBounds(popped.X + j, popped.Y + i))
                            continue;

                        int index = source.GetIndex(popped.X + j, popped.Y + i);
                        if (source.ByteArray[index] == ObjectColor)
                        {
                            source.ByteArray[index] = helpColorValue;
                            CoordPoint add = new CoordPoint(popped.X + j, popped.Y + i);
                            helper.Add(add);
                            result.Add(add);
                        }
                    }
            }

            return result;
        }

        public List<List<CoordPoint>> GetImageObjects(ImageWrapper source)
        {
            List<List<CoordPoint>> result = new List<List<CoordPoint>>();

            ImageWrapper copy = new ImageWrapper(source);

            int index = 0;
            for (int y = 0; y < copy.Height; ++y)
                for (int x = 0; x < copy.Width; ++x)
                {
                    if (copy.ByteArray[index] == ObjectColor)
                        result.Add(GetImageObject(x, y, copy));
                    index += 4;
                }
            return result;
        }

        // TODO: think about implementing this.
        //public enum HorizontalDirection { UpToBot, BotToUp };
        //public enum VerticalDirection { LeftToRight, RightToLeft };

        //public enum FindMode { Column, Rows };
        //public List<CoordPoint> FindFirstObject(ImageWrapper source, HorizontalDirection hDir, VerticalDirection vDir, FindMode fMode)
        //{
        //    int yFrom, yTo, xFrom, xTo;
        //    if (fMode == FindMode.Rows)
        //    {
        //        if (hDir == HorizontalDirection.UpToBot && vDir == VerticalDirection.LeftToRight)
        //        for(int y=0; y<source.Height; ++y)
        //        for (int x=0; x<source.Width; ++x)
        //            if (source)
        //    }
        //}

        protected List<CoordPoint> GetEndingPoints(List<CoordPoint> obj, ImageWrapper source)
        {
            List<CoordPoint> endingPoints = new List<CoordPoint>();
            foreach (CoordPoint cp in obj)
            {
                int neighbours = 0;
                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if (i == 0 && j == 0)
                            continue;

                        int x = cp.X + j;
                        int y = cp.Y + i;

                        if (!source.IsInImageBounds(cp.X + j, cp.Y + i))
                            continue;

                        if (source.ByteArray[source.GetIndex(x, y)] == ObjectColor)
                            ++neighbours;
                    }
                if (neighbours == 1)
                    endingPoints.Add(cp);
            }

            return endingPoints;
        }

        protected readonly Random rng;
    }
}
