﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib.ImageUtils
{
    public static class Drawing
    {
        #region Linear function
        /// <summary>
        /// Represents `a` and `b` params from y=a*x+b linear function formula
        /// </summary>
        public struct LinearFunctionParams
        {
            public double a;
            public double b;
        }


        public static LinearFunctionParams CalcLinearFuncParams(CoordPoint p1, CoordPoint p2)
        {
            double a = (double)(p1.Y - p2.Y) / (p1.X - p2.X);

            return new LinearFunctionParams()
            {
                a = a,
                b = p1.Y - (a * p1.X)
            };
        }

        public static Func<int, int> GetLinearDrawingFunc(LinearFunctionParams funcParams)
        {
            return new Func<int, int>((x) => (int)(funcParams.a * x + funcParams.b));
        }
        public static double GetPerpendicularParamA(double a)
        {
            return -1.0 / a;
        }

        public static double GetParamB(double y, double a, double x)
        {
            return y - a * x;
        }
        #endregion


        #region DrawLine
        /// <summary>
        /// Draw line between 2 points in specified color
        /// </summary>
        /// <param name="source">The ImageWrapper.</param>
        /// <param name="p1">First point</param>
        /// <param name="p2">Second point</param>
        /// <param name="Color">Color of drawn line</param>
        public static void DrawLine(ImageWrapper source, CoordPoint p1, CoordPoint p2, uint Color)
        {
            DrawLine(
                source,
                CalcLinearFuncParams(p1, p2),
                p1,
                p2,
                Color
            );
        }

        public static void DrawLine(ImageWrapper source, LinearFunctionParams funcParams, CoordPoint p1, CoordPoint p2, uint Color)
        {
            if (!source.IsInImageBounds(p1.X, p1.Y))
                throw new ArgumentException("CoordPoint p1 is out of image bounds");
            else if (!source.IsInImageBounds(p2.X, p2.Y))
                throw new ArgumentException("CoordPoint p2 is out of image bounds");


            if (p1.X != p2.X)
            {
                Func<int, int> foo = GetLinearDrawingFunc(funcParams);

                for (int x = Math.Min(p1.X, p2.X); x < Math.Max(p1.X, p2.X); ++x)
                {
                    int y1 = foo(x);
                    int y2 = foo(x + 1);

                    source.SetRGB(x, y1, Color);

                    int yStart = y2 > y1 ? y1 : y2;
                    int yEnd = y2 > y1 ? y2 : y1;

                    for (int yi = yStart + 1; yi < yEnd; ++yi)
                        source.SetRGB(x, yi, Color);
                }
            }
            else // vertical line
            {
                for (int y = Math.Min(p1.Y, p2.Y); y <= Math.Max(p1.Y, p2.Y); ++y)
                    source.SetRGB(p1.X, y, Color);
            }
        }

        /// <summary>
        /// Draw line using all possible x values
        /// </summary>
        /// <param name="context">BitmapContext which must have Write Access</param>
        /// <param name="funcParams">Linear function parameters</param>
        /// <param name="Color">Color of drawn line</param>
        public static void DrawLine(ImageWrapper source, LinearFunctionParams funcParams, uint Color)
        {
            Func<int, int> foo = GetLinearDrawingFunc(funcParams);

            for (int x = 0; x < source.Width; ++x)
            {
                int y1 = foo(x);
                int y2 = foo(x + 1);

                if (!source.IsInImageBounds(x, y1))
                    continue;

                source.SetRGB(x, y1, Color);

                int yStart = y2 > y1 ? y1 : y2;
                int yEnd = y2 > y1 ? y2 : y1;

                for (int yi = yStart + 1; yi < yEnd; ++yi)
                {
                    if (!source.IsInImageBounds(x, yi))
                        continue;
                    source.SetRGB(x, yi, Color);
                }
            }
        }
        #endregion

        /// <summary>
        /// Draw square around specified pixel. This function calls Debug.Assert if trying to draw out of image bounds.
        /// </summary>
        /// <param name="source">The ImageWrapper.</param>
        /// <param name="cp">Point around which we will draw the square</param>
        /// <param name="region">Half of the side size (if you want square to be of side size=3, pass 1 here)</param>
        /// <param name="Color">Square color</param>
        public static void DrawSquare(ImageWrapper source, CoordPoint cp, int region, uint Color)
        {
            for (int i = -region; i <= region; ++i)
                for (int j = -region; j <= region; ++j)
                {
                    int x = cp.X + j;
                    int y = cp.Y + i;
                    if (!source.IsInImageBounds(x, y))
                    {
                        Debug.Assert(false, "Trying to draw square out of image bounds");
                        continue;
                    }
                    source.SetRGB(x, y, Color);
                }
        }


        public static void PaintObject(ImageWrapper source, List<CoordPoint> obj, byte r, byte g, byte b)
        {
            foreach(CoordPoint cp in obj)
                source.SetRGB(cp.X, cp.Y, r, g, b);
        }

        public static void PaintObject(ImageWrapper source, List<CoordPoint> obj, uint color)
        {
            foreach (CoordPoint cp in obj)
                source.SetRGB(cp.X, cp.Y, color);
        }
    }
}
