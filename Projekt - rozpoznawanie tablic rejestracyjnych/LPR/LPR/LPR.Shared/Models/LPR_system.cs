﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ImagingLib;
using ImagingLib.ImageEffects.EdgeDetection;
using ImagingLib.ImageEffects.Other;
using ImagingLib.ImageEffects.Threshold;
using ImagingLib.ImageUtils;
using ImagingLib.ImageEffects.Thinning;
using System.Diagnostics;

namespace LPR.Models
{
    internal class Quadrangle
    {
        public Quadrangle(List<CoordPoint> pixels, CoordPoint topLeft, CoordPoint topRight, CoordPoint botRight, CoordPoint botLeft, CoordPoint midPoint)
        {
            this.Pixels = pixels;
            this.TopLeft = topLeft;
            this.TopRight = topRight;
            this.BotLeft = botLeft;
            this.BotRight = botRight;
            this.MidPoint = midPoint;
        }

        internal List<CoordPoint> Pixels;
        internal CoordPoint TopLeft;
        internal CoordPoint TopRight;
        internal CoordPoint BotRight;
        internal CoordPoint BotLeft;
        internal CoordPoint MidPoint;
    }

    internal class Edges
    {
        public Edges(List<CoordPoint> pixels, int top, int bot, int left, int right)
        {
            this.TopY = top;
            this.BotY = bot;
            this.LeftX = left;
            this.RightX = right;
        }
        internal int TopY;
        internal int BotY;
        internal int LeftX;
        internal int RightX;
    }

    public static class LPR_system
    {
        /// <summary>
        /// Localizes license plate on image.
        /// </summary>
        /// <param name="source">Non modified ImageWrapper containing car with polish license plate.</param>
        /// <returns>If found, ImageWrapper with cropped license plate, otherwise null.</returns>
        public static ImageWrapper Localize(ImageWrapper source)
        {
            ImageWrapper copy = new ImageWrapper(source);
            copy = new OtsuThreshold().ApplyEffect(copy);

            List<Quadrangle> candidates = LocalizeCandidates(copy);

            #region If otsu failed, try with Canny
            if (candidates.Count == 0)
            {
                copy = new ImageWrapper(source);
                copy = new Canny(30, 60).ApplyEffect(copy);

                candidates = LocalizeCandidates(copy);
            }
            #endregion

            #region Otsu & Canny failed, try with Sobel
            if (candidates.Count == 0)
            {
                copy = new ImageWrapper(source);
                copy = new GreyScale().ApplyEffect(copy);
                copy = new SobelFilter().ApplyEffect(copy);

                //THIS SHOULD STAY HERE BUT... OH WELL... it works without it...
                //copy = new OtsuThreshold().ApplyEffect(copy);

                candidates = LocalizeCandidates(copy);
            }
            #endregion

            if (candidates.Count == 0)
                return null;

            Quadrangle lp = candidates.Count > 1 ? ChooseLP(candidates) : candidates[0];

            //source.ChangeByteArray(copy.ByteArray);

            //Drawing.DrawLine(source, lp.TopLeft, lp.TopRight, unchecked((int)0xFFFF0000));
            //Drawing.DrawLine(source, lp.TopLeft, lp.BotLeft, unchecked((int)0xFFFF0000));
            //Drawing.DrawLine(source, lp.TopRight, lp.BotRight, unchecked((int)0xFFFF0000));
            //Drawing.DrawLine(source, lp.BotRight, lp.BotLeft, unchecked((int)0xFFFF0000));
            //Drawing.DrawSquare(source, lp.MidPoint, 1, unchecked((int)0xFF00FF00));

            const int supportX = 6;
            const int supportY = 4;

            lp.TopLeft.X -= supportX;
            lp.TopLeft.Y -= supportY;

            lp.BotRight.X += supportX;
            lp.BotRight.Y += supportY;

            return source.Crop(lp.TopLeft, lp.BotRight);
        }

        public static ImageWrapper PreprocessLicensePlate(ImageWrapper source)
        {
            const int delObjWithLowerPxThan = 15;

            source = new Normalization().ApplyEffect(source);
            source = new OtsuThreshold().ApplyEffect(source);

            // Another option - canny, negative, floodfill, k3m... but still, canny is not good for some images :-(
            // it also requires bigger pixel objects to be removed, which may remove "I" letter
            //source = new Canny(30, 60).ApplyEffect(source);
            //source = new Negative().ApplyEffect(source);

            var searchEngine = new FindObjects(FindObjects.ObjColor.Black);
            var blackObjs = searchEngine.GetImageObjects(source);

            if (blackObjs.Count == 0)
                return null;

            // assuming that first found object is the license plate border, so removing it
            Drawing.PaintObject(source, blackObjs[0], Byte.MaxValue, Byte.MaxValue, Byte.MaxValue);

            // deleting objects smaller than ~~ px
            foreach (List<CoordPoint> obj in blackObjs)
                if (obj.Count < delObjWithLowerPxThan)
                    Drawing.PaintObject(source, obj, Byte.MaxValue, Byte.MaxValue, Byte.MaxValue);


            // looking for objects in first 3 columns, deleting if there are any
            const int columnsCount = 3;
            for (int x = 0; x <= columnsCount; ++x)
                for (int y = 0; y < source.Height; ++y)
                    if (source[x, y, 0] == 0)
                    {
                        foreach (CoordPoint cp in searchEngine.GetImageObject(x, y, source))
                            source.SetRGB(cp.X, cp.Y, byte.MaxValue);
                        break;
                    }

            // the same in last 3 columns
            for (int x = source.Width - 1 - columnsCount; x < source.Width; ++x)
                for (int y = 0; y < source.Height; ++y)
                    if (source[x, y, 0] == 0)
                    {
                        foreach (CoordPoint cp in searchEngine.GetImageObject(x, y, source))
                            source.SetRGB(cp.X, cp.Y, byte.MaxValue);
                        break;
                    }

            source = new KMM(Def.BLACK, Def.WHITE).ApplyEffect(source);

            return source;
        }

        public static List<ImageWrapper> ExtractCharacters(ImageWrapper source)
        {
            List<ImageWrapper> characters = new List<ImageWrapper>();


            #region Adding characters by extracting objects - commented out, since objects may be cut
            //var searchEngine = new FindObjects(FindObjects.ObjColor.Black);
            //var objs = searchEngine.GetImageObjects(source);

            //// Pair of index and X lowest X coordinate in the object
            //Tuple<int, int>[] pairs = new Tuple<int, int>[objs.Count];
            //for (int i = 0; i < objs.Count; ++i)
            //    pairs[i] = new Tuple<int, int>(i, objs[i].OrderBy((x) => x.X).First().X);   // TODO: This may have low performance

            //var sortedPairs = pairs.OrderBy((x) => x.Item2);

            //List<List<CoordPoint>> sortedObjects = new List<List<CoordPoint>>();
            //foreach (var pair in sortedPairs)
            //    sortedObjects.Add(objs[pair.Item1]);

            //foreach (var obj in sortedObjects)
            //{
            //    Edges e = GetObjectEdges(obj);
            //    characters.Add(
            //        source.Crop(
            //            new CoordPoint(e.LeftX, e.BotY),
            //            new CoordPoint(e.RightX, e.TopY)
            //    ));
            //}
            #endregion

            // The algorithm below finds characters by iterating over columns
            // It works better than the above algorithm, because it marks objects like "C" or "G" which are cut by few pixels as one character
            // It still may have problem on letters like H, M, N that would be cut in the middle

            int left = -1, right = -1;
            bool foundObj = false;
            for (int x = 0; x < source.Width; ++x)
            {
                for (int y = 0; y < source.Height; ++y)
                {
                    if (source[x, y, 0] == 0)
                    {
                        foundObj = true;
                        if (left == -1)
                            left = x;
                        if (right < x)
                            right = x;
                        goto FOUND_OBJECT;
                    }
                }
                if (foundObj)
                {
                    foundObj = false;
                    ImageWrapper character = source.Crop(new CoordPoint(left, 0), new CoordPoint(right, source.Height - 1));
                    characters.Add(CutTopBot(character));

                    left = right = -1;
                }
                continue;

            FOUND_OBJECT:
                continue;

            }

            return characters;
        }

        public static string RecognizeCharacter(ImageWrapper source, int position)
        {

            Debug.WriteLine("Image W=" + source.Width + ", H=" + source.Height);
            Debug.WriteLine("Position = " + position);

            #region First try
            //double[] rows = new double[source.Height];
            //double[] cols = new double[source.Width];

            //int index = 0;
            //for (int y = 0; y < source.Height; ++y)
            //{
            //    double sum = 0;
            //    for (int x = 0; x < source.Width; ++x)
            //    {
            //        if (source.ByteArray[index] == 0) // if pixel is black
            //            ++sum;
            //        index += 4;
            //    }
            //    rows[y] = sum/source.Width;
            //}

            //for (int x = 0; x < source.Width; ++x)
            //{
            //    double sum = 0;
            //    for (int y = 0; y < source.Height; ++y)
            //    {
            //        if (source[x, y, 0] == 0) // if pixel is black
            //            ++sum;
            //    }
            //    cols[x] = sum/source.Height;
            //}
            //StringBuilder strBuilder = new StringBuilder();
            //foreach (double r in rows)
            //    strBuilder.AppendFormat("{0:0.00}, ", r);
            //Debug.WriteLine("Rows = " + strBuilder.ToString());

            //strBuilder.Clear();
            //foreach (double r in cols)
            //    strBuilder.AppendFormat("{0:0.00}, ", r);
            //Debug.WriteLine("Cols = " + strBuilder.ToString());
            #endregion

            double[] featureVector = new double[15];

            #region Calculating Feature Vector elements

            // Dividing image into 3x3 grid
            // Counting black pixels in each grid
            // Dividing each value by pixelCount
            #region Grid method (0-8 featureVector elements)
            const int colsCount = 3;
            const int rowCount = 4;
            int colIndex = source.Width / colsCount;
            int rowIndex = source.Height / rowCount;

            const int gridLength = colsCount * rowCount;

            // row 1
            featureVector[0] = CountBlackPixelsInRegion(source, 0, colIndex, 0, rowIndex);
            featureVector[1] = CountBlackPixelsInRegion(source, colIndex, 2 * colIndex, 0, rowIndex);
            featureVector[2] = CountBlackPixelsInRegion(source, 2 * colIndex, source.Width, 0, rowIndex);

            // row 2
            featureVector[3] = CountBlackPixelsInRegion(source, 0, colIndex, rowIndex, 2 * rowIndex);
            featureVector[4] = CountBlackPixelsInRegion(source, colIndex, 2 * colIndex, rowIndex, 2 * rowIndex);
            featureVector[5] = CountBlackPixelsInRegion(source, 2 * colIndex, source.Width, rowIndex, 2 * rowIndex);

            // row 3
            featureVector[6] = CountBlackPixelsInRegion(source, 0, colIndex, 2 * rowIndex, 3 * rowIndex);
            featureVector[7] = CountBlackPixelsInRegion(source, colIndex, 2 * colIndex, 2 * rowIndex, 3 * rowIndex);
            featureVector[8] = CountBlackPixelsInRegion(source, 2 * colIndex, source.Width, 2 * rowIndex, 3 * rowIndex);

            // row 4
            featureVector[9] = CountBlackPixelsInRegion(source, 0, colIndex, 3 * rowIndex, source.Height);
            featureVector[10] = CountBlackPixelsInRegion(source, colIndex, 2 * colIndex, 3 * rowIndex, source.Height);
            featureVector[11] = CountBlackPixelsInRegion(source, 2 * colIndex, source.Width, 3 * rowIndex, source.Height);

            double pixelCount = featureVector.Sum();

            for (int i = 0; i < gridLength; ++i)
                featureVector[i] /= pixelCount;
            #endregion

            const double coeff = 25.0;

            featureVector[12] = CountEdgePoints(source);         // counts points with 1 neighbours
            featureVector[13] = CountThreeWayPoints(source);     // counts pixels with 3 neighbours
            featureVector[14] = (double)source.Width / (double)source.Height;
            #endregion

            StringBuilder strBuilder = new StringBuilder();
            foreach (double r in featureVector)
                strBuilder.AppendFormat("{0:0.0000}, ", r);
            Debug.WriteLine("Rows = " + strBuilder.ToString());

            double lowestDiff = 0;

            if (CharactersFV.Length == 0)
                return "";

            for (int i = 0; i < gridLength; ++i)
                lowestDiff += Math.Sqrt(Math.Pow(featureVector[i] - CharactersFV[0].Item1[i], 2));
            lowestDiff += Math.Sqrt(Math.Pow((featureVector[12] - CharactersFV[0].Item1[12]) / coeff, 2));
            lowestDiff += Math.Sqrt(Math.Pow((featureVector[13] - CharactersFV[0].Item1[13]) / coeff, 2));
            lowestDiff += Math.Sqrt(Math.Pow((featureVector[14] - CharactersFV[0].Item1[14]) / coeff, 2));

            String letter = CharactersFV[0].Item2;

            for (int i = 0; i < CharactersFV.Length; ++i)
            {
                double diff = 0;
                for (int j = 0; j < gridLength; ++j)
                    diff += Math.Sqrt(Math.Pow(featureVector[j] - CharactersFV[i].Item1[j], 2));
                diff += Math.Sqrt(Math.Pow((featureVector[12] - CharactersFV[i].Item1[12]) / coeff, 2));
                diff += Math.Sqrt(Math.Pow((featureVector[13] - CharactersFV[i].Item1[13]) / coeff, 2));
                diff += Math.Sqrt(Math.Pow((featureVector[14] - CharactersFV[i].Item1[14]) / coeff, 2));

                if (lowestDiff > diff)
                {
                    lowestDiff = diff;
                    letter = CharactersFV[i].Item2;
                }
            }

            if (source.Width <= 3)
                return "I";
            return letter;
        }

        private static readonly Tuple<double[], String>[] CharactersFV = 
            {
                #region From FONT
                #region Short characters
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.1481, 0.0185, 0.0247, 0.0864, 0.1111, 0.1111, 0.0000, 0.1111, 0.1420, 0.0864, 0.1605, 3.0000, 3.0000, 0.6027},
                    "A"),
                new Tuple<double[], String>(
                    new double[] {0.1324, 0.0594, 0.0822, 0.1324, 0.0594, 0.0913, 0.0822, 0.0000, 0.0822, 0.1324, 0.0594, 0.0868, 0.0000, 2.0000, 0.5417},
                    "B"),
                new Tuple<double[], String>(
                    new double[] {0.1418, 0.1045, 0.1269, 0.1343, 0.0000, 0.0000, 0.1343, 0.0000, 0.0000, 0.1418, 0.1045, 0.1119, 3.0000, 5.0000, 0.5833},
                    "C"),
                new Tuple<double[], String>(
                    new double[] {0.1510, 0.0677, 0.0938, 0.0938, 0.0000, 0.0938, 0.0938, 0.0000, 0.0938, 0.1510, 0.0677, 0.0938, 0.0000, 0.0000, 0.5417},
                    "D"),
                new Tuple<double[], String>(
                    new double[] {0.1559, 0.0699, 0.0753, 0.1559, 0.0699, 0.0753, 0.0968, 0.0000, 0.0000, 0.1559, 0.0699, 0.0753, 3.0000, 1.0000, 0.5556},
                    "E"),
                new Tuple<double[], String>(
                    new double[] {0.2071, 0.0929, 0.1000, 0.2071, 0.0929, 0.0357, 0.1286, 0.0000, 0.0000, 0.1357, 0.0000, 0.0000, 3.0000, 1.0000, 0.5479},
                    "F"),
                new Tuple<double[], String>(
                    new double[] {0.1235, 0.0802, 0.1111, 0.1111, 0.0000, 0.0000, 0.1111, 0.0062, 0.1358, 0.1173, 0.0802, 0.1235, 2.0000, 0.0000, 0.5417},
                    "G"),
                new Tuple<double[], String>(
                    new double[] {0.0984, 0.0000, 0.0984, 0.0984, 0.0000, 0.0984, 0.1585, 0.0710, 0.1585, 0.1093, 0.0000, 0.1093, 4.0000, 2.0000, 0.5270},
                    "H"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0000, 0.2432, 0.0000, 0.0000, 0.2432, 0.0000, 0.0000, 0.2432, 0.0000, 0.0000, 0.2703, 2.0000, 0.0000, 0.0135},
                    "I"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0000, 0.2021, 0.0000, 0.0000, 0.2021, 0.0000, 0.0000, 0.2021, 0.1277, 0.0957, 0.1702, 3.0000, 1.0000, 0.3766},
                    "J"),
                new Tuple<double[], String>(
                    new double[] {0.1076, 0.0380, 0.1076, 0.1835, 0.0570, 0.0000, 0.1772, 0.0506, 0.0000, 0.1203, 0.0506, 0.1076, 4.0000, 4.0000, 0.5190},
                    "K"),
                new Tuple<double[], String>(
                    new double[] {0.1743, 0.0000, 0.0000, 0.1743, 0.0000, 0.0000, 0.1743, 0.0000, 0.0000, 0.2569, 0.1009, 0.1193, 2.0000, 0.0000, 0.4605},
                    "L"),
                new Tuple<double[], String>(
                    new double[] {0.0874, 0.0000, 0.0874, 0.1505, 0.0000, 0.1505, 0.0922, 0.1311, 0.0971, 0.0971, 0.0097, 0.0971, 5.0000, 5.0000, 0.5270},
                    "M"),
                new Tuple<double[], String>(
                    new double[] {0.1122, 0.0000, 0.0918, 0.1173, 0.0663, 0.0918, 0.0918, 0.0663, 0.1173, 0.1020, 0.0000, 0.1429, 4.0000, 4.0000, 0.5270},
                    "N"),
                new Tuple<double[], String>(
                    new double[] {0.1092, 0.0747, 0.1092, 0.1034, 0.0000, 0.1034, 0.1034, 0.0000, 0.1034, 0.1092, 0.0747, 0.1092, 0.0000, 0.0000, 0.5417},
                    "O"),
                new Tuple<double[], String>(
                    new double[] {0.1790, 0.0802, 0.1235, 0.1111, 0.0123, 0.1296, 0.1790, 0.0679, 0.0000, 0.1173, 0.0000, 0.0000, 1.0000, 1.0000, 0.5342},
                    "P"),
                new Tuple<double[], String>(
                    new double[] {0.1550, 0.0700, 0.0950, 0.1550, 0.0700, 0.0850, 0.0950, 0.0750, 0.0200, 0.0800, 0.0000, 0.1000, 2.0000, 4.0000, 0.5526},
                    "R"),
                new Tuple<double[], String>(
                    new double[] {0.1325, 0.0861, 0.1258, 0.1325, 0.0596, 0.0000, 0.0000, 0.0265, 0.1391, 0.0795, 0.0861, 0.1325, 3.0000, 3.0000, 0.5556},
                    "S"),
                new Tuple<double[], String>(
                    new double[] {0.1171, 0.2613, 0.1261, 0.0000, 0.1622, 0.0000, 0.0000, 0.1622, 0.0000, 0.0000, 0.1712, 0.0000, 3.0000, 1.0000, 0.5479},
                    "T"),
                new Tuple<double[], String>(
                    new double[] {0.1125, 0.0000, 0.1125, 0.1125, 0.0000, 0.1125, 0.1125, 0.0000, 0.1125, 0.1188, 0.0813, 0.1250, 2.0000, 0.0000, 0.5342},
                    "U"),
                new Tuple<double[], String>(
                    new double[] {0.1159, 0.0000, 0.1449, 0.1377, 0.0000, 0.1377, 0.1377, 0.0870, 0.0507, 0.0072, 0.1812, 0.0000, 3.0000, 1.0000, 0.5263},
                    "V"),
                new Tuple<double[], String>(
                    new double[] {0.0881, 0.0000, 0.0933, 0.0933, 0.1192, 0.0933, 0.1244, 0.0415, 0.1399, 0.1036, 0.0000, 0.1036, 5.0000, 5.0000, 0.5135},
                    "W"),
                new Tuple<double[], String>(
                    new double[] {0.1111, 0.0000, 0.1438, 0.0327, 0.1895, 0.0131, 0.0392, 0.1765, 0.0196, 0.1373, 0.0000, 0.1373, 4.0000, 2.0000, 0.5625},
                    "X"),
                new Tuple<double[], String>(
                    new double[] {0.1429, 0.0000, 0.1964, 0.1071, 0.2054, 0.0000, 0.0000, 0.1696, 0.0000, 0.0000, 0.1786, 0.0000, 3.0000, 1.0000, 0.5195},
                    "Y"),
                new Tuple<double[], String>(
                    new double[] {0.0897, 0.0897, 0.1862, 0.0000, 0.0759, 0.0483, 0.0483, 0.0759, 0.0000, 0.2000, 0.0897, 0.0966, 3.0000, 3.0000, 0.5556},
                    "Z"),
                #endregion
                #region Wide characters
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.1637, 0.0000, 0.0468, 0.0877, 0.0760, 0.1053, 0.0000, 0.1053, 0.1579, 0.0994, 0.1579, 3.0000, 7.0000, 0.6892},
                    "A"),
                new Tuple<double[], String>(
                    new double[] {0.1293, 0.0647, 0.0776, 0.1293, 0.0647, 0.0862, 0.0733, 0.0000, 0.0733, 0.1422, 0.0647, 0.0948, 0.0000, 4.0000, 0.6338},
                    "B"),
                new Tuple<double[], String>(
                    new double[] {0.1286, 0.1071, 0.1214, 0.1214, 0.0000, 0.0000, 0.1214, 0.0000, 0.0000, 0.1357, 0.1071, 0.1571, 3.0000, 3.0000, 0.6620},
                    "C"),
                new Tuple<double[], String>(
                    new double[] {0.1523, 0.0761, 0.0863, 0.0863, 0.0000, 0.0863, 0.0863, 0.0000, 0.0863, 0.1675, 0.0761, 0.0964, 0.0000, 0.0000, 0.6338},
                    "D"),
                new Tuple<double[], String>(
                    new double[] {0.1472, 0.0711, 0.0812, 0.0863, 0.0000, 0.0000, 0.1472, 0.0711, 0.0812, 0.1624, 0.0711, 0.0812, 3.0000, 1.0000, 0.6197},
                    "E"),
                new Tuple<double[], String>(
                    new double[] {0.2000, 0.0966, 0.1103, 0.1172, 0.0000, 0.0000, 0.2000, 0.0966, 0.0414, 0.1379, 0.0000, 0.0000, 3.0000, 1.0000, 0.6197},
                    "F"),
                new Tuple<double[], String>(
                    new double[] {0.1198, 0.0898, 0.0958, 0.1018, 0.0000, 0.0000, 0.1018, 0.0180, 0.1317, 0.1257, 0.0898, 0.1257, 2.0000, 0.0000, 0.6338},
                    "G"),
                new Tuple<double[], String>(
                    new double[] {0.0929, 0.0000, 0.0929, 0.0929, 0.0000, 0.0929, 0.1639, 0.0820, 0.1639, 0.1093, 0.0000, 0.1093, 4.0000, 4.0000, 0.6338},
                    "H"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0000, 0.2394, 0.0000, 0.0000, 0.2394, 0.0000, 0.0000, 0.2394, 0.0000, 0.0000, 0.2817, 2.0000, 0.0000, 0.0141},
                    "I"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0000, 0.1910, 0.0000, 0.0000, 0.1910, 0.0000, 0.0000, 0.1910, 0.1348, 0.0899, 0.2022, 3.0000, 1.0000, 0.3571},
                    "J"),
                new Tuple<double[], String>(
                    new double[] {0.0943, 0.0440, 0.1069, 0.1887, 0.0566, 0.0000, 0.1887, 0.0440, 0.0000, 0.1132, 0.0503, 0.1132, 4.0000, 4.0000, 0.5949},
                    "K"),
                new Tuple<double[], String>(
                    new double[] {0.1651, 0.0000, 0.0000, 0.1651, 0.0000, 0.0000, 0.1651, 0.0000, 0.0000, 0.2569, 0.1193, 0.1284, 3.0000, 1.0000, 0.5405},
                    "L"),
                new Tuple<double[], String>(
                    new double[] {0.0887, 0.0000, 0.0788, 0.1724, 0.0049, 0.1724, 0.0887, 0.1281, 0.0887, 0.0887, 0.0000, 0.0887, 5.0000, 7.0000, 0.6250},
                    "M"),
                new Tuple<double[], String>(
                    new double[] {0.1371, 0.0000, 0.0863, 0.1168, 0.0660, 0.0914, 0.0914, 0.0660, 0.1168, 0.0914, 0.0000, 0.1371, 4.0000, 2.0000, 0.6250},
                    "N"),
                new Tuple<double[], String>(
                    new double[] {0.1080, 0.0852, 0.1023, 0.0966, 0.0000, 0.0966, 0.0966, 0.0000, 0.0966, 0.1136, 0.0852, 0.1193, 0.0000, 0.0000, 0.6429},
                    "O"),
                new Tuple<double[], String>(
                    new double[] {0.1765, 0.0882, 0.1235, 0.1000, 0.0000, 0.1176, 0.1765, 0.0882, 0.0118, 0.1176, 0.0000, 0.0000, 1.0000, 1.0000, 0.6338},
                    "P"),
                new Tuple<double[], String>(
                    new double[] {0.1490, 0.0721, 0.1058, 0.1490, 0.0673, 0.1010, 0.0865, 0.0625, 0.0240, 0.0817, 0.0000, 0.1010, 2.0000, 2.0000, 0.6216},
                    "R"),
                new Tuple<double[], String>(
                    new double[] {0.1333, 0.0909, 0.1212, 0.1152, 0.0000, 0.0000, 0.0182, 0.0909, 0.1212, 0.0788, 0.0909, 0.1394, 3.0000, 1.0000, 0.6338},
                    "S"),
                new Tuple<double[], String>(
                    new double[] {0.1204, 0.2593, 0.1204, 0.0000, 0.1574, 0.0000, 0.0000, 0.1574, 0.0000, 0.0000, 0.1852, 0.0000, 3.0000, 1.0000, 0.5493},
                    "T"),
                new Tuple<double[], String>(
                    new double[] {0.1076, 0.0000, 0.1076, 0.1076, 0.0000, 0.1076, 0.1076, 0.0000, 0.1076, 0.1266, 0.0949, 0.1329, 2.0000, 0.0000, 0.6429},
                    "U"),
                new Tuple<double[], String>(
                    new double[] {0.1159, 0.0000, 0.1377, 0.1304, 0.0000, 0.1304, 0.1304, 0.0507, 0.0797, 0.0145, 0.2101, 0.0000, 3.0000, 1.0000, 0.6351},
                    "V"),
                new Tuple<double[], String>(
                    new double[] {0.0928, 0.0000, 0.0825, 0.0928, 0.1082, 0.0928, 0.1443, 0.0670, 0.1289, 0.0928, 0.0000, 0.0979, 5.0000, 7.0000, 0.6164},
                    "W"),
                new Tuple<double[], String>(
                    new double[] {0.1032, 0.0000, 0.1419, 0.0323, 0.1742, 0.0258, 0.0129, 0.1935, 0.0065, 0.1613, 0.0000, 0.1484, 4.0000, 4.0000, 0.6582},
                    "X"),
                new Tuple<double[], String>(
                    new double[] {0.1441, 0.0000, 0.1982, 0.0991, 0.2162, 0.0000, 0.0000, 0.1712, 0.0000, 0.0000, 0.1712, 0.0000, 3.0000, 3.0000, 0.6053},
                    "Y"),
                new Tuple<double[], String>(
                    new double[] {0.0909, 0.0974, 0.1948, 0.0000, 0.0649, 0.0455, 0.0390, 0.0714, 0.0000, 0.2078, 0.0974, 0.0909, 4.0000, 4.0000, 0.6338},
                    "Z"),
                #endregion
                #region Font digits
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0778, 0.2444, 0.0667, 0.0111, 0.2000, 0.0000, 0.0000, 0.2000, 0.0000, 0.0000, 0.2000, 4.0000, 2.0000, 0.2778},
                    "1"),
                new Tuple<double[], String>(
                    new double[] {0.1203, 0.0902, 0.1504, 0.0000, 0.0000, 0.1278, 0.0000, 0.0827, 0.0451, 0.1579, 0.1278, 0.0977, 4.0000, 2.0000, 0.5352},
                    "2"),
                new Tuple<double[], String>(
                    new double[] {0.1280, 0.0960, 0.1600, 0.0000, 0.0000, 0.1360, 0.0000, 0.0000, 0.1600, 0.0480, 0.0960, 0.1760, 4.0000, 4.0000, 0.5429},
                    "3"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0272, 0.1769, 0.0272, 0.0884, 0.1156, 0.1701, 0.0748, 0.1837, 0.0000, 0.0000, 0.1361, 3.0000, 7.0000, 0.4789},
                    "4"),
                new Tuple<double[], String>(
                    new double[] {0.1399, 0.0839, 0.0979, 0.1259, 0.0839, 0.0140, 0.0000, 0.0000, 0.1329, 0.1049, 0.0839, 0.1329, 3.0000, 3.0000, 0.5135},
                    "5"),
                new Tuple<double[], String>(
                    new double[] {0.0000, 0.0556, 0.0903, 0.0417, 0.1736, 0.0139, 0.1250, 0.0000, 0.1250, 0.1528, 0.0833, 0.1389, 1.0000, 1.0000, 0.4865},
                    "6"),
                new Tuple<double[], String>(
                    new double[] {0.0748, 0.1121, 0.2710, 0.0000, 0.0561, 0.1121, 0.0000, 0.1682, 0.0000, 0.1776, 0.0280, 0.0000, 3.0000, 1.0000, 0.5000},
                    "7"),
                new Tuple<double[], String>(
                    new double[] {0.0994, 0.0608, 0.0994, 0.0994, 0.0608, 0.1050, 0.0939, 0.0000, 0.0939, 0.1105, 0.0608, 0.1160, 0.0000, 2.0000, 0.4857},
                    "8"),
                new Tuple<double[], String>(
                    new double[] {0.1096, 0.0822, 0.1507, 0.1233, 0.0000, 0.1233, 0.0205, 0.1370, 0.0822, 0.0959, 0.0753, 0.0000, 1.0000, 3.0000, 0.5067},
                    "9"),
                new Tuple<double[], String>(
                    new double[] {0.1071, 0.0655, 0.1131, 0.1012, 0.0000, 0.1012, 0.1012, 0.0000, 0.1012, 0.1190, 0.0655, 0.1250, 0.0000, 0.0000, 0.4857},
                    "0"),
                #endregion

                #region Added manually

                #endregion
                #endregion
            };

        #region Implementation details

        #region Character recognition helpers
        private static int CountBlackPixelsInRegion(ImageWrapper source, int xStart, int xEnd, int yStart, int yEnd)
        {
            int sum = 0;
            for (int y = yStart; y < yEnd; ++y)
                for (int x = xStart; x < xEnd; ++x)
                    if (source[x, y, 0] == 0) // if pixel is black;
                        ++sum;
            return sum;
        }

        private static double CountThreeWayPoints(ImageWrapper source)
        {
            double sum = 0;
            for (int y = 0; y < source.Height; ++y)
                for (int x = 0; x < source.Width; ++x)
                {
                    if (source[x, y, 0] == 0) // if pixel is black, look at neighbours count
                    {
                        int neighbours = 0;
                        for (int i = -1; i <= 1; ++i)
                            for (int j = -1; j <= 1; ++j)
                                if (!(i == 0 && j == 0) && source.IsInImageBounds(new CoordPoint(x + j, y + i)) && source[x + j, y + i, 0] == 0) // if neighbour is black
                                    ++neighbours;
                        if (neighbours == 3)
                        {
                            //source.SetRGB(x, y, 255, 0, 0);
                            ++sum;
                        }
                    }
                }

            return sum;
        }

        private static double CountEdgePoints(ImageWrapper source)
        {
            double sum = 0;
            for (int y = 0; y < source.Height; ++y)
                for (int x = 0; x < source.Width; ++x)
                {
                    if (source[x, y, 0] == 0) // if pixel is black, look at neighbours count
                    {
                        int neighbours = 0;
                        for (int i = -1; i <= 1; ++i)
                            for (int j = -1; j <= 1; ++j)
                                if (!(i == 0 && j == 0) && source.IsInImageBounds(new CoordPoint(x + j, y + i)) && source[x + j, y + i, 0] == 0) // if neighbour is black
                                    ++neighbours;
                        if (neighbours == 1)
                        {
                            //source.SetRGB(x, y, 0, 255, 0);
                            ++sum;
                        }
                    }
                }

            return sum;
        }
        #endregion

        private static List<Quadrangle> LocalizeCandidates(ImageWrapper source)
        {
            const double coeffX = 0.2;
            const double coeffLowY = 0.4;   // don't forget: Y goes from top to the bottom
            const double coeffHighY = 0.1;

            double objectMinX = coeffX * source.Width;
            double objectMaxX = (1.0 - coeffX) * source.Width;
            double objectMinY = coeffLowY * source.Height;
            double objectMaxY = (1.0 - coeffHighY) * source.Height;

            #region Debug informations
            System.Diagnostics.Debug.WriteLine("------------------------");
            System.Diagnostics.Debug.WriteLine("Analyzed image: ");
            System.Diagnostics.Debug.WriteLine("Width: " + source.Width);
            System.Diagnostics.Debug.WriteLine("Height: " + source.Height);
            System.Diagnostics.Debug.WriteLine("## Parameters ##");
            System.Diagnostics.Debug.WriteLine("Object X - min: " + objectMinX + ", max: " + objectMaxX);
            System.Diagnostics.Debug.WriteLine("Object Y - min: " + objectMinY + ", max: " + objectMaxY);

            #endregion


            List<List<CoordPoint>> objects = new FindObjects(FindObjects.ObjColor.White).GetImageObjects(source);
            List<Quadrangle> candidates = new List<Quadrangle>();

            #region For each object ~~
            foreach (var obj in objects)
            {
                if (obj.Count < 250)
                    continue;


                #region Find Top, Right, Bot, Left x,y coords
                int leftX = obj[0].X, rightX = obj[0].X;
                int botY = obj[0].Y, topY = obj[0].Y;

                foreach (CoordPoint cp in obj)
                {
                    if (leftX > cp.X)
                        leftX = cp.X;
                    else if (rightX < cp.X)
                        rightX = cp.X;

                    if (botY > cp.Y)
                        botY = cp.Y;
                    else if (topY < cp.Y)
                        topY = cp.Y;
                }
                #endregion

                // Aspect ratio
                int sizeX = Math.Abs(leftX - rightX);
                int sizeY = Math.Abs(topY - botY);

#if NOT_DELETE
#else
                if (sizeX <= 3.15 * sizeY || sizeX >= 5.25 * sizeY)
                {
                    System.Diagnostics.Debug.WriteLine("Object deleted - wrong ratio");
                    continue;
                }
#endif

                int midX = (leftX + rightX) / 2;
                int midY = (botY + topY) / 2;

                CoordPoint topLeft = obj[0], topRight = obj[0], botLeft = obj[0], botRight = obj[0];
                int topLeftR = 0, topRightR = 0, botLeftR = 0, botRightR = 0;

                #region Find Top-Left, Top-Right, Bot-Right, Bot-Left corners
                foreach (CoordPoint cp in obj)
                {
                    int r = Math.Abs(cp.X - midX) + Math.Abs(cp.Y - midY);

                    if (cp.X <= midX)       // LEFT
                    {
                        if (r > topLeftR && cp.Y <= midY)
                        {
                            topLeft = cp;
                            topLeftR = r;
                        }
                        else if (r > botLeftR && cp.Y >= midY)
                        {
                            botLeft = cp;
                            botLeftR = r;
                        }
                    }

                    else // RIGHT
                    {
                        if (r > topRightR && cp.Y <= midY)
                        {
                            topRight = cp;
                            topRightR = r;
                        }
                        else if (r > botRightR && cp.Y >= midY)
                        {
                            botRight = cp;
                            botRightR = r;
                        }
                    }
                }
                #endregion

#if NOT_DELETE
#else
                // Omitting object if contains two points with the same coordinates
                if (topLeft == topRight || topLeft == botLeft || topRight == botRight || botLeft == botRight ||

                // Omitting object if it isn't in the bottom middle of image
                    Math.Min(topLeft.X, botLeft.X) <= objectMinX ||
                    Math.Max(topRight.X, botRight.X) >= objectMaxX ||
                    Math.Min(botLeft.Y, botRight.Y) <= objectMinY ||    // don't forget: Y goes from top to the bottom
                    Math.Max(topLeft.Y, topRight.Y) >= objectMaxY)
                {
                    System.Diagnostics.Debug.WriteLine("Object deleted - not in middle of image or duplicated corner pixels");
                    continue;
                }
#endif


                candidates.Add(
                    new Quadrangle(
                        obj,
                        topLeft,
                        topRight,
                        botRight,
                        botLeft,
                        new CoordPoint(midX, midY)
                ));
#if DRAWING
                Drawing.DrawLine(source, topLeft, topRight, unchecked((int)0xFFFF0000));
                Drawing.DrawLine(source, topLeft, botLeft, unchecked((int)0xFFFF0000));
                Drawing.DrawLine(source, topRight, botRight, unchecked((int)0xFFFF0000));
                Drawing.DrawLine(source, botRight, botLeft, unchecked((int)0xFFFF0000));
                Drawing.DrawSquare(source, new CoordPoint(midX, midY), 1, unchecked((int)0xFF00FF00));
#endif
                #region DebugLogs
                System.Diagnostics.Debug.WriteLine("#### OBJECT FOUND ####");
                System.Diagnostics.Debug.WriteLine("sizeX=" + sizeX + ", sizeY=" + sizeY + ", ratio=" + (double)sizeX / sizeY);
                System.Diagnostics.Debug.WriteLine("midX=" + midX + ", midY=" + midY);
                System.Diagnostics.Debug.WriteLine("Corners:");
                System.Diagnostics.Debug.WriteLine("TopLeft x=" + topLeft.X + ", y=" + topLeft.Y);
                System.Diagnostics.Debug.WriteLine("TopRight x=" + topRight.X + ", y=" + topRight.Y);
                System.Diagnostics.Debug.WriteLine("BotLeft x=" + botLeft.X + ", y=" + botLeft.Y);
                System.Diagnostics.Debug.WriteLine("BotRight x=" + botRight.X + ", y=" + botRight.Y);
                #endregion
            }
            #endregion

            return candidates;
        }


        /// <summary>
        /// TODO: This should be much more complicated probably... but is good enough for the test cases.
        /// </summary>
        /// <param name="candidates"></param>
        /// <returns></returns>
        private static Quadrangle ChooseLP(List<Quadrangle> candidates)
        {
            int index = 0;
            CoordPoint midPoint = candidates[index].MidPoint;
            for (int i = 1; i < candidates.Count; ++i)
                if (midPoint.Y < candidates[i].MidPoint.Y)
                {
                    midPoint = candidates[i].MidPoint;
                    index = i;
                }

            return candidates[index];
        }

        private static Quadrangle GetObjectQuadrangle(List<CoordPoint> obj)
        {
            if (obj == null || obj.Count == 0)
                return null;

            int leftX = obj[0].X, rightX = obj[0].X;
            int botY = obj[0].Y, topY = obj[0].Y;

            foreach (CoordPoint cp in obj)
            {
                if (leftX > cp.X)
                    leftX = cp.X;
                else if (rightX < cp.X)
                    rightX = cp.X;

                if (botY > cp.Y)
                    botY = cp.Y;
                else if (topY < cp.Y)
                    topY = cp.Y;
            }

            int midX = (leftX + rightX) / 2;
            int midY = (botY + topY) / 2;

            CoordPoint topLeft = obj[0], topRight = obj[0], botLeft = obj[0], botRight = obj[0];
            int topLeftR = 0, topRightR = 0, botLeftR = 0, botRightR = 0;

            #region Find Top-Left, Top-Right, Bot-Right, Bot-Left corners
            foreach (CoordPoint cp in obj)
            {
                int r = Math.Abs(cp.X - midX) + Math.Abs(cp.Y - midY);

                if (cp.X <= midX)       // LEFT
                {
                    if (r > topLeftR && cp.Y <= midY)
                    {
                        topLeft = cp;
                        topLeftR = r;
                    }
                    else if (r > botLeftR && cp.Y >= midY)
                    {
                        botLeft = cp;
                        botLeftR = r;
                    }
                }

                if (cp.X >= midX) // RIGHT
                {
                    if (r > topRightR && cp.Y <= midY)
                    {
                        topRight = cp;
                        topRightR = r;
                    }
                    else if (r > botRightR && cp.Y >= midY)
                    {
                        botRight = cp;
                        botRightR = r;
                    }
                }
            }
            #endregion

            return new Quadrangle(
                obj,
                topLeft,
                topRight,
                botRight,
                botLeft,
                new CoordPoint(midX, midY)
            );
        }

        private static Edges GetObjectEdges(List<CoordPoint> obj)
        {
            if (obj == null || obj.Count == 0)
                return null;

            int leftX = obj[0].X, rightX = obj[0].X;
            int botY = obj[0].Y, topY = obj[0].Y;

            foreach (CoordPoint cp in obj)
            {
                if (leftX > cp.X)
                    leftX = cp.X;
                else if (rightX < cp.X)
                    rightX = cp.X;

                if (botY > cp.Y)
                    botY = cp.Y;
                else if (topY < cp.Y)
                    topY = cp.Y;
            }
            return new Edges(
                obj,
                topY,
                botY,
                leftX,
                rightX
            );
        }

        private static ImageWrapper CutTopBot(ImageWrapper source)
        {
            int top = -1, bot = 0, index = 0;
            for (int y = 0; y < source.Height; ++y)
                for (int x = 0; x < source.Width; ++x)
                {
                    if (source.ByteArray[index] == 0) // if pixel is black
                    {
                        if (top == -1)
                            top = y;
                        bot = y;
                    }
                    index += 4; // goto next pixel
                }

            return source.Crop(new CoordPoint(0, top), new CoordPoint(source.Width - 1, bot));
        }
        #endregion
    }
}
