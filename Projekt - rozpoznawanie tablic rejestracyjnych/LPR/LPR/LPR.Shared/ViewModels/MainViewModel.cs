﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ImagingLib;
using ImagingLib.ImageEffects.EdgeDetection;
using ImagingLib.ImageEffects.Morphology;
using ImagingLib.ImageEffects.Other;
using ImagingLib.ImageEffects.Thinning;
using ImagingLib.ImageEffects.Threshold;
using ImagingLib.ImageUtils;
using LPR.Models;
using Windows.UI.Xaml.Media.Imaging;

namespace LPR.ViewModels
{
    public class MainViewModel : PropertyChangedBase
    {
        public MainViewModel()
        {
            Effects = new ObservableCollection<IImageEffect>();
            ExtractedCharacters = new ObservableCollection<WriteableBitmap>();

            Effects.Add(new Dilation());
            Effects.Add(new Erosion());
            Effects.Add(new GlobalThreshold());
            Effects.Add(new OtsuThreshold());
            Effects.Add(new LocalThreshold());
            Effects.Add(new Normalization());
            Effects.Add(new GreyScale());
            Effects.Add(new SobelFilter());
            Effects.Add(new Canny(30, 60));
            Effects.Add(new K3M(Def.BLACK, Def.WHITE));
            Effects.Add(new KMM(Def.BLACK, Def.WHITE));
            Effects.Add(new Negative());
            Effects.Add(new AfterK3MThinning(FindObjects.ObjColor.Black));
        }

        #region UI Buttons
        public async Task LoadImage()
        {
            ImageWrapper pickedImage = await ImageWrapperFactory.CreateWithFOP();

            if (pickedImage == null)
                return;

            CurrentImage = pickedImage;
            OriginalImage = new ImageWrapper(CurrentImage);
            Bitmap = await CurrentImage.ToWriteableBitmap();
            LPR_Result = null;
        }

        public async Task LoadPlate()
        {
            ImageWrapper pickedImage = await ImageWrapperFactory.CreateWithFOP();

            if (pickedImage == null)
                return;

            LicensePlateImage = pickedImage;
            LPR_Result = await LicensePlateImage.ToWriteableBitmap();
        }


        public async Task Process()
        {
            LicensePlateImage = LPR_system.Localize(CurrentImage);
            if (LicensePlateImage == null)
                return;
            LPR_Result = await LicensePlateImage.ToWriteableBitmap();
        }

        public async Task ProcessLP()
        {
            if (LicensePlateImage == null)
                return;
            LicensePlateImage = LPR_system.PreprocessLicensePlate(LicensePlateImage);
            if (LicensePlateImage == null)
                return;
            LPR_Result = await LicensePlateImage.ToWriteableBitmap();

            var chars = LPR_system.ExtractCharacters(LicensePlateImage);
            ExtractedCharacters.Clear();


            StringBuilder strB = new StringBuilder();
            for(int i=0; i<chars.Count; ++i)
            {
                strB.Append(LPR_system.RecognizeCharacter(chars[i], i));
                Task<WriteableBitmap> t = chars[i].ToWriteableBitmap();
                t.Wait();
                WriteableBitmap wb = t.Result;
                ExtractedCharacters.Add(wb);
            }
            Debug.WriteLine("FOUND: " + strB.ToString());
            
                
            
        }

        public async Task Apply()
        {
            if (SelectedEffect == null || CurrentImage == null)
                return;

            CurrentImage = SelectedEffect.ApplyEffect(CurrentImage);
            Bitmap = await CurrentImage.ToWriteableBitmap();
        }

        public async Task ApplyLP()
        {
            if (LicensePlateImage == null)
                return;

            LicensePlateImage = SelectedEffect.ApplyEffect(LicensePlateImage);
            LPR_Result = await LicensePlateImage.ToWriteableBitmap();
        }

        public async Task Reset()
        {
            if (OriginalImage == null)
                return;
            Bitmap = await OriginalImage.ToWriteableBitmap();
            CurrentImage = new ImageWrapper(OriginalImage);
        }
        #endregion

        #region Fields
        private ImageWrapper OriginalImage;

        private ImageWrapper CurrentImage;

        private ImageWrapper LicensePlateImage;
        #endregion

        #region Properties
        private WriteableBitmap _Bitmap;
        private WriteableBitmap _LPR_Result;
        private IImageEffect _SelectedEffect;
        
        public ObservableCollection<IImageEffect> Effects { get; set; }
        public ObservableCollection<WriteableBitmap> ExtractedCharacters { get; set; } 
        
        public WriteableBitmap Bitmap
        {
            get { return _Bitmap; }
            set
            {
                _Bitmap = value;
                NotifyOfPropertyChange(() => Bitmap);
            }
        }
        public WriteableBitmap LPR_Result
        {
            get { return _LPR_Result; }
            set
            {
                _LPR_Result = value;
                NotifyOfPropertyChange(() => LPR_Result);
            }
        }

        public IImageEffect SelectedEffect
        {
            get { return _SelectedEffect; }
            set
            {
                _SelectedEffect = value;
                NotifyOfPropertyChange(() => SelectedEffect);
            }
        }
        #endregion
    }
}
