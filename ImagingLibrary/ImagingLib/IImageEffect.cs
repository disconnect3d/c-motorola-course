﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib
{
    public interface IImageEffect
    {
        String Name { get; }
        String Description { get; }
        Task<WriteableBitmap> ApplyEffect(WriteableBitmap source);
    }
}
