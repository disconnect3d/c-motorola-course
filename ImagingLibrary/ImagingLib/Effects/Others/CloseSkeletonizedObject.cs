﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public enum Connect { ClosestPoints, FurthestPoints };
    public class CloseSkeletonizedObject : FindObjectsBase, IImageEffect
    {

        public Connect ConnectMethod { get; set; }

        public CloseSkeletonizedObject(Connect connectMethod, int objectColor, int bgColor)
            : base(objectColor, bgColor)
        {
            this.ConnectMethod = connectMethod;
        }

        public string Name
        {
            get { return "Close Skeletonized Object"; }
        }

        public string Description
        {
            get { return "Closes skeletonized object"; }
        }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            using (var context = source.GetBitmapContext())
            {
                var biggestObject = GetImageObjects(context).OrderByDescending((x) => x.Count).First();

                List<CoordPoint> endPoints = GetEndingPoints(biggestObject, context);

                if (endPoints.Count < 2)
                    throw new ArgumentException("Biggest object on passed image does not have at least 2 end points");

                CoordPoint p1 = endPoints[0], p2 = endPoints[1];
                double findDist = Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));

                if (ConnectMethod == Connect.ClosestPoints)
                {
                    foreach (CoordPoint cp1 in endPoints)
                        foreach (CoordPoint cp2 in endPoints)
                        {
                            if (cp1.Equals(cp2))
                                continue;

                            double distance = Math.Sqrt(Math.Pow(cp1.X - cp2.X, 2) + Math.Pow(cp1.Y - cp2.Y, 2));
                            if (distance < findDist)
                            {
                                findDist = distance;
                                p1 = cp1;
                                p2 = cp2;
                            }
                        }
                }
                else if (ConnectMethod == Connect.FurthestPoints)
                {
                    foreach (CoordPoint cp1 in endPoints)
                        foreach (CoordPoint cp2 in endPoints)
                        {
                            if (cp1.Equals(cp2))
                                continue;

                            double distance = Math.Sqrt(Math.Pow(cp1.X - cp2.X, 2) + Math.Pow(cp1.Y - cp2.Y, 2));
                            if (distance > findDist)
                            {
                                findDist = distance;
                                p1 = cp1;
                                p2 = cp2;
                            }
                        }
                }
                else
                    throw new NotImplementedException("Such ConnectMethod is not implemented.");

                DrawingUtils.DrawLine(context, p1, p2, ObjectColor);
            }

            return source;
        }
    }
}
