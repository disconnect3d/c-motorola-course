﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    

    public class DeleteObjects : FindObjectsBase, IImageEffect
    {
        public int ConditionPixels { get; set; }
        private readonly Delete Condition;

        public enum Delete { LessThanPixels, MoreThanPixels, LowestObject, BiggestObject };

        /// <summary>
        /// Delete objects specified with the condition
        /// </summary>
        /// <param name="condition">If LessThanPixels or MoreThanPixels is choosen, delete multiple objects. Otherwise delete biggest/smallest object.</param>
        /// <param name="conditionPixels">This is omitted for Delete Condition == LowestObject/BiggestObject</param>
        /// <param name="objectColor">ARGB color</param>
        /// <param name="bgColor">ARGB color</param>
        public DeleteObjects(Delete condition, int conditionPixels, int objectColor, int bgColor)
            : base(objectColor, bgColor)
        {
            this.Condition = condition;
            this.ConditionPixels = conditionPixels;
        }

        public string Name { get { return "Delete Lines"; } }

        public string Description { get { return "Delete lines having less pixels than the specified ammount."; } }

        public unsafe async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            WriteableBitmap copy = source.DeepCopy();
            List<List<CoordPoint>> imageObjects = new List<List<CoordPoint>>();

            // Searching objects on the image
            using (var context = copy.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int index = 0;
                fixed (int* pixels = context.Pixels)
                    for (int y = 0; y < context.Height; ++y)
                        for (int x = 0; x < context.Width; ++x)
                            if (pixels[index++] == ObjectColor)
                                imageObjects.Add(GetImageObject(x, y, context));
            }

            // Returning if no objects
            if (imageObjects.Count == 0)
                return source;

            using (var context = source.GetBitmapContext())
            {
                fixed (int* pixels = context.Pixels)
                {
                    if (Condition == Delete.LessThanPixels)
                    {
                        foreach (List<CoordPoint> lp in imageObjects)
                            if (lp.Count < ConditionPixels)
                                foreach (CoordPoint cp in lp)
                                    pixels[source.IntArrIndex(cp.X, cp.Y)] = BackgroundColor;
                    }
                    else if (Condition == Delete.MoreThanPixels)
                    {
                        foreach (List<CoordPoint> lp in imageObjects)
                            if (lp.Count > ConditionPixels)
                                foreach (CoordPoint cp in lp)
                                    pixels[source.IntArrIndex(cp.X, cp.Y)] = BackgroundColor;
                    }
                    else if (Condition == Delete.LowestObject)
                    {
                        foreach (CoordPoint cp in imageObjects.OrderBy((x) => x.Count).First())
                            pixels[source.IntArrIndex(cp.X, cp.Y)] = BackgroundColor;
                    }
                    else if (Condition == Delete.BiggestObject)
                    {
                        foreach (CoordPoint cp in imageObjects.OrderBy((x) => x.Count).Last())
                            pixels[source.IntArrIndex(cp.X, cp.Y)] = BackgroundColor;
                    }
                    else
                        throw new ArgumentOutOfRangeException("Unknown delete condition");
                }
            }
            imageObjects.Clear();


            return source;
        }
    }
}
