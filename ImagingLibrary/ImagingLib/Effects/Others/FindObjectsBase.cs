﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public struct CoordPoint
    {
        public CoordPoint(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public int X;
        public int Y;
    }

    public abstract class FindObjectsBase
    {
        public FindObjectsBase(int objectColor, int bgColor)
        {
            this.ObjectColor = objectColor;
            this.BackgroundColor = bgColor;

            this.HelperColor = rng.Next();
            while (this.HelperColor == ObjectColor || this.HelperColor == BackgroundColor)
                this.HelperColor = rng.Next();
        }

        public int ObjectColor { get; set; }
        public int BackgroundColor { get; set; }

        protected int HelperColor;

        protected List<CoordPoint> GetImageObject(int x, int y, BitmapContext context)
        {
            List<CoordPoint> result = new List<CoordPoint>();

            List<CoordPoint> helper = new List<CoordPoint>();

            int[] pixels = context.Pixels;
            int index = context.IntArrIndex(x, y);
            pixels[index] = HelperColor;

            CoordPoint firstPoint = new CoordPoint(x, y);
            helper.Add(firstPoint);
            result.Add(firstPoint);

            while (helper.Count != 0)
            {
                CoordPoint popped = helper.ElementAt(0);
                helper.RemoveAt(0);

                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if ((i == 0 && j == 0) || !context.IsInImageBounds(popped.X + j, popped.Y + i))
                            continue;

                        index = context.IntArrIndex(popped.X + j, popped.Y + i);
                        if (pixels[index] == ObjectColor)
                        {
                            pixels[index] = HelperColor;
                            CoordPoint add = new CoordPoint(popped.X + j, popped.Y + i);
                            helper.Add(add);
                            result.Add(add);
                        }
                    }
            }

            return result;
        }

        protected List<List<CoordPoint>> GetImageObjects(BitmapContext context, bool returnInObjectColor = true)
        {
            List<List<CoordPoint>> result = new List<List<CoordPoint>>();
            for (int y = 0; y < context.Height; ++y)
                for (int x = 0; x < context.Width; ++x)
                    if (context.Pixels[context.IntArrIndex(x, y)] == ObjectColor)
                        result.Add(GetImageObject(x, y, context));

            if (returnInObjectColor)
                foreach (List<CoordPoint> objects in result)
                    foreach (CoordPoint cp in objects)
                        context.Pixels[context.IntArrIndex(cp.X, cp.Y)] = ObjectColor;

            return result;
        }

        protected List<CoordPoint> GetEndingPoints(List<CoordPoint> obj, BitmapContext context)
        {
            List<CoordPoint> endingPoints = new List<CoordPoint>();
            foreach (CoordPoint cp in obj)
            {
                int neighbours = 0;
                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if (i == 0 && j == 0)
                            continue;

                        int x = cp.X + j;
                        int y = cp.Y + i;

                        if (!context.IsInImageBounds(cp.X + j, cp.Y + i))
                            continue;

                        if (context.Pixels[context.IntArrIndex(x, y)] == ObjectColor)
                            ++neighbours;
                    }
                if (neighbours == 1)
                    endingPoints.Add(cp);
            }

            return endingPoints;
        }

        // TODO: for testing purposes, this should be injected through the constructor or taken from some kind of container/factory/internal utility class
        protected readonly Random rng = new Random();
    }
}
