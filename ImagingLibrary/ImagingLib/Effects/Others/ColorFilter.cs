﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Others
{
    public class ColorFilter : IImageEffect
    {
        public ColorFilter(ColorMask mask)
        {
            this.Mask = mask;
        }

        public string Name
        {
            get { return "Change colors"; }
        }

        public string Description
        {
            get { return "Changes colors using specified mask"; }
        }

        /// <summary>
        /// Applies ChangeColors effect into specified image
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>WriteableBitmap containing image with applied effect</returns>
        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            using (var context = source.GetBitmapContext())
            {
                unsafe 
                {
                    fixed(int* Pixels = context.Pixels)
                    {
                        int* lastPixel = Pixels + context.Length;
                        int* currentPixel = Pixels;
                        while(currentPixel != lastPixel)
                        {
                            byte* R = ((byte*)currentPixel) + Def.R_Offset;
                            byte* G = ((byte*)currentPixel) + Def.G_Offset;
                            byte* B = ((byte*)currentPixel) + Def.B_Offset;

                            Mask.setter(ref *R, ref *G, ref *B);

                            ++currentPixel;
                        }
                    }
                }
            }
            return source;
        }

        public ColorMask Mask { get; set; }
    }
}
