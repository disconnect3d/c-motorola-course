﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Thresholds
{
    public class Otsu : IImageEffect
    {
        public string Name { get { return "Otsu threshold"; } }
        public string Description { get { return "TODO"; } }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            int[] histogram = source.HistogramRed();
            double SU = 0;
            for (int i = 0; i < 256; ++i)
                SU += i * histogram[i];
            double W = 0;
            double MAX = 0;
            double SUP = 0;
            double LP = source.PixelCount();
            double WP = 0;
            double SG, SD, R;
            double threshold1 = 0, threshold2 = 0;
            for (int i = 0; i < 256; ++i)
            {
                W += histogram[i];
                if (W == 0)
                    continue;
                WP = LP - W;
                if (WP == 0)
                    break;
                SUP += i * histogram[i];
                SG = SUP / W;
                SD = (SU - SUP) / WP;
                R = W * WP * (SG - SD) * (SG - SD);
                if (R >= MAX)
                {
                    threshold1 = i;
                    if (R > MAX)
                        threshold2 = i;
                    MAX = R;
                }
            }

            int threshold = (int)(threshold1 + threshold2) / 2;

            unsafe
            {
                using (BitmapContext context = source.GetBitmapContext())
                    fixed (int* pixels = context.Pixels)
                        for (int i = 0; i < LP; ++i)
                            pixels[i] = (pixels[i] & Def.MASK_B) < threshold ? Def.BLACK : Def.WHITE;
            }
            return source;
        }
    }
}
