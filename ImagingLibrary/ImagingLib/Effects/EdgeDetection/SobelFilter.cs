﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using ImagingLib;

namespace ImagingLib.Effects.EdgeDetection
{
    public class SobelsFilter : IImageEffect
    {
        public string Name { get { return "Sobel filter"; } }

        public string Description { get { return ""; } }

        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            const int channels = 4;
            byte[] copy = source.ToByteArray();
            byte[] result = source.ToByteArray();

            // p0, p1, p2, p3, p4, p5, p6, p7, px stands for the indexes of pixel and its neighbors in such a grid:
            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4

            // creating most of the image

            // Pinning the ByteArray memory, so GC won't move it during work
            // Using ptr[] instead of ByteArray[] is faster (same for resultPtr[] instead of result[])
            // because OutOfBoundsArray checks aren't made
            unsafe
            {
                fixed (Byte* ptr = copy, resultPtr = result)
                {
                    for (int y = 1; y < source.PixelHeight - 1; ++y)
                    {
                        for (int x = 1; x < source.PixelWidth - 1; ++x)
                        {
                            for (int i = 0; i < 3; ++i)
                            {
                                // px = (x, y)
                                int px = source.ByteArrIndex(x, y, i);

                                int moveBottom = source.PixelWidth * channels;
                                // p0 = (x-1, y-1)
                                int p0 = px - channels - moveBottom;
                                // p1 = (x, y-1)
                                int p1 = px - moveBottom;
                                // p2 = (x+1, y-1)
                                int p2 = px + channels - moveBottom;
                                // p3 = (x+1, y)
                                int p3 = px + channels;
                                // p4 = (x+1, y+1)
                                int p4 = px + channels + moveBottom;
                                // p5 = (x, y+1)
                                int p5 = px + moveBottom;
                                // p6 = (x-1, y+1)
                                int p6 = px - channels + moveBottom;
                                // p7 = (x-1, y)
                                int p7 = px - channels;

                                // tmp1 = (p2 + 2*p3 + p4) - (p0 + 2*p7 + p6)
                                int tmp1 = (ptr[p2] + (2 * ptr[p3]) + ptr[p4]) - (ptr[p0] + (2 * ptr[p7]) + ptr[p6]);
                                // tmp2 =  (p0 + 2*p1 + p2) - (p6 + 2*p5 + p4)
                                int tmp2 = (ptr[p0] + (2 * ptr[p1]) + ptr[p2]) - (ptr[p6] + (2 * ptr[p5]) + ptr[p4]);

                                // (x,y) = sqrt(tmp1^2 + tmp2^2)
                                resultPtr[px] = ((int)Math.Sqrt((Math.Pow(tmp1, 2) + Math.Pow(tmp2, 2)))).CutToByte();
                            }
                        }
                    }
                }
            }
            return source.FromByteArray(result);
        }

    }
}
