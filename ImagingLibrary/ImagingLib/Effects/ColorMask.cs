﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib.Effects
{
    public class ColorMask
    {
        #region NOT IMPLEMENTED/NOT SUPPORTED // OLD CODE
        ///// <summary>
        ///// Creates mask setting CalculateColor with the input function.
        ///// </summary>
        ///// <param name="calcIntColor">Function that int RGB color will be evaluated with</param>
        //public ColorMask(Func<byte, byte, byte, int> calcIntColor)
        //{
        //    CalculateColor = calcIntColor;
        //}


        ///// <summary>
        ///// Used to calculate RGB color using R, G, B arguments
        ///// </summary>
        //public Func<byte, byte, byte, int> CalculateColor { get; set; }

        ///// <summary>
        ///// Creates mask setting CalculateRed, CalculateBlue and CalculateGreen with the input function.
        ///// </summary>
        ///// <param name="calcEachColor">Function that every color channel will be evaluated with</param>
        //public ColorMask(Func<byte, byte, byte, byte> calcEachColor)
        //{
        //    CalculateRed = CalculateBlue = CalculateGreen = calcEachColor;
        //}

        ///// <summary>
        ///// Calculates 
        ///// </summary>
        ///// <param name="calcRed">Red channel evaluation function</param>
        ///// <param name="calcGreen">Green channel evaluation function</param>
        ///// <param name="calcBlue">Blue channel evaluation function</param>
        //public ColorMask(Func<byte, byte, byte, byte> calcRed,
        //                 Func<byte, byte, byte, byte> calcGreen,
        //                 Func<byte, byte, byte, byte> calcBlue)
        //{
        //    this.CalculateRed = calcRed;
        //    this.CalculateGreen = calcGreen;
        //    this.CalculateBlue = calcBlue;
        //}

        ///// <summary>
        ///// Used to calculate Blue channel using R, G, B arguments
        ///// </summary>
        //public Func<byte, byte, byte, byte> CalculateBlue { get; set; }

        ///// <summary>
        ///// Used to calculate Green channel using R, G, B arguments
        ///// </summary>
        //public Func<byte, byte, byte, byte> CalculateGreen { get; set; }

        ///// <summary>
        ///// Used to calculate Red channel using R, G, B arguments
        ///// </summary>
        //public Func<byte, byte, byte, byte> CalculateRed { get; set; }



        //#region Predefined masks
        ///// <summary>
        ///// Standard grayscale counted using
        ///// Formula: (R + G + B) / 3.0F
        ///// </summary>
        //public static readonly ColorMask StandardGrayscale =
        //    new ColorMask((R, G, B) => (byte)((R + G + B) / 3.0F));

        ///// <summary>
        ///// Grayscale more natural for humans
        ///// Formula: 0.3F * R + 0.59F * G + 0.11F * B
        ///// </summary>
        //public static readonly ColorMask HumanGrayscale =
        //    new ColorMask((R, G, B) => (byte)(0.3F * R + 0.59F * G + 0.11F * B));

        ///// <summary>
        ///// <para> Grayscale used for skin detection </para>
        ///// 
        ///// <para> Formula: R - G </para>
        ///// </summary>
        //public static readonly ColorMask SkinDetectionGrayscale =
        //    new ColorMask(new Func<byte, byte, byte, byte>((R, G, B) => ((int)R - (int)G).CutToByte()));
        #endregion

        public ColorMask(RGBsetter setter)
        {
            this.setter = setter;
        }
        public RGBsetter setter { get; set; }

        public delegate void RGBsetter(ref byte R, ref byte G, ref byte B);

        /// <summary>
        /// Negative
        /// <para> </para>
        /// Formula:
        /// <para> </para>
        /// R = 255 - R
        /// <para> </para>
        /// G = 255 - G
        /// <para> </para>
        /// B = 255 - B
        /// </summary>
        public static readonly ColorMask Negative = new ColorMask(new RGBsetter(Predefined_Negative));

        /// <summary>
        /// Standard grayscale
        /// <para> </para>
        /// Formula: R = G = B = (R + G + B) / 3
        /// </summary>
        public static readonly ColorMask StandardGrayscale = new ColorMask(new RGBsetter(Predefined_StandardGrayscale));

        /// <summary>
        /// Grayscale more natural for humans
        /// <para> </para>
        /// Formula: R = G = B = 0.3F * R + 0.59F * G + 0.11F * B
        /// </summary>
        public static readonly ColorMask HumanGrayscale = new ColorMask(new RGBsetter(Predefined_HumanGrayscale));

        /// <summary>
        /// Grayscale used for skin detection
        /// <para> </para>
        /// Formula: R = G = B = R - G
        /// </summary>
        public static readonly ColorMask SkinDetectionGrayscale = new ColorMask(new RGBsetter(Predefined_SkinDetectionGrayscale));

        #region Predefined masks code behind
        private static void Predefined_Negative(ref byte R, ref byte G, ref byte B)
        {
            R = (byte)(255 - R);
            G = (byte)(255 - G);
            B = (byte)(255 - B);
        }

        private static void Predefined_StandardGrayscale(ref byte R, ref byte G, ref byte B)
        {
            R = G = B = (byte)((R + G + B) / 3);
        }

        private static void Predefined_HumanGrayscale(ref byte R, ref byte G, ref byte B)
        {
            R = G = B = (byte)(0.3F * R + 0.59F * G + 0.11F * B);
        }

        private static void Predefined_SkinDetectionGrayscale(ref byte R, ref byte G, ref byte B)
        {
            R = G = B = ((int)R - (int)G).CutToByte();
        }
        #endregion
    }
}
