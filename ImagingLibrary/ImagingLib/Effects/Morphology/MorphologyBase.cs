﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace ImagingLib.Effects.Morphology
{
    public class MorphologyBase
    {
        protected static readonly bool[,] Mask = new bool[3, 3]
        {
            {true,  true,    true},
            {true,  false,   true},
            {true,  true,    true},
        };

        protected const int MASK_LEN = 1;
    }
}

