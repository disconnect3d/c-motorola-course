﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using System.IO;

namespace ImagingLib.Effects.Morphology
{
    /// <summary>
    /// Process of thinning black parts on the image.
    /// Works on grayscale images (it doesn't check if image is in grayscale)
    /// </summary>
    public class Erosion : MorphologyBase, IImageEffect
    {
        public string Name { get { return "Erosion"; } }

        public string Description { get { return "Process of thinning black parts on the image"; } }

        /// <summary>
        /// Applies erosion to input image.
        /// This effect works on copy image.
        /// </summary>
        /// <param name="source">Image to apply filter to</param>
        /// <returns>Copied WriteableBitmap with applied effect.</returns>
        public async Task<WriteableBitmap> ApplyEffect(WriteableBitmap source)
        {
            WriteableBitmap result = await source.DeepCopyAsync();

            using (BitmapContext resultContext = result.GetBitmapContext(), sourceContext = source.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] resultPixels = resultContext.Pixels;      // ARGB
                int[] sourcePixels = sourceContext.Pixels;      // ARGB

                for (int h = MASK_LEN; h < sourceContext.Height - MASK_LEN; ++h)
                    for (int w = MASK_LEN; w < sourceContext.Width - MASK_LEN; ++w)
                    {
                        for (int y = -MASK_LEN; y <= MASK_LEN; ++y)
                            for (int x = -MASK_LEN; x <= MASK_LEN; ++x)
                            {
                                int sourcePix = sourcePixels[(w + x) + (h + y) * sourceContext.Width];
                                if (Mask[x + 1, y + 1] && (sourcePix & Def.MASK_RGB) == Def.MASK_RGB)
                                {
                                    resultPixels[w + h * sourceContext.Width] = Def.WHITE;
                                    goto EndWorkOnThisPixel;
                                }
                            }
                    EndWorkOnThisPixel:
                        continue;
                    }
            }
            return result;
        }
    }
}
