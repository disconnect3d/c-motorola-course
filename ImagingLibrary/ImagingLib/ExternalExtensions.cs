﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;

namespace ImagingLib
{
    public static class ExternalExtensions
    {
        /// <summary>
        ///  Asynchronously creates deep copy of WriteableBitmap
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>Task which copies WriteableBitmap</returns>
        public static async Task<WriteableBitmap> DeepCopyAsync(this WriteableBitmap source)
        {
            WriteableBitmap copy = BitmapFactory.New(source.PixelWidth, source.PixelHeight);
            using (Stream sourceStream = source.PixelBuffer.AsStream(), copyStream = copy.PixelBuffer.AsStream())
                await sourceStream.CopyToAsync(copyStream);
            return copy;
        }

        /// <summary>
        ///  Synchronously creates deep copy of WriteableBitmap
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>Copied WriteableBitmap</returns>
        public static WriteableBitmap DeepCopy(this WriteableBitmap source)
        {
            WriteableBitmap copy = BitmapFactory.New(source.PixelWidth, source.PixelHeight);
            using (Stream sourceStream = source.PixelBuffer.AsStream(), copyStream = copy.PixelBuffer.AsStream())
                sourceStream.CopyTo(copyStream);
            return copy;
        }

        /// <summary>
        /// Gets a 32-bit integer that represents the total number of pixels in WriteableBitmap.
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>Total number of pixels in WriteableBitmap (Width * Height)</returns>
        public static int PixelCount(this WriteableBitmap source)
        {
            return source.PixelHeight * source.PixelWidth;
        }

        private const int ALPHA_INDEX = 0;
        private const int RED_INDEX = 1;
        private const int GREEN_INDEX = 2;
        private const int BLUE_INDEX = 3;

        /// <summary>
        /// Gets a System.Array of 256 32-bit integers that represent blue histogram data of specified WriteableBitmap.
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>256 integers containing blue histogram data (quantity of pixels having specified blue channel)/returns>
        public static int[] HistogramRed(this WriteableBitmap source)
        {
            byte[] channelArray = source.ToByteArray().Where((item, index) => index % 4 == RED_INDEX).ToArray();

            int[] data = new int[256];
            for (int i = 0; i <= 255; ++i)
                data[i] = channelArray.Count(channel => channel == i);

            return data;
        }

        /// <summary>
        /// Gets a System.Array of 256 32-bit integers that represent blue histogram data of specified WriteableBitmap.
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>256 integers containing blue histogram data (quantity of pixels having specified blue channel)/returns>
        public static int[] HistogramGreen(this WriteableBitmap source)
        {
            byte[] channelArray = source.ToByteArray().Where((item, index) => index % 4 == GREEN_INDEX).ToArray();

            int[] data = new int[256];
            for (int i = 0; i <= 255; ++i)
                data[i] = channelArray.Count(channel => channel == i);

            return data;
        }

        /// <summary>
        /// Gets a System.Array of 256 32-bit integers that represent blue histogram data of specified WriteableBitmap.
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <returns>256 integers containing blue histogram data (quantity of pixels having specified blue channel)/returns>
        public static int[] HistogramBlue(this WriteableBitmap source)
        {
            byte[] channelArray = source.ToByteArray().Where((item, index) => index % 4 == BLUE_INDEX).ToArray();

            int[] data = new int[256];
            for (int i = 0; i <= 255; ++i)
                data[i] = channelArray.Count(channel => channel == i);

            return data;
        }




        public static int IntArrIndex(this WriteableBitmap source, int x, int y)
        {
            return source.PixelWidth * y + x;
        }

        public static int IntArrIndex(this BitmapContext source, int x, int y)
        {
            return source.Width * y + x;
        }

        public static bool IsInImageBounds(this WriteableBitmap source, int x, int y)
        {
            return x >= 0 && x < source.PixelWidth && y >= 0 && y < source.PixelHeight;
        }

        public static bool IsInImageBounds(this BitmapContext context, int x, int y)
        {
            return x >= 0 && x < context.Width && y >= 0 && y < context.Height;
        }
    }
}
