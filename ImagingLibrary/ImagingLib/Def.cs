﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagingLib
{
    public static class Def
    {
        public const int WHITE = unchecked((int)0xFFFFFFFF);
        public const int BLACK = unchecked((int)0xFF000000);
        public const int RED = unchecked((int)0xFFFF0000);
        public const int GREEN = unchecked((int)0xFF00FF00);
        public const int BLUE = unchecked((int)0xFF0000FF);

        public const int MASK_BLACK = 0x00000000;

        public const int MASK_ARGB = unchecked((int)0xFFFFFFFF);

        public static readonly int[] BLACK_N_WHITE = { Def.BLACK, Def.WHITE };
        
        /// <summary>
        /// 0x00 FF FF FF, also white color with 0 alpha channel
        /// </summary>
        public const int MASK_RGB = 0x00FFFFFF;    // A R G B
        public const int MASK_R = 0x00FF0000;
        public const int MASK_G = 0x0000FF00;
        public const int MASK_B = 0x000000FF;
        public const int MASK_A = unchecked((int)0xFF000000);



        internal const int A_Offset = 3;
        internal const int R_Offset = 2;
        internal const int G_Offset = 1;
        internal const int B_Offset = 0;

    }
}
