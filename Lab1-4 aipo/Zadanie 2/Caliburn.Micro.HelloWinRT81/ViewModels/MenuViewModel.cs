﻿using Caliburn.Micro;
using Motorola.Models;
using Motorola.Models.ImageEffects;
using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.IO;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Linq;
using System.Diagnostics;
using Windows.UI.Xaml.Input;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Motorola.Models.ImageEffects.Thresholds;

namespace Motorola.ViewModels
{
    public class MenuViewModel : ViewModelBase, IHandle<ErrorMsg>
    {
        private readonly INavigationService navigationService;

        public MenuViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.navigationService = navigationService;
            Effects = new ObservableCollection<IImageEffect>();
            Effects.Add(new Negative());
            Effects.Add(new GreyScale());
            Effects.Add(new GreyScale4Humans());
            Effects.Add(new Sepia());
            Effects.Add(new Normalization());
            Effects.Add(new RobertsCross());
            Effects.Add(new SobelFilter());
            Effects.Add(new GlobalThreshold());
            Effects.Add(new LocalThreshold());
            Effects.Add(new MixedThreshold());
            Effects.Add(new BernsenThreshold());
            Effects.Add(new OtsuThreshold());
            Effects.Add(new SplotFilter());
            Effects.Add(new IsolateSingleShapes());
            Effects.Add(new Scaling());
            Effects.Add(new Rotation());
            Effects.Add(new NoiseReduction());
            Effects.Add(new KuwaharaFilter());
            Effects.Add(new SkeletonizationK3M());
            Effects.Add(new GaussFilter());
            //Effects.Add(new TimeTest());
            SelectedEffect = Effects[0];
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        private IRandomAccessStream fileStream = null;
        private Guid decoderId;
        private BitmapDecoder decoder;
        private PixelDataProvider pixelData;
        private WriteableBitmap writeableBitmap;

        private ImageWrapper OriginalImage;
        private ImageWrapper CurrentImage;

        public async void LoadImage()
        {
            FileOpenPicker FOP = new FileOpenPicker();
            FOP.ViewMode = PickerViewMode.Thumbnail;
            FOP.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            FOP.FileTypeFilter.Add(".bmp");
            FOP.FileTypeFilter.Add(".jpg");
            FOP.FileTypeFilter.Add(".png");
            FOP.FileTypeFilter.Add(".jpeg");

            StorageFile file = await FOP.PickSingleFileAsync();
            if (file != null)
            {
                fileStream = await file.OpenAsync(FileAccessMode.Read);

                switch (file.FileType.ToLower())
                {
                    case ".jpg":
                    case ".jpeg":
                        decoderId = BitmapDecoder.JpegDecoderId;
                        break;
                    case ".bmp":
                        decoderId = BitmapDecoder.BmpDecoderId;
                        break;
                    case ".png":
                        decoderId = BitmapDecoder.PngDecoderId;
                        break;
                    case ".gif":
                        decoderId = BitmapDecoder.GifDecoderId;
                        break;
                }
                decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
                pixelData = await decoder.GetPixelDataAsync(
                    BitmapPixelFormat.Bgra8,    // byte array format: Blue Green Red Alpha [8 bit values: 0-255]
                    BitmapAlphaMode.Straight,   // How to code Alpha channel
                    new BitmapTransform(),
                    ExifOrientationMode.IgnoreExifOrientation,
                    ColorManagementMode.DoNotColorManage);

                OriginalImage = new ImageWrapper(pixelData.DetachPixelData(), decoder.PixelWidth, decoder.PixelHeight);

                writeableBitmap = new WriteableBitmap((int)OriginalImage.Width, (int)OriginalImage.Height);
                await writeableBitmap.SetSourceAsync(fileStream);

                ImageWriteableBitmap = writeableBitmap;

                CurrentImage = new ImageWrapper(OriginalImage);
            }
        }

        public void Histogram()
        {
            if (fileStream == null)
                return;

            HistogramConfig = new HistogramConfig();

            HistogramConfig.BlueChannel = CurrentImage.ByteArray.Where((item, index) => index % 4 == 0).ToArray();
            HistogramConfig.GreenChannel = CurrentImage.ByteArray.Where((item, index) => index % 4 == 1).ToArray();
            HistogramConfig.RedChannel = CurrentImage.ByteArray.Where((item, index) => index % 4 == 2).ToArray();

            NavigationExtensions.NavigateToViewModel<HistogramViewModel>(navigationService);
        }

        public HistogramConfig HistogramConfig;

        public async void ApplyEffect()
        {
            if (fileStream == null || SelectedEffect == null)
                return;

            ImageWrapper result = null;
            await ThreadPool.RunAsync(new WorkItemHandler(
                        (IAsyncAction action) =>
                        {
                            result = SelectedEffect.ApplyEffect(CurrentImage);
                        }
                    ));

            if (result == null)
                return;
            else
                CurrentImage = result;

            writeableBitmap = new WriteableBitmap((int)CurrentImage.Width, (int)CurrentImage.Height);

            using (Stream stream = writeableBitmap.PixelBuffer.AsStream())
                await stream.WriteAsync(CurrentImage.ByteArray, 0, CurrentImage.ByteArray.Length);

            ImageWriteableBitmap = writeableBitmap;
            Debug.WriteLine("Effect done");

        }

        public async void ResetImage()
        {
            if (fileStream == null)
                return;

            CurrentImage = new ImageWrapper(OriginalImage);

            writeableBitmap = new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight);

            using (Stream stream = writeableBitmap.PixelBuffer.AsStream())
                await stream.WriteAsync(OriginalImage.ByteArray, 0, OriginalImage.ByteArray.Length);

            ImageWriteableBitmap = writeableBitmap;
        }


        private WriteableBitmap _ImageWriteableBitmap;
        public WriteableBitmap ImageWriteableBitmap
        {
            get { return _ImageWriteableBitmap; }
            set
            {
                _ImageWriteableBitmap = value;
                NotifyOfPropertyChange(() => ImageWriteableBitmap);
            }
        }

        public ObservableCollection<IImageEffect> Effects { get; set; }
        private IImageEffect _SelectedEffect;
        public IImageEffect SelectedEffect
        {
            get { return _SelectedEffect; }
            set
            {
                _SelectedEffect = value;
                NotifyOfPropertyChange(() => SelectedEffect);
            }
        }

        public void NumericOnlyKeyDown(TextBox tb, TextChangedEventArgs e) //KeyRoutedEventArgs e)
        {
            try
            {
                int.Parse(tb.Text);
            }
            catch (Exception)
            {
                String resultStr = "";
                if (tb.Text.Length > 0 && tb.Text[0] == '-')
                    resultStr += "-";
                for (int i = 1; i < tb.Text.Length; ++i)
                    if (char.IsDigit(tb.Text[i]))
                        resultStr += tb.Text[i];
                tb.Text = resultStr;
            }
            //if ((uint)e.Key >= (uint)VirtualKey.Number0 && (uint)e.Key <= (uint)VirtualKey.Number9)
            //    e.Handled = false;

            //else if (((uint)e.Key == 189 || (uint)e.Key == (uint)VirtualKey.Subtract) && tb.Text == String.Empty) // 189 is the key for keyboard "-"
            //    e.Handled = false;

            //else
            //    e.Handled = true;   
        }

        public async void Handle(ErrorMsg message)
        {
            var x = new Windows.UI.Popups.MessageDialog((String)Application.Current.Resources[message.Msg], (String)Application.Current.Resources[message.Title]);
            await x.ShowAsync();
        }
    }
}
