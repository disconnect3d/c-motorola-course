﻿<Page
    x:Class="Motorola.Views.MenuView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:caliburn="using:Caliburn.Micro"
    xmlns:model="using:Motorola.Models"
    mc:Ignorable="d">

    <Page.Resources>
        <DataTemplate x:Key="StandardTemplate">
            <TextBlock Foreground="White" Text="{Binding Name}" FontSize="15" TextWrapping="Wrap"/>
        </DataTemplate>
        <DataTemplate x:Key="SepiaTemplate">
            <StackPanel Orientation="Vertical">
                <TextBlock Foreground="White" Margin="0, 23, 0, 0" Text="{Binding Name}" HorizontalAlignment="Center" TextWrapping="Wrap" FontSize="15"/>
                <Slider Width="50" Padding="0, -15,0,-20" Minimum="20" Maximum="40" Value="{Binding SepiaParameter, Mode=TwoWay}"/>
            </StackPanel>
        </DataTemplate>
        <DataTemplate x:Key="SplotFilterTemplate">
            <StackPanel Orientation="Vertical">
                <TextBlock Foreground="White" Margin="0, 23, 0, 0" Text="{Binding Name}" HorizontalAlignment="Center" TextWrapping="Wrap" FontSize="15"/>
                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition/>
                        <RowDefinition/>
                        <RowDefinition/>
                    </Grid.RowDefinitions>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition/>
                        <ColumnDefinition/>
                        <ColumnDefinition/>
                    </Grid.ColumnDefinitions>
                    <TextBox Text="{Binding Mask.x0y0}" Grid.Row="0" Grid.Column="0"/>
                    <TextBox Text="{Binding Mask.x1y0}" Grid.Row="0" Grid.Column="1"/>
                    <TextBox Text="{Binding Mask.x2y0}" Grid.Row="0" Grid.Column="2"/>

                    <TextBox Text="{Binding Mask.x0y1}" Grid.Row="1" Grid.Column="0" MaxLength="3"/>
                    <TextBox Text="{Binding Mask.x1y1}" Grid.Row="1" Grid.Column="1" MaxLength="3"/>
                    <TextBox Text="{Binding Mask.x2y1}" Grid.Row="1" Grid.Column="2" MaxLength="3"/>

                    <TextBox Text="{Binding Mask.x0y2}" Grid.Row="2" Grid.Column="0" MaxLength="3"/>
                    <TextBox Text="{Binding Mask.x1y2}" Grid.Row="2" Grid.Column="1" MaxLength="3"/>
                    <TextBox Text="{Binding Mask.x2y2}" Grid.Row="2" Grid.Column="2" MaxLength="3"/>
                </Grid>
            </StackPanel>
        </DataTemplate>
        <model:TemplateSelector x:Key="FilterTemplateSelector" SepiaTemplate="{StaticResource SepiaTemplate}" StandardTemplate="{StaticResource StandardTemplate}"/>
        <Style x:Key="ListBoxItemStyle" TargetType="ListBoxItem">
        	<Setter Property="Background" Value="Transparent"/>
        	<Setter Property="TabNavigation" Value="Local"/>
        	<Setter Property="Padding" Value="8,10"/>
            <Setter Property="MaxWidth" Value="140"/>
        	<Setter Property="HorizontalContentAlignment" Value="Left"/>
        	<Setter Property="Template">
        		<Setter.Value>
        			<ControlTemplate TargetType="ListBoxItem">
        				<Border x:Name="LayoutRoot" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}" Background="{TemplateBinding Background}">
        					<VisualStateManager.VisualStateGroups>
        						<VisualStateGroup x:Name="CommonStates">
        							<VisualState x:Name="Normal"/>
        							<VisualState x:Name="PointerOver">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="LayoutRoot">
                                                <DiscreteObjectKeyFrame KeyTime="0" Value="#8A8AFF"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemPointerOverForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Disabled">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="LayoutRoot">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="Transparent"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemDisabledForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Pressed">
        								<Storyboard>
        									<DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="PressedBackground"/>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemPressedForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        						</VisualStateGroup>
        						<VisualStateGroup x:Name="SelectionStates">
        							<VisualState x:Name="Unselected"/>
        							<VisualState x:Name="Selected">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="InnerGrid">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedBackgroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="SelectedUnfocused">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="InnerGrid">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedBackgroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="SelectedDisabled">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="InnerGrid">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedDisabledBackgroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedDisabledForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="SelectedPointerOver">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="InnerGrid">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedPointerOverBackgroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="SelectedPressed">
        								<Storyboard>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="InnerGrid">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedBackgroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        									<ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
        										<DiscreteObjectKeyFrame KeyTime="0" Value="{ThemeResource ListBoxItemSelectedForegroundThemeBrush}"/>
        									</ObjectAnimationUsingKeyFrames>
        								</Storyboard>
        							</VisualState>
        						</VisualStateGroup>
        						<VisualStateGroup x:Name="FocusStates">
        							<VisualState x:Name="Focused">
        								<Storyboard>
        									<DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualWhite"/>
        									<DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualBlack"/>
        								</Storyboard>
        							</VisualState>
        							<VisualState x:Name="Unfocused"/>
        							<VisualState x:Name="PointerFocused"/>
        						</VisualStateGroup>
        					</VisualStateManager.VisualStateGroups>
        					<Grid x:Name="InnerGrid" Background="Transparent">
        						<Rectangle x:Name="PressedBackground" Fill="{ThemeResource ListBoxItemPressedBackgroundThemeBrush}" Opacity="0"/>
        						<ContentPresenter x:Name="ContentPresenter" ContentTemplate="{TemplateBinding ContentTemplate}" ContentTransitions="{TemplateBinding ContentTransitions}" Content="{TemplateBinding Content}" HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" Margin="{TemplateBinding Padding}" VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
        						<Rectangle x:Name="FocusVisualWhite" Opacity="0" StrokeDashOffset=".5" StrokeEndLineCap="Square" Stroke="{ThemeResource FocusVisualWhiteStrokeThemeBrush}" StrokeDashArray="1,1"/>
        						<Rectangle x:Name="FocusVisualBlack" Opacity="0" StrokeDashOffset="1.5" StrokeEndLineCap="Square" Stroke="{ThemeResource FocusVisualBlackStrokeThemeBrush}" StrokeDashArray="1,1"/>
        					</Grid>
        				</Border>
        			</ControlTemplate>
        		</Setter.Value>
        	</Setter>
        </Style>
    </Page.Resources>

    <Grid >
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="38*"/>
            <ColumnDefinition Width="500*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="300*"/>
            <RowDefinition Height="50*"/>
        </Grid.RowDefinitions>
        <StackPanel Orientation="Vertical">
            <AppBarButton x:Name="LoadImage" Icon="OpenFile" Label="Open Image"/>
            <AppBarButton x:Name="ResetImage" Icon="ReShare"  Label="Reset Image"/>
            <AppBarButton x:Name="Histogram" Label="Histogram">
                <AppBarButton.Icon>
                    <BitmapIcon UriSource="ms-appx:///Assets/HistogramIcon.png" Width="30"/>
                </AppBarButton.Icon>
            </AppBarButton>
        </StackPanel>


        <Image Source="{Binding ImageWriteableBitmap}" HorizontalAlignment="Center" VerticalAlignment="Center" Width="Auto" Height="Auto" Margin="6,6,6,6" Grid.Column="1"/>

        <Grid Row="1" Grid.ColumnSpan="2" Height="Auto">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="100"/>
            </Grid.ColumnDefinitions>
            <ScrollViewer HorizontalScrollBarVisibility="Visible" VerticalScrollBarVisibility="Disabled">
                <ListBox HorizontalAlignment="Stretch" VerticalAlignment="Stretch" ItemsSource="{Binding Effects}" SelectedValue="{Binding SelectedEffect, Mode=TwoWay}" ItemTemplateSelector="{StaticResource FilterTemplateSelector}" FontSize="14" ItemContainerStyle="{StaticResource ListBoxItemStyle}">
                    <ListBox.ItemsPanel>
                        <ItemsPanelTemplate>
                            <StackPanel Orientation="Horizontal" Background="Black"/>
                        </ItemsPanelTemplate>
                    </ListBox.ItemsPanel>
                </ListBox>
            </ScrollViewer>

            <AppBarButton Name="ApplyEffect" Label="Apply" VerticalAlignment="Bottom" Icon="Accept" Grid.Column="1" />
        </Grid>
    </Grid>
</Page>