﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models
{
    public interface IImageEffect
    {
        /// <summary>
        /// Used to display ImageEffect's name in ComboBox items
        /// </summary>
        String Name
        { get; }
        String Description
        { get; }
        ImageWrapper ApplyEffect(ImageWrapper image);
    }
}
