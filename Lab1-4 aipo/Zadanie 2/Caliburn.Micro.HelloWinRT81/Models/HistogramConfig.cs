﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models
{
    public class HistogramConfig
    {
        public byte[] RedChannel { get; set; }
        public byte[] GreenChannel { get; set; }
        public byte[] BlueChannel { get; set; }
    }
}
