﻿using Caliburn.Micro;
using Motorola.Models.ImageEffects;
using Motorola.Models.ImageEffects.Thresholds;
using Motorola.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Motorola.Models
{
    class ContentTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            IImageEffect selected = IoC.Get<MenuViewModel>().SelectedEffect;
            
            var splotFilter = selected as SplotFilter;
            if (splotFilter != null)
                return SplotFilterTemplate;

            var sepia = selected as Sepia;
            if (sepia != null)
                return SepiaTemplate;

            var scaling = selected as Scaling;
            if (scaling != null)
                return ScalingTemplate;

            var rotation = selected as Rotation;
            if (rotation != null)
                return RotatingTemplate;

            var noise = selected as NoiseReduction;
            if (noise != null)
                return NoiseTemplate;

            var gauss = selected as GaussFilter;
            if (gauss != null)
                return GaussFilterTemplate;

            var kuwahara = selected as KuwaharaFilter;
            if (kuwahara != null)
                return KuwaharaFilterTemplate;

            var regionAndDev = selected as RegionAndDeviationThreshold;
            if (regionAndDev != null)
                return RegionAndDeviationThresholdTemplate;

            var region = selected as RegionThreshold;
            if (region != null)
                return RegionThresholdTemplate;

            return EmptyTemplate;
        }

        public DataTemplate SepiaTemplate { get; set; }
        public DataTemplate EmptyTemplate { get; set; }
        public DataTemplate SplotFilterTemplate { get; set; }
        public DataTemplate ScalingTemplate { get; set; }
        public DataTemplate RotatingTemplate { get; set; }
        public DataTemplate NoiseTemplate { get; set; }
        public DataTemplate GaussFilterTemplate { get; set; }
        public DataTemplate KuwaharaFilterTemplate { get; set; }
        public DataTemplate RegionThresholdTemplate { get; set; }
        public DataTemplate RegionAndDeviationThresholdTemplate { get; set; }
    }
}
