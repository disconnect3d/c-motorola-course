﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models
{
    public class ErrorMsg
    {
        public ErrorMsg(String msg, String title)
        {
            Msg = msg; Title = title;
        }
        public String Msg { get; set; }
        public String Title { get; set; }
    }
}
