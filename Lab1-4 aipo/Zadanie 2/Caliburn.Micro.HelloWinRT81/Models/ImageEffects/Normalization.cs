﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    class Normalization : IImageEffect
    {
        public String Name { get { return (String)Application.Current.Resources["NormalizationName"]; } }
        public string Description { get { return (String)Application.Current.Resources["NormalizationDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            int index = 0;
            const byte dpp = 255;

            byte BMin = image.ByteArray[0];
            byte BMax = BMin;
            byte GMin = image.ByteArray[1];
            byte GMax = BMin;
            byte RMin = image.ByteArray[2];
            byte RMax = BMin;

            int iterIndex = 0;
            foreach (var item in image.ByteArray)
            {
                switch (iterIndex++ % 4)
                {
                    case 0:
                        if (item > BMax)
                            BMax = item;
                        else if (item < BMin)
                            BMin = item;
                        break;
                    case 1:
                        if (item > GMax)
                            GMax = item;
                        else if (item < GMin)
                            GMin = item;
                        break;
                    case 2:
                        if (item > RMax)
                            RMax = item;
                        else if (item < RMin)
                            RMin = item;
                        break;
                }
            }

            for (int y = 0; y < image.Height; ++y)
            {
                for (int x = 0; x < image.Width; ++x)
                {
                    int B = image.ByteArray[index] - BMin;
                    B = ImageWrapper.CutColorRange(B);
                    int G = image.ByteArray[index + 1] - GMin;
                    G = ImageWrapper.CutColorRange(G);
                    int R = image.ByteArray[index + 2] - RMin;
                    R = ImageWrapper.CutColorRange(R);

                    image.ByteArray[index] = (byte)(dpp * B / (BMax - BMin));
                    image.ByteArray[index + 1] = (byte)(dpp * G / (GMax - GMin));
                    image.ByteArray[index + 2] = (byte)(dpp * R / (RMax - RMin));
                    index += 4;
                }
            }
            return image;
        }
    }
}
