﻿using Motorola.Models.ImageEffects.Thresholds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    public class IsolateSingleShapes : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["IsolateSingleShapesName"]; } }
        public string Description { get { return (String)Application.Current.Resources["IsolateSingleShapesDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            const int BLACK = 0;
            CurrentWorkingImage = image;
            if (!IsImageBlackAndWhite())
                image = new GlobalThreshold().ApplyEffect(image);

            Random rnd = new Random();

            for (int y = 0; y < CurrentWorkingImage.Height; ++y)
                for (int x = 0; x < CurrentWorkingImage.Width; ++x)
                {
                    if (CurrentWorkingImage[x, y, 0] == BLACK)
                    {
                        R = (byte)rnd.Next(1, 255);
                        G = (byte)rnd.Next(0, 255);
                        B = (byte)rnd.Next(0, 255);
                        FloodFill(x, y);
                    }
                }

            return CurrentWorkingImage;

        }
        byte R;
        byte G;
        byte B;

        private void FloodFill(int x, int y)
        {
            List<Tuple<int, int>> helper = new List<Tuple<int, int>>(700000);
            CurrentWorkingImage.SetRGB(x, y, R, G, B);
            helper.Add(new Tuple<int, int>(x, y));
            while (helper.Count != 0)
            {
                Tuple<int, int> popped = helper.ElementAt(0);
                helper.RemoveAt(0);

                for (int i = -1; i <= 1; ++i)
                    for (int j = -1; j <= 1; ++j)
                    {
                        if ((i == 0 && j == 0) || popped.Item1 + j < 0 || popped.Item1 + j == CurrentWorkingImage.Width || popped.Item2 + i < 0 || popped.Item2 + i == CurrentWorkingImage.Height)
                            continue;
                        if (CurrentWorkingImage[popped.Item1 + j, popped.Item2 + i, 0] == 0)
                        {
                            CurrentWorkingImage.SetRGB(popped.Item1 + j, popped.Item2 + i, R, G, B);
                            helper.Add(new Tuple<int, int>(popped.Item1 + j, popped.Item2 + i));
                        }
                    }
            }
        }

        private unsafe bool IsImageBlackAndWhite()
        {
            fixed (byte* ptr = CurrentWorkingImage.ByteArray)
            {
                for (long i = 0; i < CurrentWorkingImage.ByteArray.Length; i += 4)
                    if ((ptr[i] != 0 && ptr[i] != 255) ||
                        (ptr[i + 1] != 0 && ptr[i + 1] != 255) ||
                        (ptr[i + 2] != 0 && ptr[i + 2] != 255))
                        return false;
            }
            return true;
        }

        private ImageWrapper CurrentWorkingImage;

    }
}