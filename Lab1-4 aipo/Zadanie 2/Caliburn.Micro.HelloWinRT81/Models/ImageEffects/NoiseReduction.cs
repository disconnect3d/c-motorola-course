﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    public class NoiseReduction : PropertyChangedBase, IImageEffect
    {
        public NoiseReduction()
        {
            NoiseChance = 0;
            NoiseChangeVisibility = Visibility.Visible;
        }

        private enum NoiseEffect { SaltAndPepperNoise, UniformNoise, ReductionByAverageFilter, ReductionByMedianFilter, ReductionByExtendedMedianFilter };

        private Visibility _NoiseChangeVisibility;
        public Visibility NoiseChangeVisibility
        {
            get { return _NoiseChangeVisibility; }
            set
            {
                _NoiseChangeVisibility = value;
                NotifyOfPropertyChange(() => NoiseChangeVisibility);
            }
        }

        public string Name { get { return (String)Application.Current.Resources["NoiseReductionName"]; } }
        public string Description { get { return (String)Application.Current.Resources["NoiseReductionDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            switch (SelectedNoise)
            {
                case (int)NoiseEffect.SaltAndPepperNoise:
                    if (NoiseChance != 0)
                        return SaltAndPepper(image);
                    else
                        return image;
                case (int)NoiseEffect.UniformNoise:
                    if (NoiseChance != 0)
                        return UniformNoise(image);
                    else
                        return image;
                case (int)NoiseEffect.ReductionByAverageFilter:
                    return ReductionByAverageFilter(image);
                case (int)NoiseEffect.ReductionByMedianFilter:
                    return ReductionByMedianFilter(image);
                case (int)NoiseEffect.ReductionByExtendedMedianFilter:
                    return ReductionByExtendedMedianFilter(image);
            }
            return image;
        }

        private double _NoiseChance;
        public double NoiseChance
        {
            get { return _NoiseChance; }
            set
            {
                _NoiseChance = value;
                NotifyOfPropertyChange(() => NoiseChance);
            }
        }

        private int _SelectedNoise;
        public int SelectedNoise
        {
            get { return _SelectedNoise; }
            set
            {
                _SelectedNoise = value;
                if (value == (int)NoiseEffect.SaltAndPepperNoise || value == (int)NoiseEffect.UniformNoise)
                    NoiseChangeVisibility = Visibility.Visible;
                else
                    NoiseChangeVisibility = Visibility.Collapsed;

                NotifyOfPropertyChange(() => SelectedNoise);
            }
        }


        private ImageWrapper ReductionByAverageFilter(ImageWrapper image)
        {
            const int region = 5;
            int regionTo = region / 2;
            int regionFrom = -regionTo;

            int R, G, B, divisionNumber;
            //int sumA = 0;

            ImageWrapper imageHelper = new ImageWrapper(image);

            for (int y = 0; y < image.Height; ++y)
                for (int x = 0; x < image.Width; ++x)
                {
                    divisionNumber = R = G = B = 0;

                    for (int xRegion = regionFrom; xRegion <= regionTo; ++xRegion)
                        for (int yRegion = regionFrom; yRegion <= regionTo; ++yRegion)
                        {
                            int xCoord = x + xRegion;
                            int yCoord = y + yRegion;
                            if (xCoord < 0 || xCoord >= image.Width || yCoord < 0 || yCoord >= image.Height)
                                continue;
                            ++divisionNumber;
                            int index = (int)image.GetIndex(xCoord, yCoord, 0);

                            B += imageHelper.ByteArray[index + 0];
                            G += imageHelper.ByteArray[index + 1];
                            R += imageHelper.ByteArray[index + 2];
                            //sumA += imageHelper.ByteArray[index + 3];
                        }
                    R /= divisionNumber;
                    G /= divisionNumber;
                    B /= divisionNumber;

                    image.SetRGB(x, y, R, G, B);
                }

            return image;
        }

        private unsafe ImageWrapper SaltAndPepper(ImageWrapper image)
        {
            Random rnd = new Random();
            fixed (byte* ptr = image.ByteArray)
            {
                for (int i = 0; i < image.ByteArray.Length; i += 4)
                {
                    if (rnd.NextDouble() < NoiseChance)
                    {
                        if (rnd.NextDouble() <= 0.5)
                            ptr[i] = ptr[i + 1] = ptr[i + 2] = 0;
                        else
                            ptr[i] = ptr[i + 1] = ptr[i + 2] = 255;
                    }
                }
            }
            return image;
        }

        private unsafe ImageWrapper UniformNoise(ImageWrapper image)
        {
            Random rnd = new Random();

            int noiseValue = rnd.Next(-50, 50);
            fixed (byte* ptr = image.ByteArray)
            {
                for (int i = 0; i < image.ByteArray.Length; i += 4)
                {
                    if (rnd.NextDouble() < NoiseChance)
                    {
                        ptr[i] = ImageWrapper.CutColorRange(ptr[i] + noiseValue);
                        ptr[i + 1] = ImageWrapper.CutColorRange(ptr[i + 1] + noiseValue);
                        ptr[i + 2] = ImageWrapper.CutColorRange(ptr[i + 2] + noiseValue);
                    }
                }
            }
            return image;
        }

        private ImageWrapper ReductionByMedianFilter(ImageWrapper image)
        {
            const int region = 5;
            const int regionSquare = region * region;
            const int regionTo = region / 2;
            const int regionFrom = -regionTo;

            ImageWrapper imageHelper = new ImageWrapper(image);
            //System.Diagnostics.Stopwatch xxx = new System.Diagnostics.Stopwatch();
            //xxx.Start();
            List<byte> Rlist = new List<byte>(regionSquare);
            List<byte> Glist = new List<byte>(regionSquare);
            List<byte> Blist = new List<byte>(regionSquare);
            int R, G, B;

            for (int y = 0; y < image.Height; ++y)
                for (int x = 0; x < image.Width; ++x)
                {
                    #region ADDING ENVIRONMENT PIXELS' COLORS TO ITS LISTS
                    R = G = B = 0;
                    //int sumA = 0;

                    for (int xRegion = regionFrom; xRegion <= regionTo; ++xRegion)
                        for (int yRegion = regionFrom; yRegion <= regionTo; ++yRegion)
                        {
                            int xCoord = x + xRegion;
                            int yCoord = y + yRegion;
                            if (xCoord < 0 || xCoord >= image.Width || yCoord < 0 || yCoord >= image.Height)
                                continue;
                            int index = (int)image.GetIndex(xCoord, yCoord, 0);

                            Blist.Add(imageHelper.ByteArray[index + 0]);
                            Glist.Add(imageHelper.ByteArray[index + 1]);
                            Rlist.Add(imageHelper.ByteArray[index + 2]);
                        }
                    #endregion

                    #region COUTING MEDIANS
                    Rlist.Sort();
                    Glist.Sort();
                    Blist.Sort();

                    // Counting medians
                    int i = Rlist.Count / 2;
                    if (Rlist.Count % 2 == 1)
                        R = Rlist[i];
                    else
                        R = (Rlist[i] + Rlist[i + 1]) / 2;

                    if (Glist.Count % 2 == 1)
                        G = Glist[i];
                    else
                        G = (Glist[i] + Glist[i + 1]) / 2;

                    if (Blist.Count % 2 == 1)
                        B = Blist[i];
                    else
                        B = (Blist[i] + Blist[i + 1]) / 2;
                    #endregion

                    Rlist.Clear();
                    Glist.Clear();
                    Blist.Clear();

                    image.SetRGB(x, y, R, G, B);
                }

            //System.Diagnostics.Debug.WriteLine(xxx.ElapsedMilliseconds);
            return image;
        }

        private unsafe ImageWrapper ReductionByExtendedMedianFilter(ImageWrapper image)
        {
            const int region = 5;
            const int regionSquare = region * region;
            const int regionTo = region / 2;
            const int regionFrom = -regionTo;

            ImageWrapper imageHelper = new ImageWrapper(image);

            System.Diagnostics.Stopwatch xxx = new System.Diagnostics.Stopwatch();
            xxx.Start();

            List<byte> Rlist = new List<byte>(regionSquare);
            List<byte> Glist = new List<byte>(regionSquare);
            List<byte> Blist = new List<byte>(regionSquare);
            int R, G, B;
            fixed (byte* imagePtr = image.ByteArray)
            {
                fixed (byte* ptr = imageHelper.ByteArray)
                {
                    for (int y = 0; y < image.Height; ++y)
                        for (int x = 0; x < image.Width; ++x)
                        {
                            #region ADDING ENVIRONMENT PIXELS' COLORS TO ITS LISTS
                            R = G = B = 0;
                            //int sumA = 0;

                            for (int xRegion = regionFrom; xRegion <= regionTo; ++xRegion)
                                for (int yRegion = regionFrom; yRegion <= regionTo; ++yRegion)
                                {
                                    int xCoord = x + xRegion;
                                    int yCoord = y + yRegion;
                                    if (xCoord < 0 || xCoord >= image.Width || yCoord < 0 || yCoord >= image.Height)
                                        continue;
                                    int index = (int)image.GetIndex(xCoord, yCoord, 0);

                                    Blist.Add(ptr[index + 0]);
                                    Glist.Add(ptr[index + 1]);
                                    Rlist.Add(ptr[index + 2]);
                                }
                            #endregion

                            #region COUNTING MEDIANS
                            Rlist.Sort();
                            Glist.Sort();
                            Blist.Sort();

                            int indexL = Rlist.Count / 2;
                            if (Rlist.Count % 2 == 1)
                                R = Rlist[indexL];
                            else
                                R = (Rlist[indexL] + Rlist[indexL + 1]) / 2;

                            if (Glist.Count % 2 == 1)
                                G = Glist[indexL];
                            else
                                G = (Glist[indexL] + Glist[indexL + 1]) / 2;

                            if (Blist.Count % 2 == 1)
                                B = Blist[indexL];
                            else
                                B = (Blist[indexL] + Blist[indexL + 1]) / 2;
                            #endregion

                            // counting 20% index
                            int twentyPercentIndex = (int)((double)Rlist.Count * 0.2);

                            int xyIndex = (int)image.GetIndex(x, y, 0);

                            #region CHECKING IF BLUE CHANNEL IS IN 20% OF 'BORDER' LIST COLORS
                            bool blueFound = false;
                            byte blueColor = imagePtr[xyIndex];
                            for (int i = 0; i < twentyPercentIndex; ++i)
                            {
                                if (blueColor == Blist[i])
                                {
                                    blueFound = true;
                                    break;
                                }
                            }
                            if (!blueFound)
                                for (int i = Rlist.Count - 1 - twentyPercentIndex; i < Rlist.Count; ++i)
                                {
                                    if (blueColor == Blist[i])
                                    {
                                        blueFound = true;
                                        break;
                                    }
                                }
                            #endregion

                            #region CHECKING IF GREEN CHANNEL IS IN 20% OF 'BORDER' LIST COLORS
                            bool greenFound = false;
                            byte greenColor = imagePtr[xyIndex + 1];
                            for (int i = 0; i < twentyPercentIndex; ++i)
                            {
                                if (greenColor == Glist[i])
                                {
                                    greenFound = true;
                                    break;
                                }
                            }
                            if (!greenFound)
                                for (int i = Rlist.Count - 1 - twentyPercentIndex; i < Rlist.Count; ++i)
                                {
                                    if (greenColor == Glist[i])
                                    {
                                        greenFound = true;
                                        break;
                                    }
                                }
                            #endregion

                            #region CHECKING IF RED CHANNEL IS IN 20% OF 'BORDER' LIST COLORS
                            bool redFound = false;
                            byte redColor = imagePtr[xyIndex + 2];
                            for (int i = 0; i < twentyPercentIndex; ++i)
                            {
                                if (redColor == Rlist[i])
                                {
                                    image.SetRGB(x, y, R, G, B);
                                    redFound = true;
                                }
                            }
                            if (!redFound)
                                for (int i = Rlist.Count - 1 - twentyPercentIndex; i < Rlist.Count; ++i)
                                {
                                    if (redColor == Glist[i])
                                    {
                                        image.SetRGB(x, y, R, G, B);
                                        redFound = true;
                                    }
                                }
                            #endregion

                            Rlist.Clear();
                            Glist.Clear();
                            Blist.Clear();

                            if (!blueFound)
                                B = blueColor;
                            if (!greenFound)
                                G = greenColor;
                            if (!redFound)
                                R = redColor;

                            image.SetRGB(xyIndex, (byte)R, (byte)G, (byte)B);
                        }
                }
            }
            System.Diagnostics.Debug.WriteLine(xxx.ElapsedMilliseconds);
            return image;
        }
    }
}
