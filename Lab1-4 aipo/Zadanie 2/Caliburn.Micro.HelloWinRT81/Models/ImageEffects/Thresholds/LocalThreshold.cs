﻿using Caliburn.Micro;
using Motorola.Models.ImageEffects.Thresholds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects.Thresholds
{
    class LocalThreshold : RegionThreshold, IImageEffect
    {
        public LocalThreshold()
        {
            Region = 15;
        }

        public string Name { get { return (String)Application.Current.Resources["LocalThresholdName"]; } }
        public string Description { get { return (String)Application.Current.Resources["LocalThresholdDescription"]; } }
        //*
        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);
            ImageWrapper resultImage = new ImageWrapper(image);

            for (long y = 0; y < image.Height; ++y)
                for (long x = 0; x < image.Width; ++x)
                {
                    long threshold = 0;
                    for (long xRegion = -Region / 2; xRegion <= Region / 2; ++xRegion)
                    { 
                        for (long yRegion = -Region / 2; yRegion <= Region / 2; ++yRegion)
                        {
                            long xCopy = xRegion;
                            long yCopy = yRegion;
                            while (x + xCopy < 0)
                                ++xCopy;
                            while (x + xCopy >= image.Width)
                                --xCopy;

                            while (y + yCopy < 0)
                                ++yCopy;
                            while (y + yCopy >= image.Height)
                                --yCopy;

                            threshold += image[x + xCopy, y + yCopy, 0];
                        }
                    }
                    threshold /= Region * Region;
                    resultImage.SetRGB(x, y, image[x, y, 0] < ImageWrapper.CutColorRange(threshold) ? (byte)0 : (byte)255);
                }

            return resultImage;
        }
    }
}
