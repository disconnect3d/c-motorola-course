﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models.ImageEffects.Thresholds
{
    public abstract class RegionThreshold : PropertyChangedBase
    {
        private int _Region;
        public int Region
        {
            get { return _Region; }
            set
            {
                _Region = value;
                NotifyOfPropertyChange(() => Region);
            }
        }
    }
}
