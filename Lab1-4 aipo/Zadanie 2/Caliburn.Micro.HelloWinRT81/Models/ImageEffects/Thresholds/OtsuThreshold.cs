﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects.Thresholds
{
    public class OtsuThreshold : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["OtsuThresholdName"]; } }
        public string Description { get { return (String)Application.Current.Resources["OtsuThresholdDescription"]; } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);

            int[] histogram = image.BlueHistogramData();
            double SU = 0;
            for (int i = 0; i < 256; ++i)
                SU += i * histogram[i];
            double W = 0;
            double MAX = 0;
            double SUP = 0;
            double LP = image.ByteArray.Length / 4;
            double WP = 0;
            double SG, SD, R;
            double threshold1 = 0, threshold2 = 0;
            for(int i=0; i<256; ++i)
            {
                W += histogram[i];
                if (W == 0) continue;
                WP = LP - W;
                if (WP == 0) break;
                SUP += i * histogram[i];
                SG = SUP / W;
                SD = (SU - SUP) / WP;
                R = W * WP * (SG - SD) * (SG - SD);
                if (R>=MAX)
                {
                    threshold1 = i;
                    if (R > MAX)
                        threshold2 = i;
                    MAX = R;
                }
            }

            int threshold = (int)(threshold1 + threshold2) / 2;

            fixed(byte* ptr = image.ByteArray)
            { 
            for (int i = 0; i < image.ByteArray.Length; i += 4)
            {
                ptr[i] = ptr[i + 1] = ptr[i + 2] =
                    ptr[i] < threshold ? (byte)0 : (byte)255;
            }
            }
            return image;
        }
    }
}
