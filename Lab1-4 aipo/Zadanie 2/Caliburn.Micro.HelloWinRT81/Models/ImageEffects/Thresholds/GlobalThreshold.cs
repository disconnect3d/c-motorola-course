﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects.Thresholds
{
    class GlobalThreshold : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["GlobalThresholdName"]; } }
        public string Description { get { return (String)Application.Current.Resources["GlobalThresholdDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);
            int threshold = GetGlobalThresholdFromGrayScaleImage(image.ByteArray);

            for (int i = 0; i < image.ByteArray.Length; i += 4)
            {
                image.ByteArray[i] = image.ByteArray[i + 1] = image.ByteArray[i + 2] =
                    image.ByteArray[i] <= threshold ? (byte)0 : (byte)255;
            }

            return image;
        }

        public static int GetGlobalThresholdFromGrayScaleImage(byte[] imageByteArray)
        {
            int threshold = 0;
            for (int i = 0; i < imageByteArray.Length; i += 4)
                threshold += imageByteArray[i];
            threshold /= (imageByteArray.Length / 4);
            return threshold;
        }
    }
}
