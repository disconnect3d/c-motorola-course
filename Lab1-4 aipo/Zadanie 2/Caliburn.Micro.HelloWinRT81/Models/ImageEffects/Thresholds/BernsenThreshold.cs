﻿using Caliburn.Micro;
using Motorola.Models.ImageEffects.Thresholds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects.Thresholds
{
    public class BernsenThreshold : RegionAndDeviationThreshold, IImageEffect
    {
        public BernsenThreshold()
        {
            Region = 15;
            Deviation = 15;
        }
            
        public string Name { get { return (String)Application.Current.Resources["BernsenThresholdName"]; } }
        public string Description { get { return (String)Application.Current.Resources["BernsenThresholdDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);

            int globalThreshold = GlobalThreshold.GetGlobalThresholdFromGrayScaleImage(image.ByteArray);

            ImageWrapper imageHelper = image.CopyImageByIncreasingUsingBorders(Region);

            for (long y = Region; y < image.Height + Region - 1; ++y)
                for (long x = Region; x < image.Width + Region - 1; ++x)
                {
                    byte min = imageHelper[x-1, y-1, 0];
                    byte max = min;

                    long threshold = 0;
                    for (long xRegion = -Region / 2; xRegion <= Region / 2; ++xRegion)
                        for (long yRegion = -Region / 2; yRegion <= Region / 2; ++yRegion)
                        {
                            byte taken = imageHelper[x + xRegion, y + yRegion, 0];
                            if (min > taken)
                                min = taken;
                            else if (max < taken)
                                max = taken;
                        }

                    threshold = (min + max) / 2;

                    if (Math.Abs(globalThreshold - threshold) > Deviation)
                    {
                        if (threshold < globalThreshold - Deviation)
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold - Deviation) ? (byte)0 : (byte)255);
                        else
                            image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < (globalThreshold + Deviation) ? (byte)0 : (byte)255);
                    }
                    else
                        image.SetRGB(x - Region, y - Region, imageHelper[x, y, 0] < threshold ? (byte)0 : (byte)255);
                }

            return image;
        }
    }
}
