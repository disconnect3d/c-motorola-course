﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models.ImageEffects.Thresholds
{
    public abstract class RegionAndDeviationThreshold : RegionThreshold
    {
        private int _Deviation;
        public int Deviation
        {
            get { return _Deviation; }
            set
            {
                _Deviation = value;
                NotifyOfPropertyChange(() => Deviation);
            }
        }
    }
}
