﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    public class MaskGrid : PropertyChangedBase
    {
        private int _x0y0;
        public int x0y0
        {
            get { return _x0y0; }
            set
            {
                _x0y0 = value;
                NotifyOfPropertyChange(() => x0y0);
            }
        }

        private int _x1y0;
        public int x1y0
        {
            get { return _x1y0; }
            set
            {
                _x1y0 = value;
                NotifyOfPropertyChange(() => x1y0);
            }
        }

        private int _x2y0;
        public int x2y0
        {
            get { return _x2y0; }
            set
            {
                _x2y0 = value;
                NotifyOfPropertyChange(() => x2y0);
            }
        }

        private int _x0y1;
        public int x0y1
        {
            get { return _x0y1; }
            set
            {
                _x0y1 = value;
                NotifyOfPropertyChange(() => x0y1);
            }
        }

        private int _x1y1;
        public int x1y1
        {
            get { return _x1y1; }
            set
            {
                _x1y1 = value;
                NotifyOfPropertyChange(() => x1y1);
            }
        }

        private int _x2y1;
        public int x2y1
        {
            get { return _x2y1; }
            set
            {
                _x2y1 = value;
                NotifyOfPropertyChange(() => x2y1);
            }
        }

        private int _x0y2;
        public int x0y2
        {
            get { return _x0y2; }
            set
            {
                _x0y2 = value;
                NotifyOfPropertyChange(() => x0y2);
            }
        }

        private int _x1y2;
        public int x1y2
        {
            get { return _x1y2; }
            set
            {
                _x1y2 = value;
                NotifyOfPropertyChange(() => x1y2);
            }
        }

        private int _x2y2;
        public int x2y2
        {
            get { return _x2y2; }
            set
            {
                _x2y2 = value;
                NotifyOfPropertyChange(() => x2y2);
            }
        }

        public int Sum()
        {
            return x0y0 + x0y1 + x0y2 + x1y0 + x1y1 + x1y2 + x2y0 + x2y1 + x2y2;
        }

    }

    public class SplotFilter : IImageEffect
    {

        private MaskGrid _Mask = new MaskGrid();
        public MaskGrid Mask { get { return _Mask; } }
        public string Name { get { return (String)Application.Current.Resources["SplotFilterName"]; } }
        public string Description { get { return (String)Application.Current.Resources["SplotFilterDescription"]; } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            ImageWrapper imageHelper = image.CopyImageByIncreasingUsingBorders(1);
            System.Diagnostics.Stopwatch xxx = new System.Diagnostics.Stopwatch();
            xxx.Start();

            fixed (byte* ptr = imageHelper.ByteArray)
            {
                for (int y = 1; y < imageHelper.Height - 1; ++y)
                    for (int x = 1; x < imageHelper.Width - 1; ++x)
                    {
                        int B = 0;
                        int G = 0;
                        int R = 0;
                        int index;

                        int mask = 0;
                        mask = Mask.x0y0;
                        index = (int)imageHelper.GetIndex(x - 1, y - 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x1y0;
                        index = (int)imageHelper.GetIndex(x, y - 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x2y0;
                        index = (int)imageHelper.GetIndex(x + 1, y - 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x0y1;
                        index = (int)imageHelper.GetIndex(x - 1, y, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x1y1;
                        index = (int)imageHelper.GetIndex(x, y, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x2y1;
                        index = (int)imageHelper.GetIndex(x + 1, y, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x0y2;
                        index = (int)imageHelper.GetIndex(x - 1, y + 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x1y2;
                        index = (int)imageHelper.GetIndex(x, y + 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        mask = Mask.x2y2;
                        index = (int)imageHelper.GetIndex(x + 1, y + 1, 0);
                        B += ptr[index] * mask;
                        G += ptr[index + 1] * mask;
                        R += ptr[index + 2] * mask;

                        int maskValue = Mask.Sum();
                        B /= maskValue;
                        G /= maskValue;
                        R /= maskValue;

                        image.SetRGB(x - 1, y - 1, ImageWrapper.CutColorRange(R), ImageWrapper.CutColorRange(G), ImageWrapper.CutColorRange(B));
                    }
            }
            System.Diagnostics.Debug.WriteLine(xxx.ElapsedMilliseconds);
            return image;
        }
    }
}
