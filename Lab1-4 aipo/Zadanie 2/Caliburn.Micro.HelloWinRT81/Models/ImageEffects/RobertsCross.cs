﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    class RobertsCross : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["RobertsCrossName"]; } }
        public string Description { get { return (String)Application.Current.Resources["RobertsCrossDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            const int channels = 4;

            // creating most of the image
            for (uint y = 0; y < image.Height; ++y)
            {
                for (uint x = 0; x < image.Width; ++x)
                {
                    for (uint i = 0; i < 3; ++i)
                    {
                        // (x, y)
                        uint x0y0 = y * image.Width * channels + x * channels + i;

                        // if pixels are not on the right or bottom borders
                        if (y != image.Height - 1 && x != image.Width - 1)
                        {
                            // (x+1, y+1)
                            uint x1y1 = x0y0 + image.Width * channels + channels;
                            // (x+1, y)
                            uint x1y0 = x0y0 + channels;
                            // (x, y+1)
                            uint x0y1 = x0y0 + image.Width * channels;

                            // tmp1 = (x, y) - (x+1, y+1)
                            int tmp1 = image.ByteArray[x0y0] - image.ByteArray[x1y1];
                            // tmp2 = (x+1, y) - (x, y+1)
                            int tmp2 = image.ByteArray[x1y0] - image.ByteArray[x0y1];

                            // (x,y) = (tmp1) + (tmp2)
                            image.ByteArray[x0y0] = ImageWrapper.CutColorRange(Math.Abs(tmp1) + Math.Abs(tmp2));
                        }
                        else // set pixel to black color (each channel to 0) when on border
                            image.ByteArray[x0y0] = (byte)0;
                    }
                }
            }
            return image;
        }
    }
}
