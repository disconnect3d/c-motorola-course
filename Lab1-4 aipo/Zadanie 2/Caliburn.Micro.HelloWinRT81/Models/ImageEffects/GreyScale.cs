﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    class GreyScale : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["GreyScaleName"]; } }
        public string Description { get { return (String)Application.Current.Resources["GreyScaleDescription"]; } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            fixed (byte* ptr = image.ByteArray)
                for (int index = 0; index < image.ByteArray.Length; index += 4)
                    ptr[index] = ptr[index + 1] = ptr[index + 2]
                        = (byte)((ptr[index] + ptr[index + 1] + ptr[index + 2]) / 3);
            return image;
        }
    }
}
