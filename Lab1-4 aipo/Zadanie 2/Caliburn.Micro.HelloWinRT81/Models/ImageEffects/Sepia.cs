﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.Models.ImageEffects
{
    class Sepia : PropertyChangedBase, IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["SepiaName"]; } }
        public string Description { get { return (String)Application.Current.Resources["SepiaDescription"]; } }

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            fixed (byte* ptr = image.ByteArray)
                for (int index = 0; index < image.ByteArray.Length; index += 4)
                {
                    int G = ptr[index + 1] + (int)SepiaParameter;
                    int R = ptr[index + 2] + 2 * (int)SepiaParameter;
                    ptr[index + 2] = ImageWrapper.CutColorRange(R);
                    ptr[index + 1] = ImageWrapper.CutColorRange(G);
                }
            return image;
        }

        private double _SepiaParameter;
        public double SepiaParameter
        {
            get { return _SepiaParameter; }
            set
            {
                _SepiaParameter = value;

                NotifyOfPropertyChange(() => SepiaParameter);
            }
        }

    }
}
