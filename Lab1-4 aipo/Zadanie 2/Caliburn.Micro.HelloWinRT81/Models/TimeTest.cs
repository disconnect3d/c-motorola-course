﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.Models.ImageEffects
{
    class TimeTest : IImageEffect
    {
        public string Name
        {
            get { return "DO NOT USE IT"; }
        }

        public string Description
        {
            get { return ""; }
        }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            ImageWrapper returnImg = image.CopyImageByIncreasingUsingBorders(10);
            System.Diagnostics.Debug.WriteLine("Timer elapsed (ms): " + timer.ElapsedMilliseconds);

            return returnImg;
        }
    }
}
